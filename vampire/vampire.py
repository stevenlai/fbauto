import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *

class VampireParser(HTMLParser):
	begin=False
	opened=False
	opener=None
	header=[]
	monster_type=''

	def __init__(self, monster_type, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.monster_type=monster_type
		self.begin=False
		self.opened=False
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form' and attrs.count(('action','feed-eat.php'))>0:
			self.begin=True
		if tag=='input' and self.begin:
			if attrs[0]==('type', 'hidden'):
				self.header.append((attrs[1][1], attrs[2][1]))
			elif attrs[0]==('type', 'radio'):
				if self.opened==False:
					self.header.append((attrs[1][1], attrs[2][1]))
					print ('opening feed page: '+Facebook.appsURL+'/'+self.monster_type+'/feed-eat.php')
					for i in self.header:
						print (i)
					self.opened=True
					self.header = urllib.parse.urlencode(self.header)
					self.opener.open(Facebook.appsURL+'/'+self.monster_type+'/feed-eat.php', self.header)


class Vampire(JobRunner):
	sleepTime=0
	monsters=['slayers','werewolves','vampires','zombies']
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.sleepTime=0
	def feed (self, monster_type):
		print ('feeding '+monster_type)
		historyPage=Facebook.appsURL+'/'+monster_type+'/event-history.php'
		html = self.opener.open(historyPage)
		iterator=re.compile("consumer_id=[0-9]+").finditer(html)
		feedList=[]
		for match in iterator:
			id=match.group()
			if feedList.count(id)==0:
				feedList.append(id)
		print ("feedList:", feedList)
		for i in feedList:
			feedPage=Facebook.appsURL+'/'+monster_type+'/feed-main.php?'+i
			html = self.opener.open(feedPage)
			if re.compile("You can't feed any more").search(html)!=None:
				print ('Finished feeding',monster_type)
				break
			if re.compile("You can't feed this").search(html)==None:
				vp = VampireParser(monster_type, self.opener)
				vp.opener=self.opener
				print ('going to feed:'+i)
				vp.feed(html)
				vp.close()
	def checkTime_monstertype(self, monster_type):
		time=0
		#zombies are the last to feed
		html=self.opener.open(Facebook.appsURL+'/'+monster_type+'/feed-home.php?ref=top_menu')
		m=re.compile("You can't feed any more [a-zA-Z]+ for another [0-9]+ hours and [0-9]+ minutes!").search(html)
		if m!=None:
			hour_string=re.compile('[0-9]+ hours').search(m.group()).group()
			minute_string=re.compile('[0-9]+ minutes').search(m.group()).group()
			no_hours=int(re.compile('[0-9]+').search(hour_string).group())
			no_minutes=int(re.compile('[0-9]+').search(minute_string).group())+1
			print ('Need to wait to feed',monster_type,'for', no_hours, 'hours,', no_minutes, 'minutes')
			time=no_hours*3600+no_minutes*60
		return time
	def checkTime(self):
		t_final=0
		for m in self.monsters:
			t=self.checkTime_monstertype(m)
			if t>t_final:
				t_final=t
		print ('Total wait time:', t_final)
		return t_final
	def doJob(self):
		t1=time.time()
		for m in self.monsters:
			while self.checkTime_monstertype(m)==0:
				#self.opener.login()
				self.feed(m)
		t2=time.time()-t1
		print ('Finished job, used time:', t2)
		self.sleepTime=self.checkTime()
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True

	#Temp
	#time.sleep((18*60+26)*60)
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		vampire = Vampire(f)
		vampire.doJob()
		f.logout()
	else:
		f.rememberPass=True
		vampire = Vampire(f)
		vampire.start()

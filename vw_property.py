import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampiresgame.common
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.vampiresgame.inventory

if __name__ == "__main__":
	accounts=AccountManager('vw_property.txt')
	senders=[]
	for f in accounts.logins:
		controller=SessionController(f)
		controller.setMaxConnection(100)
		senders.append(controller)

	for sender in senders:
		vampiresgame_property = FBAuto.vampiresgame.property.PropertyMain(sender)
		vampiresgame_property.start()
		time.sleep(random.randint(10, 15))


import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.treasurehunt.egghunt
import FBAuto.treasurehunt.fbfairy
import FBAuto.treasurehunt.scavengermouse
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite

if __name__ == "__main__":
	controller=SessionController()
	controller.start()
	time.sleep(random.randint(10, 15))
	for i in range (0, 30):
		egghunt = FBAuto.treasurehunt.egghunt.JobMain(controller)
		egghunt.start()
		time.sleep(random.randint(10, 15))

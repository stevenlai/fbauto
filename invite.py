import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite
import FBAuto.common.autoadd

if __name__ == "__main__":
	controller=SessionController()
	controller.start()
	time.sleep(random.randint(10, 15))
	#Misc
	autoaccept = FBAuto.common.autoaccept.JobMain(controller)
	autoaccept.start()
	time.sleep(random.randint(10, 15))
	autoinvite = FBAuto.common.autoinvite.JobMain(controller)
	autoinvite.start()
	time.sleep(random.randint(10, 15))
	autoadd = FBAuto.common.autoadd.JobMain(controller)
	autoadd.start()
	time.sleep(random.randint(10, 15))

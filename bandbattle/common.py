from FBAuto.common.facebook import Facebook
import FBAuto.common.bandbattle_common

appID='bandbattle'
config = FBAuto.common.bandbattle_common.GameConfig()
config.appID=appID
config.energyPrefix='Energy:[ ]'
config.healthPrefix='Confidence: [ ]*'
config.staminaPrefix='Stamina:[ ]*'
config.cashPrefix='Money:[ ]*\$'
config.propertyPrefix='You own:[ ]*'
config.propertyURL='promote'
config.energyRechargeTime=300
if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.bandbattle_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()

import os.path
import sys
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class BandAttackParser(HTMLParser):
	detector=0
	header=[]
	opener=None
	lvl=0
	count=0

	def __init__(self, opener, lvl, count):
		HTMLParser.__init__(self)
		self.header=[]
		self.detector=0
		self.opener=opener
		self.lvl=lvl
		self.count=count
	def handle_starttag(self, tag, attrs):
		if tag=='td' and self.detector==0:
			#start to detect lvl
			self.detector=1

		if tag=='form' and self.detector==2 and attrs[0]==('action','/bandbattle/battle/battle'):
			#start to scan the form
			self.detector=3

		if self.detector==3 and tag=='input':
			if attrs[0]==('type', 'hidden'):
				self.header.append((attrs[1][1], attrs[2][1]))
			elif self.detector==3 and len(attrs)>=4 and attrs[1]==('name', 'battle[challange_id]'):
				self.header.append((attrs[1][1], attrs[3][1]))
	def handle_data (self, data):
		if self.detector==1 and re.compile("[0-9]+ other bands").search(data)!=None:
			m=re.compile('[0-9]+').search(data)
			if int(m.group())<=self.lvl:
				#lvl confirmed OK
				self.detector=2
	def handle_endtag(self, tag):
		if self.detector==3 and tag=='form' and self.count>0:
			print ('Attack!')
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open('http://apps.facebook.com/bandbattle/battle/battle', input)
			self.count=self.count-1
			self.header=[]
			self.detector=0
class BandAttack(threading.Thread):
	opener=None
	hasRun=False
	def __init__(self, f):
		threading.Thread.__init__(self)
		self.opener=f
	def battle (self, lvl, count):
		maxRetry=random.randint(2, 4)
		while count>0 and maxRetry>0:
			print ('battling...')
			page = 'http://apps.facebook.com/bandbattle/battle'
			ap = BandAttackParser(self.opener, lvl, count)
			ap.feed(self.opener.open(page))
			ap.close()
			if count==ap.count:
				maxRetry=maxRetry-1
			count=ap.count
			time.sleep(random.randint(20, 30))
	def doJob(self):
		self.battle(random.randint(1,2), 4)
	def run (self):
		while True:
			errorFound=False
			try:
				self.opener.login()
				self.doJob()
				self.opener.logout()
			except:
				print ("Something went wrong: ", sys.exc_info()[0])
				errorFound=True
			print ('Going to sleep and wait for the next Job..')
			if errorFound:
				time.sleep(random.randint(20, 30))
			else:
				time.sleep(random.randint(480, 500))

if __name__ == "__main__":
	f = Facebook('bandbattle_attack.lwp')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	#Temp
	#time.sleep(2100)
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		bandbattle = BandAttack(f)
		bandbattle.doJob()
		f.logout()
	else:
		bandbattle = BandAttack(f)
		bandbattle.start()



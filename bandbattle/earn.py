import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *

appID='bandbattle'
class ActionParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	actionID=''
	action='/'+appID+'/quests/do_quest/'

	def __init__(self, opener, actionID):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
		self.actionID=actionID
		self.action='/'+appID+'/user_items/do/'+self.actionID
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			for a in attrs:
				if a[0]=='action' and a[1]==self.action:
					self.begin=True
		if self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Using action: '+self.actionID)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/'+appID+'/user_items/do/'+self.actionID, input)
			self.begin=False

class PurchaseParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	actionID=''
	actiontype=''
	quantity=''

	def __init__(self, opener, actionID, actionType, quantity):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
		self.actionID=actionID
		self.actionType=actionType
		self.quantity=quantity
	def handle_starttag(self, tag, attrs):
		if tag=='form' and attrs[0][1]=='/'+appID+'/user_items/'+self.actionType+'/'+self.actionID:
			self.begin=True
		if self.begin and tag=='input':
			if attrs[0]==('type', 'hidden'):
				self.header.append((attrs[1][1], attrs[2][1]))
		elif self.begin and tag=='select' and len(attrs)>2 and attrs[1]==('name', 'item[quantity]'):
			self.header.append((attrs[1][1], self.quantity))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Using action:',self.actionID,', type:',self.actionType,', quantity:',self.quantity)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/'+appID+'/user_items/'+self.actionType+'/'+self.actionID, input)
			self.begin=False

class BandBattle(JobRunner):
	opener=None
	hasRun=False
	requiredEnergy=150
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.jobDescription='Earn'
	def earn (self, actionID):
		print ('Earning (Band Battle) ...')
		page = Facebook.appsURL+'/'+appID+'/shows'
		ap = ActionParser(self.opener, actionID)
		ap.feed(self.opener.open(page))
		ap.close()
	def learn (self, actionID):
		print ('Learning (Band Battle) ...')
		page = Facebook.appsURL+'/'+appID+'/todo'
		ap = ActionParser(self.opener, actionID)
		ap.feed(self.opener.open(page))
		ap.close()
	def getEnergy (self):
		html=self.opener.open(Facebook.appsURL+'/'+appID+'/')
		m=re.compile("Energy: [0-9]+/").search(html)
		if m!=None:
			return int(re.compile('[0-9]+').search(m.group()).group())
		else:
			print ('Error! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			return self.getEnergy()
	def doJob(self):
		energy=self.getEnergy()
		print ('Current Energy: ', energy)
		while energy>=self.requiredEnergy:
			self.earn('6')
			energy=energy-self.requiredEnergy
		#Energy is regenerated 10(basic)+26(travel) every 5 minutes
		sleepTime=(self.requiredEnergy-energy)*5*60/36
		if sleepTime<240:
			sleepTime=240
		self.sleepTime=[sleepTime, sleepTime+20]

if __name__ == "__main__":
	f = Facebook(appID+'.lwp')
	opts, args = getopt.getopt(sys.argv[1:], "la:t:q:")
	runOnce=False
	transaction=False
	t_action=''
	t_type=''
	t_quantity=''
	for o, a in opts:
		if o == "-l":
			runOnce=True
		elif o == "-a":
			transaction=True
			t_action=a
		elif o == "-t":
			t_type=a
		elif o == "-q":
			t_quantity=a
	#Temp
	#time.sleep(10*60)
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		bandbattle = BandBattle(f)
		bandbattle.doJob()
		f.logout()
	elif transaction:
		print ('Doing transactions for once!')
		f.login()
		print (t_action, t_type, t_quantity)
		pp=PurchaseParser(f, t_action, t_type, t_quantity)
		pp.feed(f.open(f.opener.appsURL+'/'+appID+'/gear'))
		pp.close()
		f.logout()
	else:
		bandbattle = BandBattle(f)
		bandbattle.start()


import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *

appID='liloceanpatch'
class SellParser(HTMLParser):
	begin=False
	header=[]
	input=''
	action='/'+appID+'/marketplace/checkout'
	def __init__(self):
		HTMLParser.__init__(self)
		self.begin=False
		self.header=[]
		self.input=''
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			for a in attrs:
				if a[0]=='action' and a[1]==self.action and not self.begin:
					self.begin=True
					self.header=[]
		elif tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.header.append((name, value))
	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			self.input = urllib.parse.urlencode(self.header)
			self.begin=False
			self.header=[]

class JobMain(JobRunner):
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=appID
		self.jobDescription='Sell'
	def doJob(self):
		print ('Selling ('+appID+') ...')
		sp=SellParser()
		sp.feed(self.opener.open(Facebook.appsURL+'/'+appID+'/marketplace'))
		sp.close()
		html=self.opener.open(Facebook.appsURL+sp.action, sp.input)
		sp=SellParser()
		sp.action=sp.action+'/confirm'
		sp.feed(html)
		sp.close()
		self.opener.open(Facebook.appsURL+sp.action, sp.input)
		
		self.sleepTime=[24*3600, 48*3600]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	#Temp
	#time.sleep(15*60)
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		liloceanpatch = JobMain(f)
		liloceanpatch.doJob()
		f.logout()
	else:
		liloceanpatch = JobMain(f)
		liloceanpatch.start()




import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
from .common import *
import FBAuto.common.zynga_common

class VoteMain(FBAuto.common.zynga_common.VoteMain):
	game_config=config
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = VoteMain(f)
		job.doJob()
	else:
		job = VoteMain(f)
		job.start()

import sys
import getopt
import time
import random
import threading
import urllib.parse
from FBAuto.common.facebook import Facebook
from FBAuto.common.SessionController import *
from FBAuto.vampiresgame.common import *
from FBAuto.vampiresgame.search import *
import FBAuto.common.zynga_common
import FBAuto.common.JobRunner
import FBAuto.refresher.refresher

class AllocateOnce(threading.Thread):
	marker=None
	url=None
	def __init__ (self, marker, url):
		threading.Thread.__init__(self)
		self.marker=marker
		self.url=url
	def run (self):
		print ('Main allocating')
		self.marker.allocating=True
		self.marker.opener.open(self.url)
		self.marker.allocating=False
		print ('Main finished allocating')

class StatsAllocMain(FBAuto.common.JobRunner.JobRunner):
	game_config=config
	statsForm=None
	target=None
	skillsURL=None
	allocating=False
	supportThreads=None
	def __init__(self, f, target):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='stats'
		self.target=target
		self.allocating=False
		self.skillsURL='http://facebook6.vampires.zynga.com/skills-iframe.php?x=x&ajax=1&sf_xw_user_id=568123000&sf_xw_sig=2111b47f1b16399ed19794e6db794e72'
	def getForm(self):
		html = self.opener.open(self.skillsURL)
		self.statsForm = SimpleFormParser('stats-iframe', {}, {})
		self.statsForm.feed(html)
		self.statsForm.close()
	def runMainThread(self):
		self.statsForm.forms[0].header[self.target]='200'
		allocMain = AllocateOnce(self, self.skillsURL+'?'+urllib.parse.urlencode(self.statsForm.forms[0].header))
		allocMain.start()
	def runSupportThread(self):
		self.supportThreads=[]
		self.statsForm.forms[0].header[self.target]='10'

		for i in range(0, 100):
			reloader = FBAuto.refresher.refresher.Refresher(self.opener, [self.skillsURL+'?'+urllib.parse.urlencode(self.statsForm.forms[0].header)], 1)
			reloader.diff=1
			reloader.start()
			self.supportThreads.append(reloader)

		i=0
		while True:
			time.sleep(30)
			i=i+1

		for t in self.supportThreads:
			t.stopJob()
	def doJob(self):
		self.getForm()
		self.statsForm.forms[0].display()
		#self.runMainThread()
		#time.sleep(1)
		self.runSupportThread()

class FavorAllocMain(FBAuto.common.zynga_common.FavorAllocMain):
	game_config=config
if __name__ == "__main__":
	f = SessionController(Facebook('facebook', ('kym.lewis@live.com', '@vichet2')))
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	job = Searcher (f)
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job.doJob()
		f.logout()
	else:
		allThreads=[]
		job.getSig()
		for i in range(0, 5):
			reloader = FBAuto.refresher.refresher.Refresher(f, ['http://facebook6.vampires.zynga.com/skills-iframe.php?x=x&ajax=1&sf_xw_user_id='+f.thisUserID+'&sf_xw_sig='+job.sig+'&action=update&attack=10'], 1)
			reloader.diff=1
			reloader.start()
			allThreads.append(reloader)

		while True:
			job.getSig()
			for t in allThreads:
				t.url=['http://facebook6.vampires.zynga.com/skills-iframe.php?x=x&ajax=1&sf_xw_user_id='+f.thisUserID+'&sf_xw_sig='+job.sig+'&action=update&attack=10']

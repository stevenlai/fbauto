import re
import sys
import getopt
import time
import threading
import random
import urllib.parse
from FBAuto.common.JobRunner import *
from FBAuto.common.ProfileLoader import *
from FBAuto.common.facebook import Facebook
import FBAuto.vampiresgame.common
import FBAuto.common.zynga_common

class StatsIFrameLoader(GeneralProfileLoader):
	def __init__(self, f, game_config):
		profile_config = FBAuto.common.ProfileLoader.ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+'stats.php'
		#profile_config.keepRetry=False
		profile_config.meta=[('Energy', 'Energy[^0-9]*', None), ('Rage', 'Rage[^0-9]*', None), ('Attack', 'Attack[^0-9]*', None), ('Defense', 'Defense[^0-9]*', None), ('Health', 'Health[^0-9]*', None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
		self.dotAll=True

class SearchThread (threading.Thread):
	master=None
	def __init__(self, master):
		threading.Thread.__init__(self)
		self.master=master
	def run (self):
		pass

class Searcher (JobRunner):
	profile=None
	statsLoader=None
	sig=None
	searchSpace=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.sig=None
		self.profile=FBAuto.common.zynga_common.ProfileLoader(self.opener, FBAuto.vampiresgame.common.config)
		self.statsLoader=StatsIFrameLoader(self.opener, FBAuto.vampiresgame.common.config)
		#self.searchSpace=Enumerator()
	def getSig(self):
		html = self.profile.getUltimatePage(Facebook.appsURL+'/'+FBAuto.vampiresgame.common.config.appID+'/'+'stats.php')
		m = re.compile("local_xw_sig = '([0-9a-zA-Z]+)'").search(html)
		while m==None:
			self.opener.login()
			html = self.profile.getUltimatePage(Facebook.appsURL+'/'+FBAuto.vampiresgame.common.config.appID+'/'+'stats.php')
			m = re.compile("local_xw_sig = '([0-9a-zA-Z]+)'").search(html)
		self.sig=m.group(1)
		print('Found Sig: '+self.sig)
	def getData(self, param):
		html = self.opener.open('http://facebook6.vampires.zynga.com/skills-iframe.php?x=x&ajax=1&sf_xw_user_id='+self.opener.thisUserID+'&sf_xw_sig='+self.sig+'&action=update'+param)
		self.statsLoader.loadProfile(html)
		self.statsLoader.displayProfile()
	def doJob(self):
		self.getSig()
		self.getData('')

if __name__ == "__main__":
	f = Facebook('facebook', ('avamartin24@gmail.com', 'seethingmadbitch'))
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		searcher = Searcher(f)
		searcher.doJob()
		f.logout()
	else:
		pass

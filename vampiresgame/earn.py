import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
from .common import *
import FBAuto.common.zynga_common

class JobMain(FBAuto.common.zynga_common.JobMain):
	game_config=config
class QuickJob(FBAuto.common.zynga_common.QuickJob):
	game_config=config
class BossFight(FBAuto.common.zynga_common.BossFight):
	game_config=config
class TreasureMain(FBAuto.common.zynga_common.TreasureMain):
	game_config=config
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = JobMain(f)
		job.doJob()
	else:
		job = QuickJob(f)
		job.start()

import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
from .common import *

class InventoryMain(InventoryMain):
	game_config=config

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = InventoryMain(f)
		while True:
			job.doJob()
	else:
		job = InventoryMain(f)
		job.start()

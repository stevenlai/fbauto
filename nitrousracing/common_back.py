import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from html.parser import HTMLParseError
from FBAuto.common.ProfileLoader import *
from FBAuto.common.FormParser import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *
from FBAuto.common.facebook import Facebook

appID='nitrousracing'
class GameConfig:
	appID='nitrousracing'
	profileURL='loot'
	cashPrefix='Money:<[^>]*><[^>]*>\$'
	energyPrefix='Fuel:<[^>]*><[^>]*>'
	energyMidfix='/'
	timePrefix='Next Race:</td><td class="hd2">'
	timeMidfix=':'
	expPrefix='Experience:<[^>]*><[^>]*>'
	itemPrefix='Item Inventory \('
	itemMidfix='/'
	skillPrefix='<td[^>]*style="font-weight: bold;">'
	ajax={'type':'2', 'require_login':'true'}
	minUpgradeMoney=10000
	energyRechargeTime=240
config=GameConfig()
class InventoryParser(HTMLParser):
	inventoryList=[]
	activeInventory=[]
	progress=0
	currentItem=None
	def __init__ (self, game_config):
		HTMLParser.__init__(self)
		self.inventoryList=[]
		self.activeInventory=[]
		self.currentItem=None
		self.progress=0
	def handle_starttag(self, tag, attrs):
		if tag.lower()=='div' and (self.progress==1 or self.progress==4):
			for a in attrs:
				if a[0].lower()=='style' and a[1]=='font-size: 14px; font-weight: bold;':
					self.progress=self.progress+1
		elif tag.lower()=='a' and self.progress==3:
			for a in attrs:
			 	if a[0].lower()=='href':
					self.currentItem.append(a[1].replace('&amp;', '&'))
					self.progress=1
		elif tag.lower()=='a' and self.progress==1:
			for a in attrs:
			 	if a[0].lower()=='href':
					self.currentItem.append(a[1].replace('&amp;', '&'))
				
	def handle_endtag(self, tag):
		pass
	def handle_data(self, data):
		if re.compile('Item Inventory').search(data)!=None and self.progress==0:
			self.progress=1
		elif self.progress==2:
			if self.currentItem!=None:
				self.inventoryList.append(self.currentItem)
			self.currentItem=[]
			self.currentItem.append(data)
			self.progress=3
		elif self.progress==5:
			self.activeInventory.append(data)
			self.progress=4
		elif re.compile('Active Effects').search(data)!=None:
			if self.currentItem!=None:
				self.inventoryList.append(self.currentItem)
			self.progress=4
	def displayItems(self):
		inventoryStr='Inventory list: ['
		for i in self.inventoryList:
			if len(i)==2:
				inventoryStr=inventoryStr+i[0]+'('+i[1]+'), '
			else:	
				inventoryStr=inventoryStr+i[0]+'('+i[1]+', '+i[2]+'), '
		inventoryStr=inventoryStr+']'
		print (inventoryStr)
		print ('Active effect:',self.activeInventory)
class InventoryConfig:
	action=''
	name=''
	arg=''
	def __str__ (self):
		return self.name+'('+self.action+'):'+self.arg
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	inventory=None
	inventoryConfigList={}
	skillInfo=[['Drifting', '1', 0], ['Passing', '2', 0], ['Speed and Control', '3', 0], ['Reaction and Response', '4', 0], ['Recovery', '5', 0]]
	carInfo=[['7', 100075, [15000, 15000, 5000, 0, 0]], ['8', 118775, [0, 0, 10000, 22000, 8000]], ['9', 200000, [18000, 0, 7000, 0, 20000]], ['10', 200575, [0, 25000, 20000, 5000, 0]], ['11', 250000, [0, 0, 5000, 30000, 25000]], ['12', 265000, [60000, 5000, 5000, 0, 0]], ['13', 318845, [0, 30000, 40000, 10000, 0]], ['14', 325200, [0, 15000, 0, 15000, 60000]], ['15', 343585, [25000, 25000, 50000, 0, 0]], ['16', 1250000, [40000, 45000, 0, 0, 35000]], ['32', 595000, [31000, 0, 50000, 45000, 0]], ['33', 650000, [100000, 100000, 100000, 100000, 100000]]]
	currentCar=None
	racePos='8'
	specialQuestMoney=0
	specialQuestURL=''
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		self.currentCar=None
		self.inventory=InventoryParser(self.game_config)
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		self.racePos='8'
		self.specialQuestMoney=0
		self.specialQuestURL=''
		self.inventoryConfigList={}
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID +'/index.php?c='+game_config.profileURL
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Cash', game_config.cashPrefix, None), ('Exp', game_config.expPrefix, None), ('Items', game_config.itemPrefix, game_config.itemMidfix), ('Time', game_config.timePrefix, game_config.timeMidfix)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		self.loadItems()
	def displayProfile(self):
		GeneralProfileLoader.displayProfile(self)
		self.inventory.displayItems()
	def loadItems(self):
		self.inventory=InventoryParser(self.game_config)
		self.inventory.feed(self.page)
		self.inventory.close()
	def grabLoot(self, html):
		if re.compile('Pick Up').search(html)!=None:
			print ('Picking up loot...')
			ajax = AjaxProcessor()
			ajax.ajax_url_suffix='grabloot'
			ajax.process(html)
			ajax.getHeader().update(self.game_config.ajax)
			ajax.post(self.opener)
	def readConfig(self):
		d = self.opener.getConfigFile(self.game_config.appID+'.txt')
		f = open(d, 'r')
		m=re.compile('[0-9]+').search(f.readline())
		if m!=None:
			self.racePos=m.group()

		m=re.compile('^special\|([0-9]+)\|(.*)').search(f.readline())
		if m!=None:
			self.specialQuestMoney=int(m.group(1))
			self.specialQuestURL=m.group(2)

		self.carInfo=[]
		m=re.compile('([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)').search(f.readline())
		while m!=None:
			self.carInfo.append([m.group(1), int(m.group(2)), [int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)), int(m.group(7))]])
			m=re.compile('([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)').search(f.readline())

		self.inventoryConfigList={}
		m=re.compile('([a-z]+) (.*)').search(f.readline())
		while m!=None:
			inv_config=InventoryConfig()
			inv_config.name=m.group(2)
			inv_config.action=m.group(1)
			m2=re.compile('(.*)\|(.*)').search(inv_config.name)
			if m2!=None:
				inv_config.name=m2.group(1)
				inv_config.arg=m2.group(2)
			self.inventoryConfigList[inv_config.name]=inv_config
			m=re.compile('([a-z]+) (.*)').search(f.readline())
		f.close()
		#for k,v in self.inventoryConfigList.items():
			#print (k,v)
	def loadCarInfo(self, html):
		self.currentCar = None
		m = re.compile('index\.php\?c=dealer&amp;buy=([0-9]+)').search(html)
		if m!=None:
			for c in self.carInfo:
				if c[0]==m.group(1):
					self.currentCar=c
					break;
	def loadSkillInfo(self, html):
		iterator = re.compile(self.game_config.skillPrefix+'([0-9,]+)').finditer(html)
		i=0
		for m in iterator:
			self.skillInfo[i][2]=int(m.group(1).replace(',',''))
			i=i+1
		self.displaySkill()
	def displayCarInfo(self):
		for i in self.carInfo:
			print (i[0], i[1], i[2][0], i[2][1], i[2][2], i[2][3], i[2][4])
	def displaySkill(self):
		skillStr='Skill list: ['
		for i in self.skillInfo:
			skillStr=skillStr+i[0]+'('+i[1]+'): '+str(i[2])+', '
		skillStr=skillStr+']'
		print (skillStr)
def compareSkill(a, b):
	return cmp(a[2], b[2])
class JobMain(JobRunner):
	game_config=GameConfig()
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Job'
		self.profile=ProfileLoader(f, self.game_config)
	def doJob(self):
		self.profile.readConfig()
		print ('Racing at:',self.profile.racePos)
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=casting&location='+self.profile.racePos)
		self.profile.loadProfile(html)
		self.profile.grabLoot(html)
		inv=InventoryMain(self.opener)
		inv.game_config=self.game_config
		inv.doJob()
		self.profile.loadProfile()
		sleepTime = self.profile.data["Time"]*60+self.profile.data["TimeMax"]
		self.sleepTime=[sleepTime, sleepTime+1]
class FightMain(JobRunner):
	profile=None
	member=0
	game_config=GameConfig()
	friends=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.member=0
		self.appID=self.game_config.appID
		self.friends=[]
		self.jobDescription='Fight'
		self.profile=ProfileLoader(f, self.game_config)
	def loadFriends(self):
		self.friends=[]
		w = WhiteListLoader(self.opener)
		self.friends=self.friends+w.whiteList
		friend_links = LinkSearcher(self.opener, 'index\.php\?c=profile&amp;id=[0-9]+')
		friend_links.callback=None
		friend_links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=recruit&viewall=1'
		friend_links.load('')
		for l in friend_links.linkList:
			m=re.compile('[0-9]+').search(l)
			if m!=None:
				self.friends.append(m.group())
		print ('Friends ('+self.game_config.appID+', '+str(len(self.friends))+'):', self.friends)
	def doJob(self):
		self.loadFriends()
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=attack')
		self.profile.loadProfile(html)
		m=re.compile('showPinkSlip\(([0-9]+)\)').search(html)
		if m!=None:
			print ('Attack freshmeat')
			self.profile.grabLoot(self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=attack&rcuid='+m.group(1)+'&invited=1'))
		self.fight('index.php?c=attack', 'index\.php\?c=attack[^"]+', False)
		#Don't do inventory in fight. It's too slow
		#inv=InventoryMain(self.opener)
		#inv.game_config=self.game_config
		#inv.doJob()
		#self.profile.loadProfile()
		self.updateSleepTime()
	def fight(self, mainpage, linkPattern, fightFriends):
		print ('Fighting: '+mainpage)
		links = LinkSearcher(self.opener, linkPattern)
		links.callback=None
		links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+mainpage
		links.load('')

		for l in links.linkList:
			if self.profile.data['Energy']<=self.profile.data['EnergyMax']/2:
				break
			decoded_l=l.replace('&amp;','&')
			m=re.compile('who=[0-9]+').search(decoded_l)
			if m!=None:
				id=re.compile('[0-9]+').search(m.group()).group()
				if fightFriends or self.friends.count(id)==0:
					print ('Fighting: '+decoded_l)
					html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+decoded_l)
					self.profile.grabLoot(html)
					self.profile.loadProfile(html)
				else:
					print ('Do not fight friends: '+decoded_l)

	def updateSleepTime(self):
		sleepTime=self.game_config.energyRechargeTime*(self.profile.data['EnergyMax']-self.profile.data['Energy'])
		if sleepTime>=self.game_config.energyRechargeTime/2:
			sleepTime=sleepTime-self.game_config.energyRechargeTime/2
		else:
			sleepTime=0

		self.sleepTime=[sleepTime, sleepTime+self.game_config.energyRechargeTime/2]
		#Pink slip shows up every 20 min
		self.sleepTime=[20*60, 21*60]
class InventoryMain(JobRunner):
	profile=None
	game_config=GameConfig()
	inventoryConfigList={}
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.profile=ProfileLoader(f, self.game_config)
		self.inventoryConfigList={}
	def doJob(self):
		self.profile.readConfig()
		self.inventoryConfigList=self.profile.inventoryConfigList
		self.profile.loadProfile()
		self.profile.displayProfile()
		self.processInventory()
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=dealer')
		self.profile.loadProfile(html)
		self.profile.loadCarInfo(html)
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=upgrademember&self=1')
		self.profile.loadSkillInfo(html)
		self.upgrade(html)
		self.sleepTime=[3600, 3600*2]
	def processInventory (self):
		counters={}
		allinventory=self.profile.inventory.inventoryList
		for i in allinventory:
			cur_inventory=InventoryConfig()
			if not i[0] in self.inventoryConfigList:
				continue
			else:
				cur_inventory=self.inventoryConfigList[i[0]]
			
			if cur_inventory.name in counters:
				counters[cur_inventory.name]=counters[cur_inventory.name]+1
			else:
				counters[cur_inventory.name]=1
			#print ('Count for '+cur_inventory.name+':', counters[cur_inventory.name])

			if cur_inventory.action=='use':
				if self.profile.inventory.activeInventory.count(cur_inventory.name)==0 and counters[cur_inventory.name]==1:
					print ('To use ('+cur_inventory.name+'):', i[1])
					self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+i[1])
				elif counters[cur_inventory.name]>int(cur_inventory.arg):
					print ('To sell ('+cur_inventory.name+'):', i[2])
					self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+i[2])
			elif cur_inventory.action=='keep' and counters[cur_inventory.name]>int(cur_inventory.arg):
				print ('To sell ('+cur_inventory.name+'):', i[2])
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+i[2])
			elif cur_inventory.action=='job':
				print ('To do job ('+cur_inventory.name+'):', cur_inventory.arg)
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+ cur_inventory.arg)
	def upgrade (self, html):
		skillID=''
		print ('Current car:', self.profile.currentCar)
		if self.profile.currentCar!=None:
			i=0
			for i in range(0, len(self.profile.skillInfo)):
				if self.profile.currentCar[2][i]>self.profile.skillInfo[i][2]:
					skillID=self.profile.skillInfo[i][1]
					break
		else:
			skill_sorted=sorted(self.profile.skillInfo, compareSkill)
			print (skill_sorted)
			skillID=skill_sorted[0][1]

		specialQuestURLPattern=self.profile.specialQuestURL.replace('&', '&amp;').replace('.', '\.').replace('?', '\?')
		if specialQuestURLPattern!='' and re.compile(specialQuestURLPattern).search(self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'index.php?c=quests'))!=None:
			print ('Special quest found!', self.profile.specialQuestURL)
			if self.profile.data["Cash"]>=self.profile.specialQuestMoney:
				print ('Doing special Quest!')
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.profile.specialQuestURL)
			else:
				print ('Waiting for special Quest!')
		elif skillID!='':
			if self.profile.data["Cash"]>self.game_config.minUpgradeMoney:
				points=self.profile.data["Cash"]/self.game_config.minUpgradeMoney*self.game_config.minUpgradeMoney/100
				print ('Skill to be upgraded:', skillID, 'Total points:', points)
				fp = SimpleFormParser('index.php?c=upgrademember&who=[0-9]+&buy=1&weapon='+skillID, {'amount':str(points)}, {})
				fp.feed(html)
				fp.close()
				if (len(fp.forms)>0):
					fp.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+fp.forms[0].action)
		elif self.profile.data["Cash"]>=self.profile.currentCar[1]:
			buying_url='index.php?c=dealer&buy='+self.profile.currentCar[0]
			print ('Buying car ('+self.profile.currentCar[0]+'):',buying_url)
			self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+buying_url)
if __name__ == "__main__":
	f = Facebook(config.appID+'.lwp')

	p=ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()

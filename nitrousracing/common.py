import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from html.parser import HTMLParseError
from FBAuto.common.ProfileLoader import *
from FBAuto.common.FormParser import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *
import FBAuto.common.prisonlockdown_common
from FBAuto.common.facebook import Facebook

appID='nitrousracing'
class GameConfig:
	appID='nitrousracing'
	cashPrefix='<td [^>]*money[^>]*>\$'
	energyPrefix='<td [^>]*userFuel[^>]*>'
	energyMidfix='/'
	timePrefix='startRaceTimer[ ]*\('
	friendURL='buddies'
	expPrefix='<td [^>]*levelExp[^>]*>'
	itemPrefix='<img[^>]*itemsGear[^>]*>[ ]*\('
	itemMidfix='/'
	skillPrefix='<td[^>]*style="font-weight: bold;">'
	lootStr='Pick Up'
	ajax={'type':'2', 'require_login':'true'}
	energyRechargeTime=240
config=GameConfig()
class InventoryParser(HTMLParser):
	inventoryList=[]
	activeInventory=[]
	progress=0
	currentItem=None
	def __init__ (self, game_config):
		HTMLParser.__init__(self)
		self.inventoryList=[]
		self.activeInventory=[]
		self.currentItem=None
		self.progress=0
	def handle_starttag(self, tag, attrs):
		if tag.lower()=='img' and self.progress==0:
			for a in attrs:
				if a[0].lower()=='src' and re.compile('itemsGear').search(a[1])!=None:
					self.progress=1
		elif tag.lower()=='img':
			for a in attrs:
				if a[0].lower()=='src' and re.compile('itemActiveEffects').search(a[1])!=None:
					if self.currentItem!=None:
						self.inventoryList.append(self.currentItem)
					self.progress=4
		elif tag.lower()=='div' and (self.progress==1 or self.progress==4):
			for a in attrs:
				if a[0].lower()=='style' and a[1]=='font-size: 14px; font-weight: bold;':
					self.progress=self.progress+1
		elif tag.lower()=='a' and self.progress==3:
			for a in attrs:
				if a[0].lower()=='onclick':
					m=re.compile("loot[^']*").search(a[1])
					if m!=None:
						self.currentItem.append(m.group().replace('&amp;', '&'))
					self.progress=1
		elif tag.lower()=='a' and self.progress==1:
			for a in attrs:
				if a[0].lower()=='onclick':
					m=re.compile("loot[^']*").search(a[1])
					if m!=None:
						self.currentItem.append(m.group().replace('&amp;', '&'))
				
	def handle_endtag(self, tag):
		if tag.lower()=='div' and self.progress==10:
			self.progress=3
		elif tag.lower()=='div' and self.progress==2:
			if self.currentItem!=None:
				self.inventoryList.append(self.currentItem)
			self.currentItem=[]
			self.currentItem.append('')
			self.progress=3
	def handle_data(self, data):
		#if re.compile('Item Inventory').search(data)!=None and self.progress==0:
			#self.progress=1
		if self.progress==2:
			if self.currentItem!=None:
				self.inventoryList.append(self.currentItem)
			self.currentItem=[]
			self.currentItem.append(data)
			self.progress=10
		elif self.progress==10:
			self.currentItem[0]=self.currentItem[0]+' '+data
		elif self.progress==5:
			self.activeInventory.append(data)
			self.progress=4
		#elif re.compile('Active Effects').search(data)!=None:
			#if self.currentItem!=None:
				#self.inventoryList.append(self.currentItem)
			#self.progress=4
	def displayItems(self):
		inventoryStr='Inventory list: ['
		for i in self.inventoryList:
			if len(i)==2:
				inventoryStr=inventoryStr+i[0]+'('+i[1]+'), '
			else:	
				inventoryStr=inventoryStr+i[0]+'('+i[1]+', '+i[2]+'), '
		inventoryStr=inventoryStr+']'
		print (inventoryStr)
		print ('Active effect:',self.activeInventory)
class InventoryConfig:
	action=''
	name=''
	arg=''
	def __str__ (self):
		return self.name+'('+self.action+'):'+self.arg
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	util=None
	inventory=None
	inventoryConfigList={}
	jobList=[]
	cars=[]
	skillInfo=[['Drifting', '1', 0], ['Passing', '2', 0], ['Speed and Control', '3', 0], ['Reaction and Response', '4', 0], ['Recovery', '5', 0]]
	carInfo=[]
	currentCar=None
	racePos='8'
	minCash=0
	minUpgradeMoney=0
	specialQuestMoney=0
	specialQuestURL=''
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		self.currentCar=None
		self.inventory=InventoryParser(self.game_config)
		profile_config = ProfileConfig()
		self.util=FBAuto.common.prisonlockdown_common.Util(self.opener, game_config)
		profile_config.appID=game_config.appID
		self.racePos='8'
		self.minCash=0
		self.minUpgradeMoney=0
		self.specialQuestMoney=0
		self.specialQuestURL=''
		self.inventoryConfigList={}
		self.jobList=[]
		self.cars=[]
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID +'/'
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Cash', game_config.cashPrefix, None), ('Exp', game_config.expPrefix, None), ('Items', game_config.itemPrefix, game_config.itemMidfix), ('Time', game_config.timePrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def filterPage(self, html):
		return html.replace('&#039;',"'")
	def loadProfile (self, html=None):
		if html==None:
			html=self.util.openLink('loot')
		GeneralProfileLoader.loadProfile(self, html)
		self.loadItems()
	def displayProfile(self):
		GeneralProfileLoader.displayProfile(self)
		self.inventory.displayItems()
	def loadItems(self):
		self.inventory=InventoryParser(self.game_config)
		self.inventory.feed(self.page.replace('&#039;',"'"))
		self.inventory.close()
	def readConfig(self):
		d = self.opener.getConfigFile(self.game_config.appID+'.txt')
		f = open(d, 'r')
		m=re.compile('[0-9]+').search(f.readline())
		if m!=None:
			self.racePos=m.group()

		m=re.compile('([0-9]+),([0-9]+)').search(f.readline())
		if m!=None:
			self.cars.append(m.group(1))
			self.cars.append(m.group(2))

		m=re.compile('[0-9,]+').search(f.readline())
		if m!=None:
			self.minCash=int(m.group().replace(',',''))

		m=re.compile('[0-9,]+').search(f.readline())
		if m!=None:
			self.minUpgradeMoney=int(m.group().replace(',',''))

		m=re.compile('^special\|([0-9]+)\|(.*)').search(f.readline())
		if m!=None:
			self.specialQuestMoney=int(m.group(1))
			self.specialQuestURL=m.group(2)

		self.carInfo=[]
		m=re.compile('([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)').search(f.readline())
		while m!=None:
			self.carInfo.append([m.group(1), int(m.group(2)), [int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)), int(m.group(7))]])
			m=re.compile('([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)').search(f.readline())

		self.inventoryConfigList={}
		m=re.compile('([a-z]+) (.*)').search(f.readline())
		while m!=None:
			inv_config=InventoryConfig()
			inv_config.name=m.group(2)
			inv_config.action=m.group(1)
			m2=re.compile('(.*)\|(.*)').search(inv_config.name)
			if m2!=None:
				inv_config.name=m2.group(1)
				inv_config.arg=m2.group(2)
			self.inventoryConfigList[inv_config.name]=inv_config
			m=re.compile('([a-z]+) (.*)').search(f.readline())
		#for k,v in self.inventoryConfigList.items():
			#print (k,v)

		self.jobList=[]
		m=re.compile('([^\|]+)\|([^\|]+)\|([0-9]+)').search(f.readline())
		while m!=None:
			self.jobList.append((m.group(1), m.group(2), int(m.group(3))))
			m=re.compile('([^\|]+)\|([^\|]+)\|([0-9]+)').search(f.readline())
		#print (self.jobList)

		f.close()
	def loadCarInfo(self, html):
		self.currentCar = None
		m = re.compile('dealer&amp;buy=([0-9]+)').search(html)
		if m!=None:
			for c in self.carInfo:
				if c[0]==m.group(1):
					self.currentCar=c
					break;
	def loadSkillInfo(self, html):
		iterator = re.compile(self.game_config.skillPrefix+'([0-9,]+)').finditer(html)
		i=0
		for m in iterator:
			self.skillInfo[i][2]=int(m.group(1).replace(',',''))
			i=i+1
		self.displaySkill()
	def displayCarInfo(self):
		for i in self.carInfo:
			print (i[0], i[1], i[2][0], i[2][1], i[2][2], i[2][3], i[2][4])
	def displaySkill(self):
		skillStr='Skill list: ['
		for i in self.skillInfo:
			skillStr=skillStr+i[0]+'('+i[1]+'): '+str(i[2])+', '
		skillStr=skillStr+']'
		print (skillStr)
def compareSkill(a, b):
	return cmp(a[2], b[2])
class JobMain(JobRunner):
	game_config=GameConfig()
	profile=None
	counter=0
	maxCounter=0
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Job'
		self.profile=ProfileLoader(f, self.game_config)
		self.resetCounter()
	def resetCounter(self):
		self.counter=0
		self.maxCounter=random.randint(1, 3)
	def checkJobs(self):
		self.counter=self.counter+1
		if self.counter<self.maxCounter:
			return
		html=self.profile.util.openLink('quests&repeatable=1')
		for job in self.profile.jobList:
			if re.compile(job[0]+'[^<]*<span style="">').search(html)!=None and self.profile.data["Energy"]>=job[2]:
				print ('Doing job:', job)
				self.profile.util.openLink(job[1])
		self.resetCounter()
	def doRace(self):
		print ('Racing at:',self.profile.racePos)
		html=self.profile.util.openLink('casting')
		m=re.compile('casting&amp;location='+self.profile.racePos+'&amp;racetime=[0-9]+&amp;racekey=[0-9a-zA-Z]+').search(html)
		if m!=None:
			html=self.profile.util.openLink(m.group().replace('&amp;','&'))
		self.profile.loadProfile(html)
		self.profile.util.grabLoot(html)
	def processInventory(self):
		inv=InventoryMain(self.opener)
		inv.game_config=self.game_config
		inv.doJob()
		self.profile=inv.profile
	def doJob(self):
		self.profile.readConfig()
		self.doRace()
		self.checkJobs()
		self.processInventory()
		#sleepTime = self.profile.data["Time"]*60+self.profile.data["TimeMax"]
		sleepTime = self.profile.data["Time"]
		self.sleepTime=[sleepTime, sleepTime+1]
class FightMain(FBAuto.common.prisonlockdown_common.FightMain):
	profile=None
	member=0
	game_config=GameConfig()
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.member=0
		self.appID=self.game_config.appID
		self.jobDescription='Fight'
		self.profile=ProfileLoader(f, self.game_config)
	def doJob(self):
		self.loadFriends()
		html=self.profile.util.openLink('attack')
		self.profile.loadProfile(html)
		self.profile.readConfig()

		iter=re.compile('getAttack\(([0-9]+),([0-9\-]+),([a-zA-Z]+)\)').finditer(html)
		if self.profile.cars[0]!=self.profile.cars[1]:
			self.profile.util.openLink('garage&setPrimary='+self.profile.cars[1])
		for m in iter:
			if self.profile.data['Energy']<=self.profile.data['EnergyMax']/2:
				break
			if self.friends.count(m.group(1))>0:
				print ('Do NOT fight friend:', m.group())
				continue

			if (m.group(3).lower()=='false'):
				urlEnd='&who='+m.group(1)+'&tcid='+m.group(2)
			else:
				urlEnd='&rcuid='+m.group(1)+'&tcid='+m.group(2)
			print ('Fighting:', m.group())

			ajax = AjaxProcessor()
			ajax.var.update({'a[0-9]+_codeBase': None, 'a[0-9]+_urlEnd':urlEnd})
			ajax.ajax_url_suffix=urlEnd
			ajax.process(html)
			header = ajax.getHeader()
			header.update(self.game_config.ajax)
			#print (ajax.header)
			ajax_html=ajax.post(self.opener)

			html=self.profile.util.openLink('attack')+'<!--'+ajax_html+'-->'
			self.profile.util.grabLoot(html)
			self.profile.loadProfile(html)

		if self.profile.cars[0]!=self.profile.cars[1]:
			self.profile.util.openLink('garage&setPrimary='+self.profile.cars[0])

		self.updateSleepTime()
	def doJob2(self):
		self.loadFriends()
		html=self.profile.util.openLink('attack')
		self.profile.loadProfile(html)
		m=re.compile('showPinkSlip\(([0-9]+)\)').search(html)
		if m!=None:
			print ('Attack freshmeat')
			self.profile.util.grabLoot(self.profile.util.openLink('attack&rcuid='+m.group(1)+'&invited=1'))
		self.fight('attack', "attack&amp;[^'\"]+", False)
		self.updateSleepTime()
	def fight(self, mainpage, linkPattern, fightFriends):
		print ('Fighting: '+mainpage)
		links=set(re.compile(linkPattern).findall(self.profile.util.openLink(mainpage)))
		for l in links:
			if self.profile.data['Energy']<=self.profile.data['EnergyMax']/2:
				break
			decoded_l=l.replace('&amp;','&')
			m=re.compile('who=[0-9]+').search(decoded_l)
			if m!=None:
				id=re.compile('[0-9]+').search(m.group()).group()
				if fightFriends or self.friends.count(id)==0:
					print ('Fighting: '+decoded_l)
					html=self.profile.util.openLink(decoded_l)
					self.profile.util.grabLoot(html)
					self.profile.loadProfile(html)
				else:
					print ('Do not fight friends: '+decoded_l)

	def updateSleepTime(self):
		sleepTime=self.game_config.energyRechargeTime*(self.profile.data['EnergyMax']-self.profile.data['Energy'])
		if sleepTime>=self.game_config.energyRechargeTime/2:
			sleepTime=sleepTime-self.game_config.energyRechargeTime/2
		else:
			sleepTime=0

		self.sleepTime=[sleepTime, sleepTime+self.game_config.energyRechargeTime/2]
		#Pink slip shows up every 20 min
		self.sleepTime=[20*60, 21*60]
class InventoryMain(JobRunner):
	profile=None
	game_config=GameConfig()
	inventoryConfigList={}
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.profile=ProfileLoader(f, self.game_config)
		self.inventoryConfigList={}
	def doJob(self):
		self.profile.readConfig()
		self.inventoryConfigList=self.profile.inventoryConfigList
		self.profile.loadProfile()
		self.profile.displayProfile()
		self.processInventory()
		if self.profile.data["Cash"]-self.profile.minCash>self.profile.minUpgradeMoney:
			html=self.profile.util.openLink('dealer')
			self.profile.loadProfile(html)
			self.profile.loadCarInfo(html)
			html=self.profile.util.openLink('upgrademember&self=1', html)
			#self.profile.loadSkillInfo(html)
			self.profile.loadProfile(html)
			#self.upgrade(html)
		self.sleepTime=[3600, 3600*2]
	def processInventory (self):
		counters={}
		allinventory=self.profile.inventory.inventoryList
		for i in allinventory:
			cur_inventory=InventoryConfig()
			if not i[0] in self.inventoryConfigList:
				continue
			else:
				cur_inventory=self.inventoryConfigList[i[0]]
			
			if cur_inventory.name in counters:
				counters[cur_inventory.name]=counters[cur_inventory.name]+1
			else:
				counters[cur_inventory.name]=1
			#print ('Count for '+cur_inventory.name+':', counters[cur_inventory.name])

			if cur_inventory.action=='use':
				if self.profile.inventory.activeInventory.count(cur_inventory.name)==0 and counters[cur_inventory.name]==1:
					print ('To use ('+cur_inventory.name+'):', i[1])
					self.profile.util.openLink(i[1])
				elif counters[cur_inventory.name]>int(cur_inventory.arg):
					print ('To sell ('+cur_inventory.name+'):', i[2])
					self.profile.util.refreshLink(i[2])
			elif cur_inventory.action=='keep' and counters[cur_inventory.name]>int(cur_inventory.arg):
				print ('To sell ('+cur_inventory.name+'):', i[2])
				self.profile.util.refreshLink(i[2])
			elif cur_inventory.action=='job':
				print ('To do job ('+cur_inventory.name+'):', cur_inventory.arg)
				self.profile.util.openLink(cur_inventory.arg)
	def upgrade (self, html):
		skillID=''
		print ('Current car:', self.profile.currentCar)
		if self.profile.currentCar!=None:
			i=0
			for i in range(0, len(self.profile.skillInfo)):
				if self.profile.currentCar[2][i]>self.profile.skillInfo[i][2]:
					skillID=self.profile.skillInfo[i][1]
					break
		else:
			skill_sorted=sorted(self.profile.skillInfo, compareSkill)
			print (skill_sorted)
			skillID=skill_sorted[0][1]

		specialQuestURLPattern=self.profile.specialQuestURL.replace('&', '&amp;').replace('.', '\.').replace('?', '\?')
		if specialQuestURLPattern!='' and re.compile(specialQuestURLPattern).search(self.profile.util.openLink('quests'))!=None:
			print ('Special quest found!', self.profile.specialQuestURL)
			if self.profile.data["Cash"]>=self.profile.specialQuestMoney:
				print ('Doing special Quest!')
				self.profile.util.openLink(self.profile.specialQuestURL)
			else:
				print ('Waiting for special Quest!')
		elif skillID!='':
			if self.profile.data["Cash"]-self.profile.minCash>self.profile.minUpgradeMoney:
				points=(self.profile.data["Cash"]-self.profile.minCash)/self.profile.minUpgradeMoney*self.profile.minUpgradeMoney/100
				print ('Skill to be upgraded:', skillID, 'Total points:', points)
				fp = SimpleFormParser('index.php?c=upgrademember&who=[0-9]+&buy=1&weapon='+skillID, {'amount':str(points)}, {})
				fp.feed(html)
				fp.close()
				if (len(fp.forms)>0):
					html=fp.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+fp.forms[0].action)
					self.profile.loadProfile(html)
		elif self.profile.data["Cash"]>=self.profile.currentCar[1]:
			buying_url='dealer&buy='+self.profile.currentCar[0]
			print ('Buying car ('+self.profile.currentCar[0]+'):',buying_url)
			self.profile.util.openLink(buying_url)
if __name__ == "__main__":
	f = Facebook('facebook')

	p=ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()
	p.readConfig()

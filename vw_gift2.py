import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampiresgame.common
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite

cafe_config=FBAuto.common.zynga_common.GameConfig()
cafe_config.appID='cafeworld'
cafe_config.useAjax=False
cafe_config.energyPrefix='Free Gifts'
#gift_list=[['1833',10], ['1834',10], ['1835',10], ['1836',10]]
gift_list=[['814',1]]
game_config=cafe_config

#gift_list=[['247',1]]
#game_config=FBAuto.vampiresgame.common.config

def compareGift(g1, g2):
	return cmp(g1[1], g2[1])
class GiftSender(threading.Thread):
	giftID=''
	senders=[]
	realSenderIdx=0
	profiles=None
	allThreads=None
	#inviteURLTemplate=Facebook.appsURL+'/vampiresgame'+'/recruit_gift.php?giftId='+'XXXXXXXXXX'+'&action=recruit_gift_friends'
	#canvasURL='vampiresgame'
	inviteURLTemplate=Facebook.appsURL+'/cafeworld'+'/send_gift.php?view=cafe&k=free_gift&p=tab_send_free_gift&gid=XXXXXXXXXX'
	canvasURL='cafeworld'
	lastURL=None
	toSend=True
	total=10
	def __init__ (self, giftID, senders):
		threading.Thread.__init__(self)
		self.giftID=giftID
		self.senders=senders
		self.profiles=[]
		self.allThreads=[]
		self.lastURL=None
		self.toSend=True
		for opener in self.senders:
			self.profiles.append(FBAuto.common.zynga_common.ProfileLoader(opener, game_config))
	def run(self):
		while self.toSend and not self.send():
			self.send()
		if self.total>1:
			for i in range (0, self.total-1):
				self.oneClickAccept()
	def oneClickAccept(self):
		#self.senders[self.realSenderIdx].refresh(self.lastURL[self.realSenderIdx][0], self.lastURL[self.realSenderIdx][1])
		for i in range(0, len(self.senders)):
			if i!=self.realSenderIdx:
				self.senders[i].refresh(self.lastURL[i][0], self.lastURL[i][1])
	def createAjaxThreads(self, profileIdx, url):
		print ("*****************Creating refreshing threads for link: "+url)
		for i in range(0, 1):
			refresher=FBAuto.refresher.refresher.RefresherWithParam(self.profiles[profileIdx].opener, [(url, None)], 1)
			refresher.diff=1
			refresher.display_msg=False
			refresher.start()
			self.allThreads.append(refresher)
	def initAjax(self, inviteParser):
			print("*****Initializing Ajax*****")
			for i in range(0, len(self.senders)):
				if i!=self.realSenderIdx:
					inviteParser.ip.header.append(('ids[]', self.senders[i].thisUserID))
				if not self.profiles[i].ajaxReady():
					self.profiles[i].openForAjax('')
				self.lastURL.append(None)
			print("*****Finished Initializing Ajax*****")
	def send(self):
		self.lastURL=[]

		sender=self.senders[self.realSenderIdx]
		print (sender.opener.email+' is giving gift', self.giftID)
		inviteParser = FBAuto.common.autoinvite.JobMain(sender)
		html = self.profiles[self.realSenderIdx].getUltimatePage(self.inviteURLTemplate.replace('XXXXXXXXXX',self.giftID))
		inviteParser.parseInviteHTML(self.canvasURL, html)

		print (inviteParser.ip.action, len(inviteParser.ip.header))
		#print (inviteParser.ip.content)
		m = re.compile("url=['\"]([^'\"]*)['\"]").search(inviteParser.ip.content)
		if m!=None:
			self.initAjax(inviteParser)
			if inviteParser.ip.isGet:
				sendURL,tmpPage=self.profiles[self.realSenderIdx].fetchIFrame(inviteParser.ip.action, self.senders[self.realSenderIdx].open(inviteParser.ip.action+'?'+urllib.parse.urlencode(inviteParser.ip.header)))
				self.lastURL[self.realSenderIdx]=(inviteParser.ip.action+'?'+urllib.parse.urlencode(inviteParser.ip.header), None)
			else:
				sendURL,tmpPage=self.profiles[self.realSenderIdx].fetchIFrame(inviteParser.ip.action, self.senders[self.realSenderIdx].open(inviteParser.ip.action, urllib.parse.urlencode(inviteParser.ip.header)))
				self.lastURL[self.realSenderIdx]=(sendURL,  urllib.parse.urlencode(inviteParser.ip.header))

			f=open('send.htm', 'wb')
			f.write(tmpPage.encode('utf-8', 'ignore'))
			f.close()
			#self.createAjaxThreads(self.realSenderIdx, sendURL)

			print ("*****Starting accepters*****")
			for i in range(0, len(self.senders)):
				if i!=self.realSenderIdx:
					if self.lastURL[i]==None:
						#accepter = FBAuto.common.autoaccept.JobMain(senders[i])
						#self.lastURL[i] = accepter.acceptLink(m.group(1).replace(' ', '+')).replace('http://apps.facebook.com/vampiresgame/','')
						info = self.profiles[i].openTillEnd(m.group(1).replace(' ', '+'))
						self.lastURL[i] = (info[0], None)

						f=open('receive.htm', 'wb')
						f.write(info[1].encode('utf-8', 'ignore'))
						f.close()
					#profiles[i].refreshAjax(self.lastURL[i])
					#self.createAjaxThreads(i, self.lastURL[i])
			return True

		else:
			print ('Accept URL not found!')
			return False
if __name__ == "__main__":
	#controller=SessionController()
	#controller.start()
	accounts=AccountManager('vw_gift.txt')
	senders=[]
	for f in accounts.logins:
			senders.append(SessionController(f))
	allGs=[]
	for g in gift_list:
		for i in range(0, 1):
			gs = GiftSender(g[0], senders)
			gs.realSenderIdx=0
			gs.total=g[1]
			gs.run()
			allGs.append(gs)
	print ("*********GOOOOOO*********")
	#time.sleep(30)

	#for i in range(0, 9):
	while True:
		for gs in allGs:
			for j in range(0, 1):
				gs.oneClickAccept()
		print ("*********GOOOOOO*********")
		#time.sleep(30)

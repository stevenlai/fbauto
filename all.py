import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampire.vampire
import FBAuto.crusades.fight
import FBAuto.crusades.earn
import FBAuto.gangsterbattle.earn
import FBAuto.gangsterbattle.fight
import FBAuto.gangsterbattle.property
import FBAuto.knighted.earn
import FBAuto.knighted.fight
import FBAuto.knighted.property
import FBAuto.playsexgames.earn
import FBAuto.playsexgames.fight
import FBAuto.playsexgames.property
import FBAuto.playgangwars.earn
import FBAuto.mafiacities.earn
import FBAuto.mobwars.property
import FBAuto.mobwars.earn
import FBAuto.bandbattle.property
import FBAuto.mightofmany.earn
import FBAuto.mightofmany.fight
import FBAuto.mightofmany.property
import FBAuto.piratesrule.earn
import FBAuto.piratesrule.fight
import FBAuto.piratesrule.property
import FBAuto.dragonwars.earn
import FBAuto.dragonwars.fight
import FBAuto.dragonwars.property
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.vampiresgame.common
import FBAuto.spacewarsgame.earn
import FBAuto.spacewarsgame.fight
import FBAuto.spacewarsgame.property
import FBAuto.heroesvillains.earn
import FBAuto.heroesvillains.fight
import FBAuto.heroesvillains.property
import FBAuto.prisonlockdowngame.earn
import FBAuto.prisonlockdowngame.fight
import FBAuto.prisonlockdowngame.property
import FBAuto.gangwarsgame.earn
import FBAuto.gangwarsgame.fight
import FBAuto.gangwarsgame.property
import FBAuto.fashionwarsgame.earn
import FBAuto.fashionwarsgame.fight
import FBAuto.fashionwarsgame.property
import FBAuto.footballwars.earn
import FBAuto.footballwars.fight
import FBAuto.footballwars.property
import FBAuto.gangster_wars.property
import FBAuto.truegangsters.inventory
import FBAuto.truegangsters.earn
import FBAuto.truegangsters.fight
import FBAuto.truegangsters.property
import FBAuto.nitrousracing.inventory
import FBAuto.nitrousracing.earn
import FBAuto.nitrousracing.fight
import FBAuto.ability.ability2
import FBAuto.refresher.refresher
import FBAuto.treasurehunt.egghunt
import FBAuto.treasurehunt.fbfairy
import FBAuto.treasurehunt.scavengermouse
import FBAuto.pets.fluff
import FBAuto.pets.hatchery
import FBAuto.pets.metropolisgame
import FBAuto.pets.zoobuilder
import FBAuto.pets.helptheplanet
import FBAuto.liloceanpatch.sell
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite
import FBAuto.common.autoadd

if __name__ == "__main__":
	controller=SessionController()
	controller.setMaxConnection(5000)
	controller.start()
	time.sleep(random.randint(1, 3))
	#Critical tasks
	gangsterbattle_heal = FBAuto.gangsterbattle.fight.HealMain(controller)
	gangsterbattle_heal.start()
	time.sleep(random.randint(1, 3))
	dragonwars_heal = FBAuto.dragonwars.fight.HealMain(controller)
	dragonwars_heal.start()
	time.sleep(random.randint(1, 3))
	heroesvillains_heal = FBAuto.heroesvillains.fight.HealMain(controller)
	heroesvillains_heal.start()
	time.sleep(random.randint(1, 3))
	spacewarsgame_heal = FBAuto.spacewarsgame.fight.HealMain(controller)
	spacewarsgame_heal.start()
	time.sleep(random.randint(1, 3))
	fashionwarsgame_heal = FBAuto.fashionwarsgame.fight.HealMain(controller)
	fashionwarsgame_heal.start()
	time.sleep(random.randint(1, 3))
	pirate_heal = FBAuto.piratesrule.fight.HealMain(controller)
	pirate_heal.start()
	time.sleep(random.randint(1, 3))
	#MHA
	#ability = FBAuto.ability.ability2.Ability(controller)
	#ability.restFirst=True
	#ability_timer=JobTimer([18*3600, 20*3600], [4*3600, 5*3600])
	#ability.jobTimer=ability_timer
	#ability.start()
	#playsexgames_heal = FBAuto.playsexgames.fight.HealMain(controller)
	#playsexgames_heal.start()
	#time.sleep(random.randint(1, 3))
	#Nitro racing
	#nitrousracing_earn=FBAuto.nitrousracing.earn.JobMain(controller)
	#nitrousracing_earn.start()
	#time.sleep(random.randint(1, 3))
	#nitrousracing_fight=FBAuto.nitrousracing.fight.FightMain(controller)
	#nitrousracing_fight.start()
	#time.sleep(random.randint(1, 3))
	#Crusades
	#crusade_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
	#crusades_fight= FBAuto.crusades.fight.FightMain(controller)
	#crusades_fight.jobTimer=crusade_timer
	#crusades_fight.start()
	#time.sleep(random.randint(1, 3))
	#crusades_earn= FBAuto.crusades.earn.JobMain(controller)
	#crusades_earn.jobTimer=crusade_timer
	#crusades_earn.start()
	#time.sleep(random.randint(1, 3))
	#crusades_collect = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/crusades/home/sanctuary'], 3600*24)
	#crusades_collect.jobTimer=crusade_timer
	#crusades_collect.start()
	#time.sleep(random.randint(1, 3))
	#Zynga Serial
	#Dragon Wars
	dragonwars_job = FBAuto.dragonwars.earn.JobMain(controller)
	dragonwars_job.start()
	time.sleep(random.randint(1, 3))
	dragonwars_fight = FBAuto.dragonwars.fight.FightMain(controller)
	dragonwars_fight.start()
	time.sleep(random.randint(1, 3))
	dragonwars_property = FBAuto.dragonwars.property.PropertyMain(controller)
	dragonwars_property.start()
	time.sleep(random.randint(1, 3))
	#Pirates
	pirate_job = FBAuto.piratesrule.earn.JobMain(controller)
	pirate_job.start()
	time.sleep(random.randint(1, 3))
	pirate_fight = FBAuto.piratesrule.fight.FightMain(controller)
	pirate_fight.start()
	time.sleep(random.randint(1, 3))
	pirate_property = FBAuto.piratesrule.property.PropertyMain(controller)
	pirate_property.start()
	time.sleep(random.randint(1, 3))
	#Space Wars
	spacewarsgame_job = FBAuto.spacewarsgame.earn.JobMain(controller)
	spacewarsgame_job.start()
	time.sleep(random.randint(1, 3))
	spacewarsgame_fight = FBAuto.spacewarsgame.fight.FightMain(controller)
	spacewarsgame_fight.start()
	time.sleep(random.randint(1, 3))
	spacewarsgame_property = FBAuto.spacewarsgame.property.PropertyMain(controller)
	spacewarsgame_property.start()
	time.sleep(random.randint(1, 3))
	#Heroes & Villains
	heroesvillains_job = FBAuto.heroesvillains.earn.JobMain(controller)
	heroesvillains_job.start()
	time.sleep(random.randint(1, 3))
	heroesvillains_fight = FBAuto.heroesvillains.fight.FightMain(controller)
	heroesvillains_fight.start()
	time.sleep(random.randint(1, 3))
	heroesvillains_property = FBAuto.heroesvillains.property.PropertyMain(controller)
	heroesvillains_property.start()
	time.sleep(random.randint(1, 3))
	#Fashion Wars
	fashionwarsgame_job = FBAuto.fashionwarsgame.earn.JobMain(controller)
	fashionwarsgame_job.start()
	time.sleep(random.randint(1, 3))
	fashionwarsgame_fight = FBAuto.fashionwarsgame.fight.FightMain(controller)
	fashionwarsgame_fight.start()
	time.sleep(random.randint(1, 3))
	fashionwarsgame_property = FBAuto.fashionwarsgame.property.PropertyMain(controller)
	fashionwarsgame_property.start()
	time.sleep(random.randint(1, 3))
	#Non-critical jobs
	#Gangster Battle
	gangster_earn = FBAuto.gangsterbattle.earn.JobMain(controller)
	gangster_earn.start()
	time.sleep(random.randint(1, 3))
	gangster_fight = FBAuto.gangsterbattle.fight.FightMain(controller)
	gangster_fight.start()
	time.sleep(random.randint(1, 3))
	gangster_property = FBAuto.gangsterbattle.property.PropertyMain(controller)
	gangster_property.start()
	time.sleep(random.randint(1, 3))
	#Knighted
	knighted_earn = FBAuto.knighted.earn.JobMain(controller)
	knighted_earn.start()
	time.sleep(random.randint(1, 3))
	knighted_fight = FBAuto.knighted.fight.FightMain(controller)
	knighted_fight.start()
	time.sleep(random.randint(1, 3))
	knighted_property = FBAuto.knighted.property.PropertyMain(controller)
	knighted_property.start()
	time.sleep(random.randint(1, 3))
	#Sex Games
	playsexgames_earn = FBAuto.playsexgames.earn.JobMain(controller)
	playsexgames_earn.start()
	time.sleep(random.randint(1, 3))
	playsexgames_fight = FBAuto.playsexgames.fight.FightMain(controller)
	playsexgames_fight.start()
	time.sleep(random.randint(1, 3))
	playsexgames_property = FBAuto.playsexgames.property.PropertyMain(controller)
	playsexgames_property.start()
	time.sleep(random.randint(1, 3))
	#Might of Many
	might_earn = FBAuto.mightofmany.earn.JobMain(controller)
	might_earn.start()
	time.sleep(random.randint(1, 3))
	might_fight = FBAuto.mightofmany.fight.FightMain(controller)
	might_fight.start()
	time.sleep(random.randint(1, 3))
	might_property = FBAuto.mightofmany.property.PropertyMain(controller)
	might_property.start()
	time.sleep(random.randint(1, 3))
	#Mob Wars
	mobwars_timer=JobTimer([15*3600, 17*3600], [7*3600, 9*3600])
	mobwars_property = FBAuto.mobwars.property.PropertyMain(controller)
	mobwars_property.jobTimer=mobwars_timer
	mobwars_property.start()
	time.sleep(random.randint(1, 3))
	mobwars_earn = FBAuto.mobwars.earn.JobMain(controller)
	mobwars_earn.jobTimer=mobwars_timer
	mobwars_earn.start()
	time.sleep(random.randint(1, 3))
	#Real Time Racing
	#Band battle
	#bandbattle_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
	#bandbattle_earn=FBAuto.bandbattle.earn.BandBattle(controller)
	#bandbattle_earn.jobTimer=bandbattle_timer
	#bandbattle_earn.start()
	#time.sleep(random.randint(1, 3))
	bandbattle_property = FBAuto.bandbattle.property.PropertyMain(controller)
	bandbattle_timer=JobTimer([5*3600, 6*3600], [4*3600, 7*3600])
	bandbattle_property.jobTimer=bandbattle_timer
	bandbattle_property.start()
	time.sleep(random.randint(1, 3))
	#Gang Wars
	playgangwars = FBAuto.playgangwars.earn.JobMain(controller)
	playgangwars_timer=JobTimer([15*3600, 17*3600], [7*3600, 9*3600])
	playgangwars.jobTimer=playgangwars_timer
	playgangwars.start()
	time.sleep(random.randint(1, 3))
	# gangster-wars:
	gangster_wars_property = FBAuto.gangster_wars.property.PropertyMain(controller)
	gangster_wars_property.start()
	time.sleep(random.randint(1, 3))
	#Mafia Cities
	mafiacities_job = FBAuto.mafiacities.earn.JobMain(controller)
	mafiacities_job.start()
	time.sleep(random.randint(1, 3))
	#True Gangsters
	truegangsters_inventory=FBAuto.truegangsters.inventory.InventoryMain(controller)
	truegangsters_inventory.start()
	time.sleep(random.randint(1, 3))
	truegangsters_earn=FBAuto.truegangsters.earn.JobMain(controller)
	truegangsters_earn.start()
	time.sleep(random.randint(1, 3))
	truegangsters_fight=FBAuto.truegangsters.fight.FightMain(controller)
	truegangsters_fight.start()
	time.sleep(random.randint(1, 3))
	truegangsters_property=FBAuto.truegangsters.property.PropertyMain(controller)
	truegangsters_property.start()
	time.sleep(random.randint(1, 3))
	#Treasures
	fbfairy_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
	fbfairy = FBAuto.treasurehunt.fbfairy.JobMain(controller)
	fbfairy.jobTimer=fbfairy_timer
	fbfairy.start()
	time.sleep(random.randint(1, 3))
	fbfairy_feed = FBAuto.treasurehunt.fbfairy.FeedMain(controller)
	fbfairy_feed.start()
	time.sleep(random.randint(1, 3))
	egghunt_feed = FBAuto.treasurehunt.egghunt.FeedMain(controller)
	egghunt_feed.start()
	time.sleep(random.randint(1, 3))
	scavengermouse_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
	scavengermouse = FBAuto.treasurehunt.scavengermouse.JobMain(controller)
	scavengermouse.jobTimer=scavengermouse_timer
	scavengermouse.start()
	time.sleep(random.randint(1, 3))
	#Vampire
	#vampire = FBAuto.vampire.vampire.Vampire(controller)
	#vampire.start()
	#time.sleep(random.randint(1, 3))
	#Pets:
	fluff_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
	fluff = FBAuto.pets.fluff.JobMain(controller)
	fluff.jobTimer = fluff_timer
	fluff.start()
	time.sleep(random.randint(1, 3))
	hatchery = FBAuto.pets.hatchery.JobMain(controller)
	hatchery.start()
	time.sleep(random.randint(1, 3))
	#Metro
	metropolisgame = FBAuto.pets.metropolisgame.JobMain(controller)
	metropolisgame.alwaysGiveYoung=True
	metropolisgame.start()
	time.sleep(random.randint(1, 3))
	metropolisgame2 = FBAuto.pets.metropolisgame.JobMain(controller)
	metropolisgame2.scheduled_url='connections.php?show=members'
	metropolisgame2.recursive=True
	metropolisgame2.alwaysGiveYoung=True
	metropolisgame2.scheduled_sleepTime=[24*3600, 24*3600+60]
	metropolisgame2.start()
	time.sleep(random.randint(1, 3))
	zoobuilder = FBAuto.pets.zoobuilder.JobMain(controller)
	zoobuilder.start()
	time.sleep(random.randint(1, 3))
	zoobuilder2 = FBAuto.pets.zoobuilder.JobMain(controller)
	zoobuilder2.scheduled_url='connections.php?show=members'
	zoobuilder2.alwaysGiveYoung=True
	zoobuilder2.scheduled_sleepTime=[24*3600, 24*3600+60]
	zoobuilder2.start()
	time.sleep(random.randint(1, 3))
	helptheplanet = FBAuto.pets.helptheplanet.JobMain(controller)
	helptheplanet.start()
	time.sleep(random.randint(1, 3))
	helptheplanet_build = FBAuto.pets.helptheplanet.BuildMain(controller)
	helptheplanet_build.start()
	time.sleep(random.randint(1, 3))
	#helptheplanet2 = FBAuto.pets.helptheplanet.JobMain(controller)
	#helptheplanet2.scheduled_url='connections.php?show=members'
	#helptheplanet2.scheduled_sleepTime=[24*3600, 24*3600+60]
	#helptheplanet2.start()
	#time.sleep(random.randint(1, 3))
	#Refreshers
	hatchery_refresh = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/hatchery/'], 3600*2)
	hatchery_refresh.start()
	time.sleep(random.randint(1, 3))
	ihugyou = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/ihugyou/'], 3600*6)
	ihugyou.start()
	time.sleep(random.randint(1, 3))
	ikarmayou = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/ikarmayou/'], 3600*6)
	ikarmayou.start()
	time.sleep(random.randint(1, 3))
	siegewar = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/siegewar/'], 3600*4)
	siegewar.start()
	time.sleep(random.randint(1, 3))
	friendstock = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/friendstock/'], 3600*24)
	friendstock.start()
	time.sleep(random.randint(1, 3))
	bigbuck = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/bigbuck/'], 3600)
	bigbuck.start()
	time.sleep(random.randint(1, 3))
	peelameal = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/peelameal/'], 4*3600)
	peelameal.start()
	time.sleep(random.randint(1, 3))
	rollbrim = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/supercups/'], 4*3600)
	rollbrim.start()
	time.sleep(random.randint(1, 3))
	popthetop = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/popthetop/'], 4*3600)
	popthetop.start()
	time.sleep(random.randint(1, 3))
	yourharem = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/yourharem/'], 3600*8)
	yourharem.start()
	time.sleep(random.randint(1, 3))
	tradefriends = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/trade-friends/'], 6*3600)
	tradefriends.start()
	time.sleep(random.randint(1, 3))
	ffsale = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/friendsforsale/'], 4*3600)
	ffsale.start()
	time.sleep(random.randint(1, 3))
	tycoon = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/beatycoon/'], 4*3600)
	tycoon.start()
	time.sleep(random.randint(1, 3))
	notification = FBAuto.refresher.refresher.Refresher(controller, [Facebook.facebookURL+'/notifications.php'], 3*3600)
	notification.start()
	time.sleep(random.randint(1, 3))
	egghunt = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/egghunt/warehouse/dailyBonus.php'], 24*3600)
	egghunt.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/spacewarsgame/recruit.php?action=accept&user_id=all'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/heroesvillains/recruit.php?action=accept&user_id=all'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/piratesrule/recruit.php?action=accept&user_id=all'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/gangsterbattle/family.php?op=gangster_invites'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/knighted/family.php?op=gangster_invites'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/playlockedup/family.php?op=gangster_invites'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/playspacebattle/family.php?op=gangster_invites'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	wars = FBAuto.refresher.refresher.Refresher(controller, [Facebook.appsURL+'/playsexgames/family.php?op=gangster_invites'], 12*3600)
	wars.start()
	time.sleep(random.randint(1, 3))
	#Gifts?
	#liloceanpatch = FBAuto.liloceanpatch.sell.JobMain(controller)
	#liloceanpatch.start()
	#time.sleep(random.randint(1, 3))
	#Misc
	autoaccept = FBAuto.common.autoaccept.JobMain(controller)
	autoaccept.start()
	time.sleep(random.randint(1, 3))
	#autoinvite = FBAuto.common.autoinvite.JobMain(controller)
	#autoinvite.start()
	#time.sleep(random.randint(1, 3))
	#autoadd = FBAuto.common.autoadd.JobMain(controller)
	#autoadd.start()
	#time.sleep(random.randint(1, 3))

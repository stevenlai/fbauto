from FBAuto.common.facebook import Facebook
import FBAuto.common.playgangwars_common

appID='playgangwars'
config = FBAuto.common.playgangwars_common.GameConfig()
config.appID=appID
config.energyPrefix='<span[^>]*energy[^>]*>'
config.energyMidfix='/'
config.healthPrefix='<span[^>]*health[^>]*>'
config.healthMidfix='/'
config.staminaPrefix='<span[^>]*stamina[^>]*>'
config.staminaMidfix='/'
config.cashPrefix='<span[^>]*cash[^>]*>\$[^0-9]*'
config.bankPrefix='Account Balance: \$'
config.levelPrefix='<span class="txt_white">'
config.propertyPrefix='Owned:[ ]*'
config.jailstr='Jail'
config.energyRechargeTime=300

if __name__ == "__main__":
	f = Facebook('facebook')
	p=FBAuto.common.playgangwars_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()

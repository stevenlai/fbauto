import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

appID='gangsterbattle'
class BankParser(HTMLParser):
	begin=False
	header=[]
	opener=None

	def __init__(self, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action='accounts.php'
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]

			if name=='action' and value!='deposit':
				self.header=[]
				self.begin=False
			elif name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/'+appID+'/accounts.php', input)
			self.begin=False
			self.header=[]


class ProfileLoader:
	strength=0
	life=0
	endurance=0
	prisoned=False
	cash=0
	opener=None
	page=''
	def __init__(self, f):
		self.opener=f
		self.strength=0
		self.life=0
		self.endurance=0
		self.prisoned=False
		self.cash=0
		self.page=''
	def loadPage(self):
		self.page=self.opener.open(Facebook.appsURL+'/'+appID+'/accounts.php')
		while re.compile("Life:[ ]*[0-9]+/").search(self.page)==None:
			print ('Error loading the profile! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			self.page=self.opener.open(Facebook.appsURL+'/'+appID+'/accounts.php')
	def loadProfile (self):
		self.loadPage()
		m=re.compile("Strength:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.strength=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Life:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.life=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Endurance:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.endurance=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Cash:[ ]*\$[0-9,]+").search(self.page)
		if m!=None:
			cashstr=re.compile('[0-9,]+').search(m.group()).group()
			self.cash=int(cashstr.replace(',', ''))

		m=re.compile("stretch in prison").search(self.page)
		if m!=None:
			self.prisoned=True
		else:
			self.prisoned=False

		print ('Profile loaded. Endurance:',self.endurance,'Life:',self.life,'Strength:',self.strength,'Cash:',self.cash,'Prisoned:',self.prisoned)


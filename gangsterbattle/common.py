from FBAuto.common.facebook import Facebook
import FBAuto.common.gangsterbattle_common

appID='gangsterbattle'
config = FBAuto.common.gangsterbattle_common.GameConfig()
config.appID=appID
config.energyPrefix='Endurance:[ ]*'
config.energyMidfix='/'
config.healthPrefix='Life:[ ]*'
config.healthMidfix='/'
config.staminaPrefix='Strength:[ ]*'
config.staminaMidfix='/'
config.cashPrefix='<span>(Cash:[ ]*)*\$'
config.bankPrefix='<h1>\$'
config.propertyPrefix='Owned: <b>'
config.jailstr='stretch in prison'
config.itemURL=Facebook.appsURL+'/'+appID+'/arsenal.php?page=items'
config.stayInHospital=False
config.depositOnly=True
config.minDepositAmount=1000
config.energyRechargeTime=289

if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.gangsterbattle_common.ProfileLoader(f, config)
	f.login()
	p.loadProfile()
	p.displayProfile()
	f.logout()

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampiresgame.common
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.vampiresgame.inventory
import FBAuto.vampiresgame.stats

if __name__ == "__main__":
	accounts=AccountManager('vw_bite.txt')
	senders=[]
	for f in accounts.logins:
		controller=SessionController(f)
		controller.setMaxConnection(1000)
		senders.append(controller)

	biter = FBAuto.vampiresgame.common.QuickBite(senders[0])
	biter.accounts=[senders[1]]
	biter.start()

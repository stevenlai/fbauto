from FBAuto.common.facebook import Facebook
import FBAuto.common.zynga_common

appID='heroesvillains'
config = FBAuto.common.zynga_common.GameConfig()
config.appID=appID
config.energyPrefix='Energy:.*<strong><span id="app[0-9]+_current_energy" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.healthPrefix='Health:.*<strong><span id="app[0-9]+_current_health" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.staminaPrefix='Stamina:.*<strong><span id="app[0-9]+_current_stamina" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.cashPrefix='Vials:.*<strong class="money"><img src="[^>]*>[ ]*'
config.bankPrefix='Total Vials:.*<b class="money"><img src=[^>]+>'
config.memberPrefix='my league[ ]*\('
config.propertyPrefix='Owned:[^0-9]+'
config.jailstr='Jail'
config.adminID='91200517'
config.minBankBalance=1000000000
config.winMsg='You WON the battle'
config.feedPrefix='You were attacked by[ ]*'
config.fightMsg='You fought against'
#config.fightFromFeed=True
config.useEnergyPack=False
config.fightFriend=True
config.minDeposit=10
config.energyRechargeTime=200

if __name__ == "__main__":
	f = Facebook('facebook')
	f.login()
	p=FBAuto.common.zynga_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()
	f.logout()

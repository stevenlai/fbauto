import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.FriendFilter import *
from FBAuto.common.AjaxProcessor import *

class InviteParser2(HTMLParser):
	form_id=''
	content=''
	action=''
	form_detected=False
	appID=''
	header=[]
	isGet=True
	def __init__(self, form_id, appID):
		HTMLParser.__init__(self)
		self.form_id=form_id
		self.content=''
		self.action=''
		self.appID=appID
		self.header=[]
		self.isGet=True
		self.form_detected=False
	def handle_starttag(self, tag, attrs):
		if tag.lower()=='fb:request-form':
			self.form_detected=True
			for a in attrs:
				if a[0].lower()=='content':
					self.content=a[1].replace('&#039;',"'")
				elif a[0].lower()=='method' and a[1].lower()=='post':
					self.isGet=False
				elif a[0].lower()=='action':
					act_decoded=a[1].replace('&amp;', '&')
					if re.compile('facebook').search(act_decoded)!=None:
						self.action=act_decoded
					elif re.compile(self.appID).search(act_decoded)!=None:
						self.action=Facebook.appsURL+'/'+act_decoded.strip('.').strip('/')
					else:
						self.action=Facebook.appsURL+'/'+self.appID+'/'+act_decoded.strip('.').strip('/')
		elif tag.lower()=='input' and self.form_detected:
			name=''
			value=''
			for a in attrs:
				if a[0].lower()=='name':
					name=a[1]
				elif a[0].lower()=='value':
					value=a[1]

			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.form_detected:
			self.form_detected=False

class InviteParser(HTMLParser):
	form_id=''
	content=''
	action=''
	form_detected=False
	form_started=False
	appID=''
	header=[]
	isGet=True
	def __init__(self, form_id, appID):
		HTMLParser.__init__(self)
		self.form_id=form_id
		self.content=''
		self.action=''
		self.appID=appID
		self.header=[]
		self.isGet=True
		self.form_detected=False
		self.form_started=False
	def handle_starttag(self, tag, attrs):
		if tag.lower()=='form':
			self.form_started=False
			for a in attrs:
				if a[0].lower()=='id' and a[1]==self.form_id:
					self.form_detected=True
					self.form_started=True
			if self.form_detected:
				for a in attrs:
					if a[0].lower()=='content':
						self.content=a[1].replace('&#039;',"'")
					elif a[0].lower()=='method' and a[1].lower()=='post':
						self.isGet=False
					elif a[0].lower()=='action':
						act_decoded=a[1].replace('&amp;', '&')
						if re.compile('facebook').search(act_decoded)!=None:
							self.action=act_decoded
						elif re.compile(self.appID).search(act_decoded)!=None:
							self.action=Facebook.appsURL+'/'+act_decoded.strip('.').strip('/')
						else:
							self.action=Facebook.appsURL+'/'+self.appID+'/'+act_decoded.strip('.').strip('/')
		elif tag.lower()=='input' and self.form_started:
			name=''
			value=''
			for a in attrs:
				if a[0].lower()=='name':
					name=a[1]
				elif a[0].lower()=='value':
					value=a[1]

			if name!='' and name!='ids[]':
				#print ('Form parameter:', name+'='+value)
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if tag.lower()=='form' and self.form_started:
			self.form_started=False
class JobMain(JobRunner):
	jobList=[]
	form_id=''
	app_id=''
	request_type=''
	invite=''
	preview=''
	is_multi=''
	post_form_id=''
	action_type=''
	prefill=1
	inviteMax=0
	toInvite=[]
	candidates=[]
	available=[]
	ip=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID='N/A'
		self.jobDescription='Send Invite'
		self.jobList=[]
		self.resetAll()

	def resetAll(self):
		self.form_id=''
		self.app_id=''
		self.request_type=''
		self.invite=''
		self.preview=''
		self.is_multi=''
		self.prefill=1
		self.post_form_id=''
		self.action_type=''
		self.inviteMax=0
		self.toInvite=[]
		self.candidates=[]
		self.available=[]
		self.ip=None

	def loadAvailable(self, friendList):
		iterator = re.compile('"[0-9]+":').finditer(friendList)
		for i in iterator:
			id=re.compile('[0-9]+').search(i.group()).group()
			if self.available.count(id)==0:
				self.available.append(id)
		#print (self.available)
	def loadInviteMax(self, maxStr):
		self.inviteMax=int(maxStr)
	def process(self, func):
		i=0
		decoded=func.replace("&#039;", "'")
		parameters = decoded.split(',')
		for p in parameters:
			#print (p)
			if i==0:
				m=re.compile('[a-zA-Z0-9_]+form[a-zA-Z0-9_]+').search(p.strip())
				if m!=None:
					self.form_id=m.group()
			elif i==1:
				m=re.compile('[0-9]+').search(p.strip())
				if m!=None:
					self.app_id=m.group()
			elif i==2:
				m=re.compile("[^']+").search(p.strip())
				if m!=None:
					self.request_type=m.group()
			elif i==3:
				m=re.compile('[a-zA-Z]+').search(p.strip())
				if m!=None:
					self.invite=m.group()
			elif i==4:
				m=re.compile('[a-zA-Z]+').search(p.strip())
				if m!=None:
					self.preview=m.group()
			elif i==5:
				m=re.compile('[a-zA-Z]+').search(p.strip())
				if m!=None:
					self.is_multi=m.group()
			elif i==6:
				m=re.compile('[0-9]+').search(p.strip())
				if m!=None:
					self.prefill=int(m.group())
			i=i+1
		print ('Form id:',self.form_id,)
		print ('App id:', self.app_id,)
		print ('Request type:', self.request_type,)
		print ('Invite:', self.invite,)
		print ('Preview:', self.preview,)
		print ('Is multi:', self.is_multi,)
		print ('Prefill:', str(self.prefill>0).lower(),)
		print ('Post form ID:', self.post_form_id,)
		print ('Invite max:', self.inviteMax)
	def readJobList(self):
		self.jobList=[]
		d = os.getcwd()
		if re.compile('common').search(d)==None:
			d=os.path.join(d, 'config', 'autoinvite.txt')
		else:
			d=os.path.join(d, '..', 'config', 'autoinvite.txt')
		#print ('Opeing egg info file:', d)
		f = open(d, 'r')
		iterator=re.compile('[^ ]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					id=jobInfo.strip('\n')
				elif i==1:
					appID=jobInfo.strip('\n')
				elif i==2:
					url=jobInfo.strip('\n')
				i=i+1
			self.jobList.append((id, appID, url))
			iterator=re.compile('[^ ]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()
	def loadPage(self, url):
		finished=False
		while not finished:
			try:
				html=self.opener.open(url)
				finished=True
			except:
				traceback.print_exc(file=sys.stdout)
				fishied=False

		return html
	def doJob(self):
		startTime = time.time()
		sleepTime=24*3600

		self.readJobList()
		for j in self.jobList:
			self.action_type=j[0]
			self.parseInvitePage(j[1], j[2])
			if self.inviteMax>0 or self.action_type=='post':
				filter=FriendFilter(self.opener, self.app_id)
				filter.loadNames()
				if len(filter.allID)==0:
					print ('Cannot find find any friend... Try from the news feed.')
					sleepTime=4*3600
					filter=FriendFilter2(self.opener, self.app_id)
					filter.loadID()
				self.candidates=filter.allID
				#self.candidates.reverse()
				random.shuffle(self.candidates)
				self.doInvite()
			else:
				print ('Out of invite! Try again later')
			self.resetAll()

		timeSpent = time.time()-startTime
		print ('Finished, time used:', timeSpent)
		if sleepTime>timeSpent:
			sleepTime=sleepTime-int(timeSpent)
		else:
			sleepTime=3600

		self.sleepTime=[sleepTime, sleepTime+60]

	def parseInvitePage(self, appID, inviteURL):
		#html=self.opener.open(Facebook.appsURL+'/'+appID+'/'+inviteURL)
		html=self.loadPage(Facebook.appsURL+'/'+appID+'/'+inviteURL)
		self.parseInviteHTML(appID, html)

	def parseInviteHTML(self, appID, html):
		m=re.compile('post_form_id=([a-zA-Z0-9]+)').search(html)
		if m!=None:
			self.post_form_id=m.group(1)

		m=re.compile('multifriend_selector\(((items)|({[^;]+})),[ ]*"(req_form_[a-zA-Z0-9]+)",[ ]*([0-9]+)[^)]+\)').search(html)
		if m!=None:
			self.form_id=m.group(4)
			self.loadInviteMax(m.group(5))
			if m.group(1)!='items':
				self.loadAvailable(m.group(1))

		m=re.compile('items[ ]*=[ ]*{[^;]+}').search(html)
		if m!=None:
			self.loadAvailable(m.group())

		m=re.compile('sendRequest([^"]+);').search(html)
		if m!=None:
			func=m.group()
			self.process(func)

		self.ip=InviteParser(self.form_id, appID)
		try:
			self.ip.feed(html)
			self.ip.close()
		except:
			traceback.print_exc(file=sys.stdout)

		if not self.ip.form_detected:
			print ('Type 1 request form not detected, trying type 2')
			self.ip=InviteParser2(self.form_id, appID)
			try:
				self.ip.feed(html)
				self.ip.close()
			except:
				pass

	def doInvite(self):
		for id in self.candidates:
			if self.available.count(id)>0:
				self.toInvite.append(id)
		print ('Available ('+str(len(self.available))+')',)
		print ('Candidates ('+str(len(self.candidates))+')',)
		print ('To invite ('+str(len(self.toInvite))+')')

		if len(self.toInvite)==0:
			print ('Nobody to invite!')
			return

		header={'app_id':self.app_id, 'request_type':self.request_type,'invite':self.invite,'content':self.ip.content,'is_multi':self.is_multi,'form_id':self.form_id,'prefill':str(self.prefill>0).lower(),'message':'You are receving this invite because Facebook told me you have installed this app so I figured you could use one more invite. I will send this invite for only once. If you feel disturbed in any way, please let me know. Have a great day!','donot_send':'false','post_form_id':self.post_form_id, 'preview':'false'}

		if self.action_type=='post':
			self.ip.header=[]

		i=0
		while i<len(self.toInvite) and (i<self.inviteMax or self.action_type=='post'):
			header['to_ids['+str(i)+']']=self.toInvite[i]
			self.ip.header.append(('ids[]', self.toInvite[i]))
			i=i+1

		if self.action_type=='invite':
			input=urllib.parse.urlencode(header)
			print ('Input:', input)
			html = self.opener.open(Facebook.appsURL+'/fbml/ajax/prompt_send.php', input)
		else:
			print ('Skipping invite part, directly post')
			html = 'No error'

		ajax=AjaxProcessor()
		if not ajax.validate(html):
			print ('Error occurred: '+html)
		else:
			if i<self.inviteMax:
				print ('Not enough people to invite!')
			else:
				print ('Invite sent successfully')
			print (self.ip.action, self.ip.header)
			try:
				if ip.isGet:
					print (self.ip.action+'?'+urlencode(self.ip.header))
					self.opener.open(self.ip.action+'?'+urllib.parse.urlencode(self.ip.header))
				else:
					print (self.ip.action, urlencode(self.ip.header))
					self.opener.open(self.ip.action, urllib.parse.urlencode(self.ip.header))
			except:
				pass
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = JobMain(f)
		job.doJob()
	else:
		job = JobMain(f)
		job.start()


import time
import threading
from .facebook import *
from .JobRunner import *
from .CaptchaProcessor import *
from .Tkinter import *

class JobMain(threading.Thread):
	captcha_info=None
	opener=None

	def __init__(self, f):
		threading.Thread.__init__(self)
		self.opener=f
		self.captcha_info = CaptchaInfo(self.opener)
	def showWindow(self):
		root = Tk()
		frame = Frame(root)
		frame.pack()
		w = Label(root, text=self.captcha_info.readDir)
		w.pack()
		root.mainloop()
	def run(self):
		while True:
			html = self.opener.open(self.captcha_info.readDir)
			#if re.compile('Parent').search(html)!=None:
			if re.compile('\.htm').search(html)!=None:
				print ('New captcha found')
				self.showWindow()
			else:
				print ('No captcha found')
			time.sleep(random.randint(20, 30))

if __name__ == "__main__":
	f = Facebook('facebook')
	job=JobMain(f)
	job.start()

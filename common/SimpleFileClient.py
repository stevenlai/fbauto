import socket
import re
import sys
import traceback
import os.path
from FBAuto.common.SimpleFileServerCommon import *

class SimpleFileClient(ClientBase, FileProcessor):
	test=False
	sock=None
	host=None
	port=0
	semaphore=None
	def __init__(self, host, port):
		self.host=host
		self.port=port
		#self.open()
		if self.semaphore==None:
			self.semaphore = threading.RLock()
	def __del__(self):
		self.close()
	def getSock(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((self.host, self.port))
		return sock
	def open(self):
		self.sock=self.getSock()
	def close(self):
		if self.sock!=None:
			#protocol = ProtocolProcessor()
			self.sendData(self.sock, 'logout 0 ')
			self.sock.close()
			self.sock=None
	def executeCmd(self, cmd):
		if not self.semaphore.acquire(blocking=0):
			return ["BUSY"]
		response=["FAIL"]
		cmdFinished=False
		while not cmdFinished:
			try:
				response = self.sendData(self.sock, cmd)
				cmdFinished = True
			except:
				traceback.print_exc(file=sys.stdout)

			if not cmdFinished:
				try:
					self.open()
				except:
					pass
		self.semaphore.release()
		return response
	def authenticate(self):
		protocol = ProtocolProcessor()
		response = self.executeCmd(protocol.generateInput('login', 'auth'))
		if response[0]=="SUCCESS":
			return True
		else:
			return False
	def uploadFile(self, filename):
		protocol = ProtocolProcessor()
		response = self.executeCmd(protocol.generateInput('put', filename))
		if response[0]=='SUCCESS':
			response = self.executeCmd(protocol.generateInput('file', self.getFileData(filename)))
			if response[0]=='SUCCESS':
				print ("Upload successful!")
		elif response[0]!='BUSY':
			self.authenticate()
		else:
			print ("Upload busy!")
	def downloadFile(self, filename):
		protocol = ProtocolProcessor()
		response = self.executeCmd(protocol.generateInput('get', filename))
		if response[0]=='SUCCESS':
			self.writeFile(filename, response[1])
			print ("Download successful!")
		elif response[0]!='BUSY':
			self.authenticate()
		else:
			print ("Download busy!")
if __name__ == "__main__":
	client = SimpleFileClient(ProtocolProcessor.HOST, ProtocolProcessor.PORT)
	client.test=True
	client.authenticate()
	#client.downloadFile('facebook-stevenylai@gmail.com.lwp')
	client.downloadFile('autoacceptstevenylai@gmail.com.txt')
	#client.uploadFile('autoacceptstevenylai@gmail.com.txt')
	client.close()

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from html.parser import HTMLParseError
from .ProfileLoader import *
from .FormParser import *
from .JobRunner import *
from .AjaxProcessor import AjaxProcessor
from .facebook import Facebook

class GameConfig:
	appID=''
	appNum=''
	jobURL='jobs.php'
	bankURL='bank.php'
	ajaxURL='http://67.225.199.0/getjobresult.php'
	energyPrefix=''
	energyMidfix='</span>/[ ]*'
	healthPrefix=''
	healthMidfix='</span>/[ ]*'
	staminaPrefix=''
	staminaMidfix='</span>/[ ]*'
	cashPrefix=''
	levelPrefix='Level[ ]*'
	jailstr=''
	breakoutEnergy=5
	energyRechargeTime=600
	healthRechargeTime=180
	staminaRechargeTime=300

class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None), ('Level', game_config.levelPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		if re.compile(self.game_config.jailstr).search(self.page)!=None:
			self.data["Jailed"]=True
		else:
			self.data["Jailed"]=False
	def deposit (self):
		if self.data["Cash"]>0:
			print ('Depositing ('+self.game_config.appID+'):', self.data["Cash"])
			page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL
			html = self.opener.open(page)
			p = SimpleFormParser(self.game_config.bankURL+'?action=deposit', {'howmuch':str(self.data["Cash"])}, {})
			p.feed(html)
			p.close()
			if len(p.forms)>0:
				p.forms[0].submit(self.opener, page+'?action=deposit')

class JobMain(JobRunner):
	game_config=None
	profile=None
	counter=0
	job1Failed=False
	job2Failed=False
	jobList=[]
	#No local job
	jobList2=[]
	#General jobs
	jobList3=[(15, 1, '5', None)]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Earn'
		self.profile=ProfileLoader(f, self.game_config)
	def getRequiredEnergy(self):
		total=0
		if not self.job1Failed:
			for j in self.jobList:
				total=total+j[0]*j[1]
		elif not self.job2Failed and len(self.jobList2)>0:
			for j in self.jobList2:
				total=total+j[0]*j[1]
		else:
			for j in self.jobList3:
				total=total+j[0]*j[1]
		return total
	def canDoJob2 (self):
		for j in self.jobList2:
			if j[3]!=None and len(j)>4:
				page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/stuff.php')
				m=re.compile(j[3]+'[ ]*x[ ]*[0-9]+').search(page)
				if m==None:
					return False
				else:
					item_no=int(re.compile('[0-9]+').search(m.group()).group())
					if item_no<j[4]:
						return False
		print ('Can do job 2')
		return True
	def __earn (self, job_id):
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.jobURL)

		a=AjaxProcessor()
		a.ajax_url_suffix='getjobresult\.php'
		a.process(page)
		info=a.getHeader()
		info["query[action]"]='doit'
		info["query[id]"]=job_id
		info["type"]='2'
		a.post(self.opener)
	def earn (self):
		print ('Earning (Mafia Cities) ...')
		self.job1Failed=False
		self.job2Failed=False
		for j in self.jobList:
			for i in range(0, j[1]):
				print ('Earning:',j[2])
				page = Facebook.appsURL+'/'+self.game_config.appID+'/'+j[2]
				html=self.opener.open(page)
				if re.compile('The Law chased you off').search(html)!=None:
					self.job1Failed=True
				if self.job1Failed:
					break
			if self.job1Failed:
				break
		if self.job1Failed and len(self.jobList2)>0 and self.canDoJob2():
			print ('Job1 failed, doing job 2')
			for j in self.jobList2:
				for i in range(0, j[1]):
					print ('Earning:', j[2])
					result=self.__earn(j[2])
					if re.compile('You do not meet the requirements of the job').search(result)!=None:
						self.job2Failed=True
					if self.job2Failed:
						break
				if self.job2Failed:
					break
		if self.job1Failed and (self.job2Failed or len(self.jobList2)==0):
			print ('Job1 & job2 failed, doing job 3')
			for j in self.jobList3:
				for i in range(0, j[1]):
					print ('Earning:', j[2])
					result=self.__earn(j[2])
	def breakout (self):
		while self.profile.data["Energy"]>=self.game_config.breakoutEnergy and self.profile.data["Jailed"]:
			print ('Trying to break out of jail!')
			self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/index.php?action=breakout')
			self.profile.loadProfile()
	def readJobList(self):
		self.jobList = self.loadJob(self.game_config.appID+'1.txt')
		self.jobList2 = self.loadJob(self.game_config.appID+'2.txt')
		self.jobList3 = self.loadJob(self.game_config.appID+'3.txt')
		print ('JobList1:', self.jobList)
		print ('JobList2:', self.jobList2)
		print ('JobList3:', self.jobList3)
	def loadJob(self, filename):
		jList=[]
		d = self.opener.getConfigFile(filename)
		f = open(d, 'r')
		iterator=re.compile('[a-z0-9A-Z?.=& ]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				jList.append((energy, count, id, prereq))
			else:
				jList.append((energy, count, id, prereq, prereq_count))
			iterator=re.compile('[a-z0-9A-Z?.=& ]+').findall(f.readline())
		#print ('Job list:', jList)
		f.close()
		return jList
	def doJob(self):
		self.readJobList()
		self.profile.loadProfile()
		self.profile.displayProfile()
		energy=self.profile.data["Energy"]
		if self.getRequiredEnergy()<=energy and not self.profile.data["Jailed"]:
			self.earn()
			self.profile.loadProfile()
			if self.profile.data["Energy"]<energy and not self.profile.data["Jailed"]:
				self.profile.deposit()
				sleepTime=(self.getRequiredEnergy()-self.profile.data["Energy"])*self.game_config.energyRechargeTime
			elif self.profile.data["Jailed"]:
				self.breakout()
				sleepTime=(self.game_config.breakoutEnergy-self.profile.data["Energy"])*self.game_config.energyRechargeTime
			else:
				sleepTime=2
		elif self.profile.data["Jailed"]:
			self.breakout()
			sleepTime=(self.game_config.breakoutEnergy-self.profile.data["Energy"])*self.game_config.energyRechargeTime
		else:
			#Not enough energy for the job
			self.profile.deposit()
			sleepTime=(self.getRequiredEnergy()-energy)*self.game_config.energyRechargeTime

		if sleepTime<0:
			sleepTime=0
		if not self.job1Failed and sleepTime<30*60:
			sleepTime=30*60
		self.sleepTime=[sleepTime, sleepTime+60]

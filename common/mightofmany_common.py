import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from .ProfileLoader import *
from .GenericProperty import *
from .FormParser import *

class GameConfig:
	appID=''
	energyPrefix=''
	energyMidfix='/'
	healthPrefix=''
	healthMidfix='/'
	staminaPrefix=''
	staminaMidfix='/'
	cashPrefix=''
	bankPrefix=''
	propertyPrefix=''
	itemPrefix='Owned:'
	jailstr=''
	bankURL=''
	propertyURL=''
	bountyURL=''
	jobURL=''
	weaponURL=''
	jailURL=''
	minDeposit=200
	energyRechargeTime=300
	jobDelay=60
	deposit=True
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.keepRetry=False
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+game_config.bankURL+'/'
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None), ('Bank', game_config.bankPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		m = re.compile(self.game_config.jailstr).search(self.page)
		if m!=None:
			self.data["Jailed"]=True
		else:
			self.data["Jailed"]=False
	def deposit (self):
		if self.game_config.deposit:
			print ('Depositing ('+self.game_config.appID+') ...')
			page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL+'/'
			html=self.opener.open(page)
			ap = SimpleFormParser('do.php', {}, {'action':'deposit'})
			ap.feed(html)
			ap.close()
			if len(ap.forms)>0:
				ap.forms[0].submit(self.opener, page+ap.forms[0].action)
	def withdraw (self, amount):
		print ('Withdraw ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL+'/'
		html=self.opener.open(page)
		ap = SimpleFormParser('do.php', {'withdraw_amount':str(amount)}, {'action':'withdraw'})
		ap.feed(html)
		ap.close()
		if len(ap.forms)>0:
			ap.forms[0].submit(self.opener, page+ap.forms[0].action)
class FightMain(JobRunner):
	opener=None
	profile=None
	game_config=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.profile=ProfileLoader(f, self.game_config)
		self.appID=self.game_config.appID
		self.jobDescription='Fight'
	def doJob(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		if not self.profile.data["Jailed"] and self.profile.data["Cash"]>=self.game_config.minDeposit:
			self.profile.deposit()
		self.sleepTime=[15*60, 15*60+60]

class JobMain(JobRunner):
	opener=None
	profile=None
	counter=0
	game_config=None
	jobList=[(6, 2, '19', 'Food', 40), (15, 1, '38', 'Boulder', 25), (30, 1, '39', None)]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Earn'
		self.profile=ProfileLoader(f, self.game_config)
	def earn (self, jobID):
		print ('Earning ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.jobURL+'/'
		ap = SimpleFormParser('http://[^"]+do.php', {}, {'jobid':jobID})
		ap.feed(self.opener.open(page))
		ap.close()
		if len(ap.forms)>0:
			ap.forms[0].submit(self.opener)
	def readJobList(self):
		self.jobList=[]
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'.txt')
		#print ('Opeing egg info file:', d)
		f = open(d, 'r')
		iterator=re.compile('[^,\n]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					itempos=int(jobInfo)
				elif i==5:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				self.jobList.append((energy, count, id, prereq))
			else:
				self.jobList.append((energy, count, id, prereq, itempos, prereq_count))
			iterator=re.compile('[^,\n]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()
	def calcJobID(self):
		self.readJobList()
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.jobURL+'/')
		while re.compile(self.game_config.energyPrefix).search(page)==None:
			print (self.game_config.appID+': Error loading the job info! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.jobURL+'/')

		self.counter=0
		iterator=re.compile(self.game_config.itemPrefix+'[ ]*[0-9]+').findall(page)

		while self.jobList[self.counter][3]!=None:
			item_count=0
			i=0
			for item_no in iterator:
				if self.jobList[self.counter][3]!=None and i==self.jobList[self.counter][4]:
					item_count=int(re.compile('[0-9]+').search(item_no).group())
				i=i+1
			if item_count>=self.jobList[self.counter][5]:
				print ('Job:', self.jobList[self.counter][3], 'finished')
				newCounter=(self.counter+1)%len(self.jobList)
				self.counter=newCounter
			else:
				break
	def doJob(self):
		self.calcJobID()
		self.profile.loadProfile()
		self.profile.displayProfile()
		energy=self.profile.data["Energy"]
		if energy>=self.jobList[self.counter][0] and not self.profile.data["Jailed"]:
			for i in range(0, self.jobList[self.counter][1]):
				self.earn(self.jobList[self.counter][2])
			self.profile.loadProfile()
			#Job done (energy deducted and not jailed)
			if energy>self.profile.data["Energy"] and not self.profile.data["Jailed"]:
				self.calcJobID()
				#energy is regenerated at 1 every 5 minutes
				#Calculate the energy required for the next job
				sleepTime=(self.jobList[self.counter][0]-self.profile.data["Energy"])*300
				if sleepTime<0:
					sleepTime=0
				self.sleepTime=[sleepTime, sleepTime+60]
			#Job NOT done (probably because of errors or jailed)
			else:
				self.sleepTime=[0, 60]
		elif self.profile.data["Jailed"]:
			while self.profile.data["Jailed"] and self.profile.data["Energy"]>0:
				print ('Trying to bust out of '+self.game_config.jailURL)
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.jailURL+'/do.php?action=bustout')
				self.profile.loadProfile()

			if not self.profile.data["Jailed"]:
				self.calcJobID()
				sleepTime=(self.jobList[self.counter][0]-self.profile.data["Energy"])*300
				self.sleepTime=[sleepTime, sleepTime+self.game_config.jobDelay]
			else:
				self.sleepTime=[300, 360]
		else:
			#Not enough energy for the current job
			sleepTime=(self.jobList[self.counter][0]-energy)*300
			self.sleepTime=[sleepTime, sleepTime+self.game_config.jobDelay]

		if self.profile.data["Cash"]>=self.game_config.minDeposit:
			self.profile.deposit()

class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def doJob(self):
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'_properties.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'_properties.txt')
		p=PropertyProcessor(self.opener)

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='http://[^"]+do.php'
		#p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.propertyURL+'do.php'
		p.propertyID_name='item'
		p.amount_name='qty'
		p.action_type='buy'
		p.action_type_name='action'

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.propertyURL+'/'):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			if profile.data["Cash"]>=money:
				p.buy()
			elif profile.data["Cash"]+profile.data["Bank"]>=money:
				profile.withdraw(money-profile.data["Cash"])
				p.buy()
			else:
				finished=True

		self.sleepTime=[60*60, 120*60]

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from .JobRunner import *
from .facebook import Facebook
from .ProfileLoader import *
from .FormParser import *
from .GenericProperty import *
from FBAuto.treasurehunt.ProfileSearcher import *

class GameConfig:
	appID=''
	energyPrefix=''
	energyMidfix='/'
	healthPrefix=''
	healthMidfix='/'
	staminaPrefix=''
	staminaMidfix='/'
	cashPrefix=''
	bankPrefix=''
	propertyPrefix=''
	propertyURL='promote'
	hasBank=False
	minDeposit=1000
	energyRechargeTime=300
class ProfileLoader(GeneralProfileLoader):
	game_config=None

	def __init__(self, f, game_config):
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None)]
		if self.game_config.hasBank:
			profile_config.profileURL=Facebook.appsURL+'/'+self.game_config.appID+'/bank.php'
			profile_config.meta.append(('Bank', game_config.bankPrefix, None))
		else:
			profile_config.profileURL=Facebook.appsURL+'/'+self.game_config.appID+'/'
		GeneralProfileLoader.__init__(self, f, profile_config)

class BOTBPropertyProcessor(PropertyProcessor):
	def loadInfoFromWeb(self, url):
		#print (url)
		self.getURL=url
		html=self.opener.open(url)
		pages=[]
		for i in range(0, len(self.propertyList)):
			if i<len(self.propertyList)-1:
				pattern=self.propertyList[i].description+'.*'+self.propertyList[i+1].description
			else:
				pattern=self.propertyList[i].description+'.*'

			m=re.compile(pattern, re.DOTALL).search(html)
			if m!=None:
				pages.append(m.group())
			
		if len(pages)==len(self.propertyList):
			i=0
			totalIncome=0
			for p in pages:
				num=0
				m=re.compile(self.itemPrefix+'[0-9]+').search(p)
				if m!=None:
					item=m.group()
					num=int(item.replace(re.compile(self.itemPrefix).search(p).group(), ''))
				self.propertyList[i].quantity=int(num)
				totalIncome=totalIncome+self.propertyList[i].quantity*self.propertyList[i].income
				i=i+1
			print ('Total income:', totalIncome)
			for p in self.propertyList:
				p.updateIncome(totalIncome)
			return True
		else:
			print ("Detected property list doesn't match with the provided info")
			return False
class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def doJob(self):
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'_properties.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'_properties.txt')
		#p=PropertyProcessor(self.opener)
		p=BOTBPropertyProcessor(self.opener)

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='/'+self.game_config.appID+'/user_items/do'
		p.amount_name='item[quantity]'
		p.embedID=True

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.propertyURL):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			profile.displayProfile()
			if profile.data['Cash']>=money:
				p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/user_items/do/'+p.getBestBuy().id
				p.buy()
			else:
				finished=True

		self.sleepTime=[60*60, 120*60]



import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from .JobRunner import *
from .facebook import Facebook
from FBAuto.treasurehunt.ProfileSearcher import *
from .GenericProperty import *
from .ProfileLoader import *

class GameConfig:
	appID=''
	energyPrefix=''
	energyMidfix=''
	healthPrefix=''
	healthMidfix=''
	staminaPrefix=''
	staminaMidfix=''
	cashPrefix=''
	bankPrefix=''
	propertyPrefix=''
	levelPrefix=''
	encoding=None
	minDepositAmount=100
	energyRechargeTime=300
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+'bank.php'
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None), ('Bank', game_config.bankPrefix, None), ('Level', game_config.levelPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)

class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def getSig(self, id):
		sig=''
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/property.php')
		m = re.compile('do_buy_property\(&#039;'+id+'&#039;,&#039;[^)]*&#039;,&#039;([a-zA-Z0-9]+)&#039;\)').search(html)
		if m!=None:
			sig=m.group(1)
		return sig
	def doJob(self):
		d = os.getcwd()
		if re.compile(self.game_config.appID.replace('-','_')).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'_properties.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'_properties.txt')
		p=PropertyProcessor(self.opener)
		p.encoding=self.game_config.encoding

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='./property.php'
		p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/property.php'
		p.propertyID_name='id'
		p.action_type='buy'
		p.action_type_name='action'
		p.isArgument=True

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/property.php'):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			if profile.data["Cash"]>=money:
				p.extraParameters={'sig':self.getSig(p.getBestBuy().id)}
				p.buy()
			else:
				finished=True

		self.sleepTime=[60*60, 120*60]


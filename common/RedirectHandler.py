import re

def urlDecode(link):
	return link.replace('\\"','"').replace('\\/','/').replace('\\n','\n').replace('\\t','\t').replace('\\\\', '\\')
def getRedirectLinks(page):
	allLinks=[]
	m = re.compile('top\.location\.href[ ]*=[ ]*"([^"]+)"').search(page)
	if m!=None:
		redirectURL=urlDecode(m.group(1))
		allLinks.append(redirectURL)
	m = re.compile('window\.location\.replace\("([^"]+)"\)').search(page)
	if m!=None:
		redirectURL=urlDecode(m.group(1))
		allLinks.append(redirectURL)
	m = re.compile('document\.location\.href[ ]*=[ ]*"([^"]+)"').search(page)
	if m!=None:
		redirectURL=urlDecode(m.group(1))
		allLinks.append(redirectURL)
	return allLinks

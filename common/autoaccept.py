import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *
from FBAuto.common.FormParser import *
import FBAuto.common.IFrameProcessor
import FBAuto.common.RedirectHandler

class JobMain(JobRunner):
	jobList=[]
	post_form_id=''
	email=''
	jobURL='/'+'?sk=games&ap=1&_fb_noscript=1'
	visitedLinks=[]
	iframeDepth=0

	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.jobList=[]
		self.appID='N/A'
		self.jobDescription='Accept Invite'
	def process_old(self, func):
		req_id=func.group(1)
		app_id=func.group(2)
		from_id=func.group(4)
		is_invite=func.group(5)
		req_type=func.group(6)
		url=func.group(7).replace('&amp;', '&').replace(' ','+')

		for j in self.jobList:
			if j[0]==app_id and re.compile(j[1]).search(url)!=None:
				header={'type':'platform_request', 'id':req_id, 'action':'accept','params[from_id]':from_id,'params[req_type]':req_type,'params[app_id]':app_id,'params[is_invite]':is_invite,'post_form_id':self.post_form_id,'_ecdc':'false'}
				input = urllib.parse.urlencode(header)
				print ('Accepting request:',)
				print ('Req id:',req_id,)
				print ('App id:', app_id,)
				print ('From ID:', from_id,)
				print ('Is Invite:', is_invite,)
				print ('Req Type:', req_type,)
				print ('URL:', url)

				html = self.opener.open(Facebook.appsURL+'/ajax/reqs.php', input)
				ajax=AjaxProcessor()
				if not ajax.validate(html):
					print ('Error occurred: '+html)
				else:
					print ('Request accepted successfully')
					self.acceptLink(url)
	def openIframes(self, html):
		self.iframeDepth=self.iframeDepth+1
		links = FBAuto.common.IFrameProcessor.getLinks(html)
		for l in links:
			self.acceptLink(l)
		self.iframeDepth=self.iframeDepth-1
	def handleRedirect(self, page):
		links=FBAuto.common.RedirectHandler.getRedirectLinks(page)
		while len(links)>0:
			page=self.opener.open(links[0])
			links=FBAuto.common.RedirectHandler.getRedirectLinks(page)
		return page
	def acceptLink(self, url):
		if self.visitedLinks.count(url)==0 and self.iframeDepth<10:
			print ("IFrame Depth:", self.iframeDepth, "No. of Links Visited:", len(self.visitedLinks))
			self.visitedLinks.append(url)
			html=self.opener.open(url.replace(' ', '%20'))
			html=self.handleRedirect(html)
			self.openIframes(html)
	def readJobList(self):
		self.jobList=[]
		f = open(self.opener.getConfigFile('autoaccept'+self.email+'.txt'), 'r')
		iterator=re.compile('[^ ]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					id=jobInfo.strip('\n')
				elif i==1:
					pattern=jobInfo.strip('\n')
				i=i+1
			self.jobList.append((id, pattern))
			iterator=re.compile('[^ ]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()
	def doJob(self):
		self.readJobList()
		html=self.opener.loadPage(Facebook.facebookURL+self.jobURL)
		#html=self.opener.loadPage(Facebook.facebookURL+'/'+'reqs.php')
		invites=SimpleFormParser('/ajax/reqs.php', {}, {})
		try:
			invites.feed(html)
			invites.close()
		except:
			f=open('tmp.htm', 'wb')
			f.write(html.encode("utf-8", "ignore"))
			f.close()
			traceback.print_exc(file=sys.stdout)
		print ('Total invites:', len(invites.forms))
		for f in invites.forms:
			self.process(f)
		self.sleepTime=[1800*1, 1800*2]
	def process(self, invite):
		finished=False
		for j in self.jobList:
			if 'params[app_id]' in invite.header and invite.header['params[app_id]']==j[0]:
				for k,v in invite.header.items():
					m = re.compile('actions\[(.*)\]').search(k)
					if m!=None and re.compile(j[1]).search(m.group(1))!=None:
						self.doAccept(invite, m.group(1))
						#self.createAcceptThread(invite, m.group(1))
						#invite.submit(self.opener, Facebook.appsURL+'/ajax/reqs.php')
						#self.acceptLink(m.group(1))
						finished=True
					if finished:
						break
			if finished:
				break
	def doAccept(self, invite, acceptLink):
		invite.submit(self.opener, Facebook.appsURL+'/ajax/reqs.php')
		#print ('accepting link:', m.group(1))
		self.visitedLinks=[]
		self.iframeDepth=0
		self.acceptLink(acceptLink)
	def createAcceptThread(self, invite, acceptLink):
		t=AcceptThread(self, invite, acceptLink)
		t.start()
	def loadInfo(self, html):
		m=re.compile('post_form_id=[a-zA-Z0-9]+').search(html)
		if m!=None:
			self.post_form_id=re.compile('[a-zA-Z0-9]+').search(re.compile('=[a-zA-Z0-9]+').search(m.group()).group()).group()

class AcceptThread(threading.Thread):
	accepter=None
	invite=None
	acceptLink=None
	def __init__ (self, accepter, invite, acceptLink):
		threading.Thread.__init__(self)
		self.accepter=accepter
		self.invite=invite
		self.acceptLink=acceptLink
	def run(self):
		self.accepter.doAccept(self.invite, self.acceptLink)
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = JobMain(f)
		job.email=f.email
		job.doJob()
		f.logout()
	else:
		job = JobMain(f)
		job.start()


import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.FormParser import *
from FBAuto.common.ProfileLoader import *
from FBAuto.common.GenericProperty import *

class GameConfig:
	appID=''
	energyPrefix=''
	energyMidfix=''
	healthPrefix=''
	healthMidfix=''
	staminaPrefix=''
	staminaMidfix=''
	cashPrefix=''
	bankPrefix=''
	levelPrefix=''
	propertyPrefix=''
	jailstr=''
	energyRechargeTime=300
class ProfileLoader (GeneralProfileLoader):
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID = game_config.appID
		profile_config.keepRetry = False
		profile_config.profileURL=Facebook.appsURL+'/'+self.game_config.appID+'/bank'
		profile_config.meta = [('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None), ('Bank', game_config.bankPrefix, None), ('Level', game_config.levelPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)

class JobMain(JobRunner):
	opener=None
	profile=None
	counter=0
	jobList=[(50, 1, '29', None)]
	#jobList=[(35, 1, '28', None)]
	#jobList=[(30, 1, '27', None)]
	targetLevel=60
	energyRechargeTime=120
	game_config=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.profile=ProfileLoader(f, self.game_config)
		self.appID=self.game_config.appID
		self.jobDescription='Earn'
	def earn (self, jobID):
		print ('Earning ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/job'
		ap = SimpleFormParser('/'+self.game_config.appID+'/job/do_job', {}, {'job_id':jobID})
		ap.feed(self.opener.open(page))
		ap.close()
		if len(ap.forms)>0:
			ap.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/job/do_job')
	def calcJobID(self):
		self.readJobList()
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/weapon')
		self.counter=0
		while self.jobList[self.counter][3]!=None and re.compile(self.jobList[self.counter][3]).search(page)!=None:
			print ('Job:', self.jobList[self.counter][3], 'finished')
			newCounter=(self.counter+1)%len(self.jobList)
			if self.counter==newCounter:
				break
			else:
				self.counter=newCounter
	def readJobList(self):
		self.jobList=[]
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'.txt')
		#print ('Opeing egg info file:', d)
		f = open(d, 'r')
		iterator=re.compile('[a-z0-9A-Z]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				self.jobList.append((energy, count, id, prereq))
			else:
				self.jobList.append((energy, count, id, prereq, prereq_count))
			iterator=re.compile('[a-z0-9A-Z]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()
	def doJob(self):
		self.calcJobID()
		self.profile.loadProfile()
		self.profile.displayProfile()
		energy=self.profile.data['Energy']
		if energy>=self.jobList[self.counter][0]:
			for i in range(0, self.jobList[self.counter][1]):
				self.earn(self.jobList[self.counter][2])
			self.profile.loadProfile()
			#Job done (energy deducted and not jailed)
			if energy>self.profile.data['Energy']:
				self.calcJobID()
				#Endurance is regenerated at 1 every 5 minutes
				sleepTime=(self.jobList[self.counter][0]-self.profile.data['Energy'])*self.energyRechargeTime
				if sleepTime<0:
					sleepTime=0
				self.sleepTime=[sleepTime, sleepTime+60]
			#Job NOT done (probably because of errors or jailed)
			else:
				self.sleepTime=[0, 60]
		else:
			#Not enough energy for the current job
			sleepTime=(self.jobList[self.counter][0]-energy)*120
			if sleepTime<0:
				sleepTime=0
			self.sleepTime=[sleepTime, sleepTime+60*10]

class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def doJob(self):
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'_properties.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'_properties.txt')
		p=PropertyProcessor(self.opener)

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='/'+self.game_config.appID+'/business/buy_business'
		p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/business/buy_business'
		p.propertyID_name='bus_id'
		p.amount_name='quantity'
		p.action_type='Buy This!'
		p.action_type_name='mysubmit'

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/business'):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			if profile.data['Cash']>=money:
				p.buy()
			else:
				finished=True

		self.sleepTime=[60*60, 120*60]

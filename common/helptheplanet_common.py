import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.ProfileLoader import *
from FBAuto.common.FormParser import *
from FBAuto.treasurehunt.ProfileSearcher import *

class GameConfig:
	appID='helptheplanet'
	appNum='53454691152'
	supportPrefix=''
	supportMidfix=''
	cashPrefix=''
	requiredSupport=0
	buy1=[]
	buy2=[]

class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+'build.php'
		profile_config.meta=[('Support', game_config.supportPrefix, game_config.supportMidfix), ('Cash', game_config.cashPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		m=re.compile('<font color="([a-z]+)">[\-]*[0-9,]+').search(self.page)
		if m!=None and m.group(1)=='red':
			self.data['OK']=0
		else:
			self.data['OK']=1

class BuildMain(JobRunner):
	game_config=None
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Stop Global Warming (build)'
	def doJob(self):
		self.profile = ProfileLoader(self.opener, self.game_config)
		self.profile.loadProfile()
		self.profile.displayProfile()

		if self.profile.data["Support"] >= self.game_config.requiredSupport:
			toBuy=self.game_config.buy1
			if self.profile.data["OK"]==1:
				toBuy=self.game_config.buy2

			f = SimpleFormParser('build.php', {'amount': str(toBuy[1])}, {'which':toBuy[0]})
			f.feed(self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'build.php?show=1'))
			f.close()
			if len(f.forms)>0:
				f.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+'build.php')

		self.sleepTime=[1800, 1800*2]

class JobMain(JobRunner):
	scheduled_sleepTime=[15*60, 30*60]
	scheduled_url='requests.php'
	game_config=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Stop Global Warming (help)'
	def foundLink(self, link):
		ownerID=re.compile('[0-9]+').search(link).group()

		print ('Helping ('+self.game_config.appID+'): '+ownerID)

		links = LinkSearcher(self.opener, 'players\.php\?action=support&amp;id=[0-9]+')
		links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+'players.php?id='+ownerID
		links.callback=None
		links.load('')
		for l in links.linkList:
			decoded_link=l.replace('&amp;','&')
			self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+decoded_link)

	def doJob(self):
		links = LinkSearcher(self.opener, 'players\.php\?id=[0-9]+')
		links.callback=self
		links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.scheduled_url
		links.load('')

		self.sleepTime=self.scheduled_sleepTime


import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from .ProfileLoader import *
from .FormParser import *
from .JobRunner import *
from .facebook import Facebook
from FBAuto.treasurehunt.ProfileSearcher import *
from .GenericProperty import *

class GameConfig:
	appID=''
	energyPrefix=''
	energyMidfix=''
	healthPrefix=''
	healthMidfix=''
	staminaPrefix=''
	staminaMidfix=''
	cashPrefix=''
	bankPrefix=''
	propertyPrefix=''
	jailstr=''
	bankURL='accounts.php'
	enemyLevelPrefix='Level:[^0-9]*'
	levelPrefix='Level:[ ]*'
	stayInHospital=False
	heal=False
	doHit=True
	minDepositAmount=1000
	energyRechargeTime=300
	propertyIsRunning=False
	depositOnly=False

class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=self.game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL
		profile_config.meta=[('Energy', self.game_config.energyPrefix, self.game_config.energyMidfix), ('Health', self.game_config.healthPrefix, self.game_config.healthMidfix), ('Stamina', self.game_config.staminaPrefix, self.game_config.staminaMidfix), ('Cash', self.game_config.cashPrefix, None), ('Bank', self.game_config.bankPrefix, None), ('Level', self.game_config.levelPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
		self.jailstr=self.game_config.jailstr
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		if re.compile(self.jailstr).search(self.page)!=None:
			self.data["Jailed"]=True
		else:
			self.data["Jailed"]=False
	def deposit (self):
		if self.data["Cash"]>=self.game_config.minDepositAmount and not self.game_config.propertyIsRunning:
			print ('Depositing ('+self.game_config.appID+') ...')
			page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL
			html=self.opener.open(page)
			p = SimpleFormParser(self.game_config.bankURL, {}, {'action':'deposit'})
			try:
				p.feed(html)
				p.close()
			except HTMLParseError:
				pass
			if len(p.forms)>0:
				p.forms[0].submit(self.opener, page)
	def withdraw (self, amount):
		print ('Withdraw ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.bankURL
		html=self.opener.open(page)
		p = SimpleFormParser(self.game_config.bankURL, {'withdraw_amount':str(amount)}, {'action':'withdraw'})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		if len(p.forms)>0:
			p.forms[0].submit(self.opener, page)
	def heal(self):
		page = Facebook.appsURL+'/'+self.game_config.appID+'/hospital.php'
		html = self.opener.open(page)
		self.loadProfile(html)

		while self.data["Health"]<self.data["HealthMax"]:
			p = SimpleFormParser('hospital.php', {}, {})
			try:
				p.feed(html)
				p.close()
			except HTMLParseError:
				pass

			if len(p.forms)>0:
				html = p.forms[0].submit(self.opener, page)
			self.loadProfile(html)
class HealMain(JobRunner):
	profile=None
	game_config=GameConfig()
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Heal'
		self.profile=ProfileLoader(f, self.game_config)
	def updateSleepTime(self):
		t=60
		self.sleepTime=[t-2, t+2]
	def doJob(self):
		if not self.game_config.depositOnly:
			self.profile.heal()
		else:
			self.profile.loadProfile()
		self.profile.deposit()
		self.profile.displayProfile()
		self.updateSleepTime()
class FightMain(JobRunner):
	opener=None
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.profile=ProfileLoader(f, self.game_config)
		self.appID=self.game_config.appID
		self.jobDescription='Fight'
	def hit (self):
		print ('Checking hit list ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/bounties.php?action=show_all'
		html=self.opener.open(page)

		while self.profile.data["Health"]>self.profile.data["HealthMax"]/5 and self.profile.data["Stamina"]>0:
			allforms = re.compile('([0-9]+) minute ago.*<form[^>]+>.*</form>', re.DOTALL).finditer(html)
			hitCount=0
			for m in allforms:
				if int(m.group(1))<=1:
					p = SimpleFormParser('bounties.php', {}, {})
					try:
						p.feed(html)
						p.close()
					except HTMLParseError:
						pass
					if len(p.forms)>0:
						html = p.forms[0].submit(self.opener, page)
						self.profile.loadProfile(html)
					hitCount=hitCount+1
			if hitCount==0:
				print ('No bounty to collect!')
				self.profile.loadProfile(html)
				break
	def doJob(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		if not self.profile.data["Jailed"]:
			if self.game_config.doHit:
				self.hit()
			if self.game_config.heal:
				self.profile.heal()
			self.profile.deposit()

		self.sleepTime=[15*60, 16*60]

class JobMain(JobRunner):
	opener=None
	profile=None
	counter=0
	energyRechargeTime=289
	jobList=[(61, 1, '25', None)]
	#jobList=[(53, 1, '21', None)]
	#jobList=[(45, 1, '20', None)]
	#jobList=[(21, 1, '17', None)]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.jobDescription='Earn'
		self.profile=ProfileLoader(f, self.game_config)
		self.appID=self.game_config.appID
		self.energyRechargeTime=self.game_config.energyRechargeTime
	def earn (self, jobID):
		print ('Earning ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/missions.php'
		html = self.opener.open(page)
		p = SimpleFormParser('missions.php', {}, {'missionid':jobID})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		if len(p.forms)>0:
			p.forms[0].submit(self.opener, page)
	def calcJobID(self):
		self.readJobList()
		page=self.opener.open(self.game_config.itemURL)
		while re.compile(self.game_config.energyPrefix).search(page)==None:
			print (self.game_config.appID+': Error loading the job info! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			page=self.opener.open(self.game_config.itemURL)
		self.counter=0
		while self.jobList[self.counter][3]!=None and self.jobFinished(page):
			print ('Job:', self.jobList[self.counter][3], 'finished')
			newCounter=(self.counter+1)%len(self.jobList)
			if self.counter==newCounter:
				break
			else:
				self.counter=newCounter
	def jobFinished (self, page):
		m=re.compile(self.jobList[self.counter][3]+'[ ]*<br[ /]*>[ ]*Owned:[ ]*[0-9]+').search(page)
		if m!=None:
			item_count=int(re.compile('[0-9]+').search(m.group()).group())
			if item_count>=self.jobList[self.counter][4]:
				return True
		return False
	def readJobList(self):
		self.jobList=[]
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'.txt')
		#print ('Opeing egg info file:', d)
		f = open(d, 'r')
		iterator=re.compile('[^,\n]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				self.jobList.append((energy, count, id, prereq))
			else:
				self.jobList.append((energy, count, id, prereq, prereq_count))
			iterator=re.compile('[^,\n]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()
	def doJob(self):
		self.calcJobID()
		self.profile.loadProfile()
		self.profile.displayProfile()
		endurance=self.profile.data["Energy"]
		if endurance>=self.jobList[self.counter][0] and not self.profile.data["Jailed"]:
			for i in range(0, self.jobList[self.counter][1]):
				self.earn(self.jobList[self.counter][2])
			self.profile.loadProfile()
			#Job done (endurance deducted and not jailed)
			if endurance>self.profile.data["Energy"] and not self.profile.data["Jailed"]:
				self.calcJobID()
				#Endurance is regenerated at 1 every 5 minutes
				sleepTime=(self.jobList[self.counter][0]-self.profile.data["Energy"])*self.energyRechargeTime
				if sleepTime<0:
					sleepTime=0
				self.sleepTime=[sleepTime, sleepTime+60]
			#Job NOT done (probably because of errors or jailed)
			else:
				self.sleepTime=[0, 60]
		elif self.profile.data["Jailed"]:
			while self.profile.data["Jailed"] and self.profile.data["Energy"]>0:
				print ('Trying to bust out of prison')
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/prison.php?action=breakout')
				self.profile.loadProfile()

			if not self.profile.data["Jailed"]:
				self.calcJobID()
				sleepTime=(self.jobList[self.counter][0]-self.profile.data["Energy"])*self.energyRechargeTime
				self.sleepTime=[sleepTime, sleepTime+60]
			else:
				self.sleepTime=[self.energyRechargeTime, self.energyRechargeTime+60]
		else:
			#Not enough endurance for the current job
			sleepTime=(self.jobList[self.counter][0]-endurance)*self.energyRechargeTime
			self.sleepTime=[sleepTime, sleepTime+60]
		self.profile.deposit()

class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def doJob(self):
		self.game_config.propertyIsRunning=True
		d = os.getcwd()
		if re.compile(self.game_config.appID).search(d)==None:
			d=os.path.join(d, 'config', self.game_config.appID+'_properties.txt')
		else:
			d=os.path.join(d, '..', 'config', self.game_config.appID+'_properties.txt')
		p=PropertyProcessor(self.opener)

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='investments.php'
		p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/investments.php'
		p.propertyID_name='idProperty'
		p.amount_name='buy'
		p.action_type='buy'
		p.action_type_name='action'

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/investments.php'):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			if profile.data["Cash"]>=money:
				p.buy()
			elif profile.data["Cash"]+profile.data["Bank"]>=money:
				profile.withdraw(money-profile.data["Cash"])
				p.buy()
			else:
				finished=True

		self.sleepTime=[60*60, 120*60]
		self.game_config.propertyIsRunning=False


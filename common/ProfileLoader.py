import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from .JobRunner import *
from .facebook import Facebook

class ProfileConfig:
	appID=''
	keepRetry=True
	profileURL=None
	meta=[]

class WhiteListLoader:
	opener=None
	whiteList=[]
	def __init__(self, opener):
		self.opener=opener
		self.whiteList=[]
		f = open(self.opener.getConfigFile('whitelist.txt'))
		id = f.readline().strip('\n')
		while len(id)>0:
			self.whiteList.append(id)
			id=f.readline().strip('\n')
		f.close()
	
class GeneralProfileLoader:
	page=''
	data=None
	profile_config=None
	opener=None
	dotAll=False

	def __init__(self, opener, profile_config):
		self.page=''
		self.opener=opener
		self.profile_config=profile_config
		self.dotAll=False
	def validPage(self, html):
		if re.compile(self.profile_config.meta[0][1]).search(html)==None:
			return False
		else:
			return True
	def loadPage(self, url):
		html=''
		try:
			html=self.opener.open(url)
		except:
			traceback.print_exc(file=sys.stdout)
		while not self.validPage(html) and self.profile_config.keepRetry:
			print (self.profile_config.appID+'('+self.opener.thisUserID+')'+': Error loading the profile! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			try:
				html=self.opener.open(self.profile_config.profileURL)
			except:
				traceback.print_exc(file=sys.stdout)
		return html
	def loadProfile (self, html=None):
		self.data={}
		if html==None:
			self.page=self.loadPage(self.profile_config.profileURL)
		else:
			self.page=html

		for d in self.profile_config.meta:
			if d[2]!=None:
				if self.dotAll:
					m=re.compile(d[1]+"(?P<field1>[0-9,]+)"+d[2]+"(?P<field2>[0-9,]+)", re.DOTALL).search(self.page)
				else:
					m=re.compile(d[1]+"(?P<field1>[0-9,]+)"+d[2]+"(?P<field2>[0-9,]+)").search(self.page)
				if m!=None:
					self.data[d[0]]=int(m.group('field1').replace(',',''))
					self.data[d[0]+'Max']=int(m.group('field2').replace(',',''))
				else:
					self.data[d[0]]=0
					self.data[d[0]+'Max']=0
			else:
				if self.dotAll:
					m=re.compile(d[1]+"(?P<field1>[0-9,]+)", re.DOTALL).search(self.page)
				else:
					m=re.compile(d[1]+"(?P<field1>[0-9,]+)").search(self.page)
				if m!=None:
					self.data[d[0]]=int(m.group('field1').replace(',',''))
				else:
					self.data[d[0]]=0

	def displayProfile(self):
		tmp=[]
		for k, v in self.data.items():
			if re.compile('Max').search(k)!=None:
				realAttr = k.replace('Max', '')
				keyFound=False
				for i in tmp:
					if i[0]==realAttr:
						i.append(v)
						keyFound=True
				if not keyFound:
					tmp.append([realAttr, 0, v])
			else:
				keyFound=False
				for i in tmp:
					if i[0]==k:
						i[1]=v
						keyFound=True
				if not keyFound:
					tmp.append([k, v])

		profileStr = 'Profile loaded ('+self.profile_config.appID+') '
		for t in tmp:
			if len(t)>2:
				profileStr = profileStr+t[0]+': '+str(t[1])+'/'+str(t[2])+' '
			else:
				profileStr = profileStr+t[0]+': '+str(t[1])+' '
		print (profileStr)
if __name__ == "__main__":
	f = Facebook('facebook')
	w = WhiteListLoader(f)
	print (w.whiteList)

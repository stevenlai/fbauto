import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import math
import getopt
from html.parser import HTMLParser
from html.parser import HTMLParseError
from FBAuto.common.facebook import Facebook
from FBAuto.common.FormParser import *
from FBAuto.common.JobRunner import *

class PropertyInfo:
	id=''
	description=''
	price=0
	income=0
	buying_quantity=0
	pre_req=None
	quantity=0
	ror=0
	totalIncome=0
	timeToBuy=10000000000

	def getPrice(self):
		return self.price+self.price*self.quantity/10
	def updateIncome(self, totalIncome):
		self.totalIncome=totalIncome
		if self.totalIncome>0:
			self.timeToBuy=float(self.price*self.buying_quantity)/self.totalIncome
	def compareROR(p1, p2):
		if (p1.getPrice()*p1.buying_quantity>p2.getPrice()*p2.buying_quantity and p1.quantity==0) or (p2.getPrice()*p2.buying_quantity>p1.getPrice()*p1.buying_quantity and p2.quantity==0):
			if p1.getPrice()*p1.buying_quantity>p2.getPrice()*p2.buying_quantity:
				t1=float(p1.getPrice()*p1.buying_quantity)/p1.totalIncome
				t2=float(p2.getPrice()*p2.buying_quantity)/p2.totalIncome+float(p1.getPrice()*p1.buying_quantity)/(p1.totalIncome+p2.income*p2.buying_quantity)
			else:
				t1=float(p1.getPrice()*p1.buying_quantity)/p1.totalIncome+float(p2.getPrice()*p2.buying_quantity)/(p2.totalIncome+p1.income*p1.buying_quantity)
				t2=float(p2.getPrice()*p2.buying_quantity)/p1.totalIncome
			#print ('Comparing:',p1,t1,p2,t2)
			return cmp(t1, t2)
			#if t1<t2:
				#return -1
			#elif t1>t2:
				#return 1
			#else:
				#return 0
		else:
			if p1.ror!=p2.ror:
				return cmp(p1.ror,p2.ror)
				#return int(sys.maxint/(p1.ror-p2.ror))
			elif p1.getPrice()!=p2.getPrice():
				return cmp(p1.getPrice(),p2.getPrice())
				#return int(sys.maxint/(p1.getPrice()-p2.getPrice()))
			else:
				return 0
	def __str__(self):
		return self.description+','+' quantity: '+str(self.quantity)+', each buy: '+str(self.buying_quantity)+', price: '+str(self.getPrice())+', income: '+str(self.income)+', ror: '+str(self.ror)+', time to buy: '+str(self.timeToBuy)

class PropertyProcessor:
	opener=None
	propertyList=[]
	itemPrefix=''

	#HTML Form related
	action=''
	action_type=''
	action_type_name=''
	propertyID_name=''
	amount_name=''
	postURL=''
	getURL=''
	embedID=False
	maxROR=10000000000000
	isArgument=False
	useAjax=False
	encoding=None
	extraParameters={}

	def __init__(self, f):
		self.jobDescription='property'
		self.opener=f
		self.propertyList=[]
		self.itemPrefix=''
		self.action=''
		self.action_type=''
		self.action_type_name=''
		self.propertyID_name=''
		self.amount_name=''
		self.postURL=''
		self.embedID=False
		self.isArgument=False
		self.useAjax=False
		self.encoding=None
		self.extraParameters={}
	def loadInfoFromFile(self, file):
		self.propertyList=[]
		f = open(file, 'r')
		iterator=re.compile('[^,\n]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			p=PropertyInfo()
			for jobInfo in iterator:
				if i==0:
					p.id=jobInfo
				elif i==1:
					p.description=jobInfo
				elif i==2:
					p.price=int(jobInfo)
				elif i==3:
					p.income=int(jobInfo)
				elif i==4:
					p.buying_quantity=int(jobInfo)
				elif i==5:
					if jobInfo=='None':
						p.pre_req=None
					else:
						p.pre_req=jobInfo
				i=i+1
			self.propertyList.append(p)
			iterator=re.compile('[^,\n]+').findall(f.readline())
		f.close()
	def loadInfoFromWeb(self, url):
		#print (url)
		self.getURL=url
		html=self.opener.open(url)
		iterator=re.compile(self.itemPrefix+'[0-9,]+').findall(html)
		i=0
		if len(iterator)==len(self.propertyList):
			totalIncome=0
			for item in iterator:
				num=item.replace(re.compile(self.itemPrefix).search(item).group(), '')
				self.propertyList[i].quantity=int(num.replace(',', ''))
				totalIncome=totalIncome+self.propertyList[i].quantity*self.propertyList[i].income
				i=i+1

			print ('Total income:', totalIncome)

			for p in self.propertyList:
				p.updateIncome(totalIncome)
			return True
		else:
			print ("Detected property list doesn't match with the provided info")
			return False
	def getNeededMoney(self):
		self.updateROR()
		self.display()
		bestBuy = self.getBestBuy()
		print ('Best buy:',bestBuy)
		if bestBuy.pre_req==None:
			return bestBuy.getPrice()*bestBuy.buying_quantity
		else:
			for p in self.propertyList:
				if p.id==bestBuy.pre_req:
					price=bestBuy.getPrice()*bestBuy.buying_quantity
					if p.quantity>=bestBuy.buying_quantity:
						return price
					else:
						return price+(bestBuy.buying_quantity-p.quantity)*p.getPrice()
	def getBestBuy(self):
		self.propertyList.sort(PropertyInfo.compareROR)
		#print ('Sorted: ')
		#self.display()
		bestBuy=None
		for p in self.propertyList:
			isLand=False
			if p.pre_req==None and p.quantity>0:
				for q in self.propertyList:
					if q.pre_req==p.id:
						isLand=True
			if not isLand:
				bestBuy=p
				for q in self.propertyList:
					if q.id==p.pre_req and q.quantity<p.buying_quantity:
						bestBuy=q
			if bestBuy!=None:
				break
		return bestBuy
	def buy(self):
		bestBuy=self.getBestBuy()
		form_action=self.action
		form_fill={}
		form_filter={}
		if self.embedID:
			form_action=self.action+'/'+bestBuy.id
		elif self.isArgument:
			form_fill[self.propertyID_name]=bestBuy.id
		else:
			form_filter[self.propertyID_name]=bestBuy.id

		if self.isArgument:
			form_fill[self.action_type_name]=self.action_type
		elif self.action_type_name!='':
			form_filter[self.action_type_name]=self.action_type

		if self.amount_name!='':
			form_fill[self.amount_name]=str(bestBuy.buying_quantity)

		print (form_action, form_fill, form_filter)
		p=SimpleFormParser(form_action, form_fill, form_filter)
		try:
			p.feed(self.opener.open(self.getURL))
			p.close()
		except HTMLParseError:
			pass

		if len(p.forms)>0:
			p.forms[0].header.update(self.extraParameters)
			if self.postURL!='':
				p.forms[0].submit(self.opener, self.postURL)
			else:
				p.forms[0].submit(self.opener, p.forms[0].action)
		else:
			print ('Form not found!')
	def updateROR(self):
		for p in self.propertyList:
			if p.income==0:
				ror=self.maxROR
			elif p.pre_req==None:
				ror=(p.getPrice())/p.income
			else:
				for q in self.propertyList:
					if q.id==p.pre_req:
						ror=(p.getPrice()+q.price)/p.income
			p.ror=ror
		#self.display()
	def display (self):
		for p in self.propertyList:
			print (p)

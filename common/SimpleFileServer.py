import re
import socket
import sys
import os
import os.path
from FBAuto.common.SimpleFileServerCommon import *

class SimpleFileHandler(ServerHandler, FileProcessor):
	def validFileName(self, filename):
		m = re.compile('[\\/ ]').search(filename)
		if m==None:
			return True
		else:
			return False
	def processCommand(self, command, arg):
		ServerHandler.processCommand(self, command, arg)
		protocol = ProtocolProcessor()
		if command=='put':
			c = self.getCurrentClient()
			if c==None or not self.validFileName(arg):
				protocol.sendAllData(self.conn, protocol.generateInput('FAIL', ''))
			else:
				c.putFile=arg
				protocol.sendAllData(self.conn, protocol.generateInput('SUCCESS', ''))
		elif command=='get':
			c = self.getCurrentClient()
			if c==None or not self.validFileName(arg):
				protocol.sendAllData(self.conn, protocol.generateInput('FAIL', ''))
			else:
				protocol.sendAllData(self.conn, protocol.generateInput('SUCCESS', self.getFileData(arg)))
		elif command=='file':
			c = self.getCurrentClient()
			if c==None or c.putFile=='':
				protocol.sendAllData(self.conn, protocol.generateInput('FAIL', ''))
			else:
				self.writeFile(c.putFile, arg)
				protocol.sendAllData(self.conn, protocol.generateInput('SUCCESS', ''))
				c.putFile=''
if __name__ == "__main__":
	server = ServerBase("0.0.0.0", ProtocolProcessor.PORT)
	server.handlerClass=SimpleFileHandler
	#server_thread.setDaemon(True)
	server.start()

import re
import socket
import sys
import os
import os.path
import threading
import traceback

class ProtocolProcessor:
	HOST='imcl.comp.polyu.edu.hk'
	PORT=25002
	BUFFER_SIZE=1024
	def processHead(self, sock):
		allData=''.encode("utf-8", "ignore")
		m=re.compile('([^ ]+) ([0-9]+) ').search(allData.decode("utf-8", "ignore"))
		while m==None:
			data=sock.recv(1)
			if not data:
				break
			allData=allData+data
			m=re.compile('([^ ]+) ([0-9]+) ').search(allData.decode("utf-8", "ignore"))
		if m!=None:
			length=int(m.group(2))
			return m.group(1), length
		else:
			#print ('Incorrect header', allData)
			return allData.decode("utf-8", "ignore"), 0
	def receiveAllData(self, sock):
		head, length=self.processHead(sock)
		data=''.encode("utf-8", "ignore")
		receivedLength=0
		while length>receivedLength:
			receivedData=sock.recv(ProtocolProcessor.BUFFER_SIZE)
			data=data+receivedData
			receivedLength=receivedLength+len(receivedData)
		#print ('received header', head, length, 'Real length', receivedLength)
		return (head, data.decode("utf-8", "ignore"))
	def sendAllData(self, sock, data):
		data=data.encode("utf-8", "ignore")
		while len(data)>0:
			sentByte=sock.send(data)
			data=data[sentByte:]
	def generateInput(self, command, arg):
		return command+' '+str(len(arg))+' '+arg
class FileProcessor:
	fileDir=None
	def getConfigFileName(self, filename):
		if FileProcessor.fileDir!=None:
			return os.path.join(FileProcessor.fileDir, filename)

		d = os.getcwd()
		if re.compile('FBAuto$').search(d)!=None:
			d=os.path.join(d, 'config', filename)
		else:
			d=os.path.join(d, '..', 'config', filename)
		return d
	def getFileData(self, filename):
		if not os.path.isfile(self.getConfigFileName(filename)):
			return ''
		f = open(self.getConfigFileName(filename), 'r')
		data = f.read()
		f.close()
		return data
	def writeFile(self, filename, data):
		f = open(self.getConfigFileName(filename), 'w')
		f.write(data)
		f.close()
class ClientSession:
	address=''
	putFile=''
class ClientBase:
	host=''
	port=0
	def __init__(self, host, port):
		self.host=host
		self.port=port
	def sendData(self, sock, request):
		protocol = ProtocolProcessor()
		protocol.sendAllData(sock, request)
		response = protocol.receiveAllData(sock)
		return response

class ServerHandler(threading.Thread):
	ClientSession=[]
	conn=None
	addr=None
	command=None
	def __init__(self, conn, addr):
		threading.Thread.__init__(self)
		self.conn=conn
		self.addr=addr
		self.command=None
	def processCommand(self, command, arg):
		dbgStr='Processing: '+command+' '
		if command=='put':
			dbgStr=dbgStr+arg
		print (dbgStr)

		protocol = ProtocolProcessor()
		if command=='login':
			if arg=='auth':
				if self.getCurrentClient()==None:
					c=ClientSession()
					c.address=self.addr
					ServerHandler.ClientSession.append(c)
				protocol.sendAllData(self.conn, protocol.generateInput('SUCCESS', ''))
			else:
				protocol.sendAllData(self.conn, protocol.generateInput('FAIL', ''))
	def getCurrentClient(self):
		for c in ServerHandler.ClientSession:
			if c.address==self.addr:
				return c
		return None
	def handle(self):
		protocol = ProtocolProcessor()
		requestData = protocol.receiveAllData(self.conn)
		self.command=requestData[0]
		self.processCommand(requestData[0], requestData[1])
	def run(self):
		while self.command!="logout" and self.command!="":
			try:
				self.handle()
			except:
				break
		self.conn.close()
class ServerBase(threading.Thread):
	host=''
	port=0
	handlerClass=ServerHandler
	serverStopped=True
	sock=None
	def __init__(self, host, port):
		threading.Thread.__init__(self)
		self.host=host
		self.port=port
		self.serverStopped=True
		self.sock=None
	def stop(self):
		self.serverStopped=True
	def start(self):
		self.serverStopped=False
		threading.Thread.start(self)
	def listen(self):
		finishedStarting=False
		while not finishedStarting:
			try:
				self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				self.sock.bind((self.host, self.port))
				self.sock.listen(5)
				finishedStarting=True
			except:
				traceback.print_exc(file=sys.stdout)
		print ('Server started')
	def accept(self):
		try:
			conn, addr = self.sock.accept()
			print ('Accepted:', addr)
			s=self.handlerClass(conn, addr)
			s.start()
		except:
			traceback.print_exc(file=sys.stdout)
	def run(self):
		while not self.serverStopped:
			if self.sock==None:
				self.listen()
			self.accept()

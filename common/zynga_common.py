import os.path
import sys
import traceback
import urllib.parse
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from html.parser import HTMLParseError
from .ProfileLoader import *
from .FormParser import *
from .JobRunner import *
from .AjaxProcessor import *
import FBAuto.common.IFrameProcessor
import FBAuto.common.RedirectHandler
from .facebook import Facebook
from FBAuto.treasurehunt.ProfileSearcher import *
import FBAuto.refresher.refresher
from .GenericProperty import *

class GameConfig:
	appID=''
	urlPrefix=''
	bankURL='bank.php'
	statsURL='stats.php'
	hospitalURL='hospital.php'
	newsURL='index.php'
	jobURL='jobs.php'
	inventoryURL='inventory.php'
	fpURL='buy.php'
	iframeURL=''
	hospitalAction='.*'
	hospitalActionName='action'
	bankActionName='action'
	energyPrefix=''
	energyMidfix='</span>/[ ]*'
	healthPrefix=''
	healthMidfix='</span>/[ ]*'
	staminaPrefix=''
	staminaMidfix='</span>/[ ]*'
	cashPrefix=''
	bankPrefix=''
	memberPrefix=''
	levelPrefix='Level:[ ]*'
	propertyPrefix=''
	attackStatsPrefix='Attack Strength<[^>]*>[^<]*<[^>]*>'
	defenseStatsPrefix='Defense Strength<[^>]*>[^<]*<[^>]*>'
	energyStatsPrefix='Maximum Energy<[^>]*>[^<]*<[^>]*>'
	healthStatsPrefix='Maximum Health<[^>]*>[^<]*<[^>]*>'
	rageStatsPrefix='Maximum Rage<[^>]*>[^<]*<[^>]*>'
	availPointsPrefix='You have <[^>]*>'
	fpPrefix='You have[ ]*'
	jailstr=''
	feedPrefix=''
	enemyLevelPrefix='level[ ]*'
	feedTag='<div class="news_title">[0-9]+ minutes ago:</div><div class="news_content">'
	KOMsg='You were knocked out'
	winMsg=''
	fightMsg=''
	adminID=''
	useEnergyPack=True
	fightFromFeed=False
	keepWeak=False
	doFight=False
	doHit=False
	healThreshold=1
	minDeposit=1000
	minBankBalance=0
	bountyLevel=0
	energyRechargeTime=300
	healthRechargeTime=180
	staminaRechargeTime=300
	isHealing=True
	useAjax=True

class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+game_config.urlPrefix+game_config.statsURL
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Health', game_config.healthPrefix, game_config.healthMidfix), ('Stamina', game_config.staminaPrefix, game_config.staminaMidfix), ('Cash', game_config.cashPrefix, None), ('Bank', game_config.bankPrefix, None), ('Member', game_config.memberPrefix, None), ('Level', game_config.levelPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
	def haveIFrame(self, page):
		links = FBAuto.common.IFrameProcessor.getLinks(page)
		for l in links:
			if re.compile(self.game_config.iframeURL).search(l)!=None:
				return True
		return False
	def fetchIFrame(self, url, page):
		links = FBAuto.common.IFrameProcessor.getLinks(page)
		for l in links:
			if re.compile(self.game_config.iframeURL).search(l)!=None:
				if self.ajax!=None:
					self.ajax.update(l)
				url=l
				return url,self.opener.open(l)
		return url,page
	def validPage(self, page):
		isValid=GeneralProfileLoader.validPage(self, page)
		if re.compile('Do you have any more gifts').search(page)!=None:
			isValid=True
		return isValid
	def handleRedirect(self, url, page):
		links=FBAuto.common.RedirectHandler.getRedirectLinks(page)
		while not self.validPage(page) and len(links)>0:
			page=self.opener.open(links[0])
			url=links[0]
			links=FBAuto.common.RedirectHandler.getRedirectLinks(page)
		return url,page
	def openTillEnd(self, url, txdata=None):
		realURL=url

		page=self.opener.open(url, txdata)
		realURL,page=self.handleRedirect(url, page)

		while not self.validPage(page) and self.game_config.iframeURL!='' and self.haveIFrame(page):
			realURL,page=self.fetchIFrame(realURL, page)
			realURL,page=self.handleRedirect(realURL, page)

			if not self.validPage(page) and self.game_config.iframeURL!='' and not self.haveIFrame(page):
				print ("Problem loading page, retrying...")
				#f=open('tmp.htm', 'w')
				#f.write(page)
				#f.close()
				page=self.opener.open(url, txdata)
				realURL,page=self.handleRedirect(url, page)

		return (realURL, page)

	def getUltimatePage(self, url, txdata=None):
		tuple=self.openTillEnd(url, txdata)
		return tuple[1]
	def loadPage(self, url):
		page=self.getUltimatePage(url)
		while not self.validPage(page):
			print (self.game_config.appID+'('+self.opener.thisUserID+')'+': Error loading the page! Redo!')
			self.opener.login()
			page=self.getUltimatePage(url)
		return page
	def deposit (self):
		print ('Depositing ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.bankURL
		html = self.opener.open(page)
		p = SimpleFormParser(self.game_config.bankURL, {}, {self.game_config.bankActionName:'Deposit'})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		if len(p.forms)>0:
			p.forms[0].submit(self.opener, page)
	def withdraw (self, amount):
		print ('Withdraw ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.bankURL
		html = self.opener.open(page)
		p = SimpleFormParser(self.game_config.bankURL, {'amount':str(amount)}, {self.game_config.bankActionName:'Withdraw'})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		if len(p.forms)>0:
			p.forms[0].submit(self.opener, page)
	def heal(self):
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.hospitalURL)
		self.loadProfile(html)
		self.displayProfile()
		p = SimpleFormParser(self.game_config.hospitalURL, {}, {self.game_config.hospitalActionName:self.game_config.hospitalAction})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		if len(p.forms)>0 and self.data["Health"]<=self.data["HealthMax"]*self.game_config.healThreshold:
			p.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.hospitalURL)

		self.loadProfile(html)
	def getEnemyLevel(self, id):
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.statsURL+'?user='+id)
		m=re.compile('fight\.php\?opponent_id='+id+'&amp;action=attack').search(page)
		if m==None:
			return self.data["Level"]+1

		m=re.compile(self.game_config.enemyLevelPrefix+'([0-9]+)').search(page)
		if m!=None:
			enemyLevel=int(m.group(1))
			return enemyLevel
		print ('Cannot get enemyLevel!')
		return self.data["Level"]+1
	def checkRefill2 (self, type):
		self.openAjax('stats.php?useBoostCons=249')
	def checkRefill (self, type):
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.jobURL)
		m=re.compile('jobs\.php\?judge_buff='+type).search(page)
		if m!=None:
			print ('Refill found', m.group())
			self.loadProfile(self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+m.group()))
		else:
			self.openAjax('stats.php?useBoostCons=249')
	#Ajax related
	ajax=None
	def ajaxReady(self):
		if not self.game_config.useAjax:
			return True
		elif self.ajax==None:
			return False
		else:
			return True
	def initAjaxProcessor(self, link):
		if self.ajax!=None:
			return
		if self.game_config.iframeURL=='':
			self.ajax=AjaxProcessor()
			self.ajax.var.update({'a[0-9]+_url': link})
			self.ajax.ajax_url_suffix=link
		else:
			self.ajax=ZyngaAjaxProcessor()
	def openForAjax(self, link, ajax_html=None):
		if ajax_html==None and self.game_config.useAjax:
			self.initAjaxProcessor(link)
			ajax_html=self.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+link)
		elif not self.game_config.useAjax:
			ajax_html=self.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+link)

		while self.game_config.useAjax and not self.ajax.process(ajax_html):
			self.initAjaxProcessor(link)
			ajax_html=self.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+link)

		if self.game_config.useAjax:
			header=self.ajax.getHeader()
			header['sf_xw_user_id']=self.opener.thisUserID
			header.update({'query[ajax]': '1'})
		return ajax_html
	def getAjaxLink(self, link, additional=None):
		if self.ajax!=None:
			if self.game_config.iframeURL!='':
				return [self.ajax.getURL(link), None]
			elif additional==None:
				return [Facebook.appsURL+'/fbml/fbjs_ajax_proxy.php', urllib.parse.urlencode(self.ajax.header)]
			else:
				self.ajax.header.update(additional)
				return [Facebook.appsURL+'/fbml/fbjs_ajax_proxy.php', urllib.parse.urlencode(self.ajax.header)]
		else:
			print ('Ajax not in use')
			if additional==None:
				return [Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+link, None]
			else:
				return [Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+link, urllib.parse.urlencode(additional)]
	def openAjax(self, link):
		refreshLink = self.getAjaxLink(link)
		t1=time.time()
		html=self.opener.open(refreshLink[0], refreshLink[1])
		t2=time.time()-t1
		print ('Success ('+str(t2)+')')
		return html
	def refreshAjax(self, link):
		refreshLink = self.getAjaxLink(link)
		t1=time.time()
		self.opener.refresh(refreshLink[0], refreshLink[1])
		t2=time.time()-t1
		print ('Success ('+str(t2)+')')
class FPLoader(ProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+self.game_config.urlPrefix+game_config.fpURL
		profile_config.meta=[('Favor', game_config.fpPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
class StatsLoader(ProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/'+self.game_config.urlPrefix+game_config.statsURL
		profile_config.meta=[('Attack', game_config.attackStatsPrefix, None), ('Defense', game_config.defenseStatsPrefix, None), ('Energy', game_config.energyStatsPrefix, None), ('Health', game_config.healthStatsPrefix, None), ('Stamina', game_config.rageStatsPrefix, None), ('Available', game_config.availPointsPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
		self.dotAll=True
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)

class FriendLoader:
	friendList=[]
	opener=None
	def __init__(self, opener):
		self.opener=opener
		w = WhiteListLoader(self.opener)
		self.friendList=self.friendList+w.whiteList
	def foundLink(self, link):
		friend=re.compile('[0-9]+').search(link)
		if friend!=None:
			self.friendList.append(friend.group())

class JobMain(JobRunner):
	opener=None
	profile=None
	counter=0
	jobList=[(13, 1, '9', None)]
	game_config=None
	keepRetry=True
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Earn'
		self.profile=ProfileLoader(f, self.game_config)
		self.energyRechargeTime=self.game_config.energyRechargeTime
		self.keepRetry=True
		self.counter=0
	def getJobBaseURL (self):
		m = re.compile('[a-z]+\.php').search(self.game_config.jobURL)
		if m!=None:
			return m.group()
		else:
			return self.game_config.jobURL
	def earn (self, jobID, html=None):
		print ('Earning ('+self.game_config.appID+'), job:',self.jobList[self.counter], 'Energy:', self.profile.data["Energy"])
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.jobURL
		if html==None or not self.profile.validPage(html):
			html = self.profile.loadPage(page)
		m = re.compile(self.getJobBaseURL()+'\?[^"\'\\\\]+job='+jobID+'[^"\'\\\\]*').search(html)
		if m!=None:
			print ('Quick job link found:', m.group())
			return self.profile.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+m.group().replace('&amp;', '&'))

		possible = set(re.compile(self.getJobBaseURL()+'\?[^"\'\\\\]+').findall(html.replace('&#039;', '"')))
		p = SimpleFormParser(self.getJobBaseURL()+'[^\'"]*', {}, {'job':jobID})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			if len(p.forms)==0:
				print ('HTML parser exception detected!')
				traceback.print_exc(file=sys.stdout)

		for j in possible:
			if len(p.forms)>0:
				break
			if self.profile.data["Energy"]<self.jobList[self.counter][0]:
				break
			print ('Form not found, trying:', j)
			html=self.profile.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+j.replace('&amp;', '&'))
			self.profile.loadProfile(html)
			try:
				p.feed(html)
				p.close()
			except HTMLParseError:
				if len(p.forms)==0:
					print ('HTML parser exception detected!')
					traceback.print_exc(file=sys.stdout)
		if len(p.forms)>0:
			if p.forms[0].action!=self.game_config.jobURL:
				page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+p.forms[0].action
			html=p.forms[0].submit(self.opener, page)
		else:
			print ('Form not found at all!')
			html = self.profile.loadPage(page)
		return html
	def calcJobID(self):
		self.readJobList()
		if len(self.jobList)==0:
			return
		page=self.profile.openForAjax(self.game_config.inventoryURL)
		while re.compile(self.game_config.energyPrefix).search(page)==None and self.keepRetry:
			print (self.game_config.appID+'('+self.opener.thisUserID+')'+': Error loading the job info! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			page=self.profile.openForAjax(self.game_config.inventoryURL)
		self.counter=0
		while self.jobList[self.counter][3]!=None and re.compile(self.jobList[self.counter][3]).search(page)!=None:
			print ('Job:', self.jobList[self.counter][3], 'finished')
			self.counter=(self.counter+1)%len(self.jobList)
	def readJobList(self):
		self.jobList=[]
		f = open(self.opener.getConfigFile(self.game_config.appID+'.txt'), 'r')
		iterator=re.compile('[a-z0-9A-Z]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				self.jobList.append((energy, count, id, prereq))
			else:
				self.jobList.append((energy, count, id, prereq, prereq_count))
			iterator=re.compile('[a-z0-9A-Z]+').findall(f.readline())
		print ('Job list:', self.jobList)
		f.close()

	def doJob(self):
		self.calcJobID()
		if len(self.jobList)==0:
			self.sleepTime=[30*60, 40*60]
			return
		self.profile.loadProfile()
		self.profile.displayProfile()

		energy=self.profile.data["Energy"]
		if energy>=self.jobList[self.counter][0]:
			for i in range(0, self.jobList[self.counter][1]):
				self.earn(self.jobList[self.counter][2])
			self.profile.loadProfile()
			#Job done (energy deducted!)
			if energy>self.profile.data["Energy"]:
				self.calcJobID()
				#Calculate the energy required for the next job
				self.updateSleepTime()
			#Job NOT done (probably because of errors)
			else:
				self.sleepTime=[0, self.game_config.energyRechargeTime/2]
		else:
			#Not enough energy for the current job
			self.updateSleepTime()

		self.profile.loadProfile()
		if self.profile.data["Cash"]>=self.game_config.minDeposit:
			self.profile.deposit()

	def updateSleepTime(self):
		sleepTime=(self.jobList[self.counter][0]-self.profile.data["Energy"])*self.game_config.energyRechargeTime
		if sleepTime>=self.game_config.energyRechargeTime/2:
			sleepTime=sleepTime-self.game_config.energyRechargeTime/2
		else:
			sleepTime=0

		self.sleepTime=[sleepTime, sleepTime+self.game_config.energyRechargeTime/2]

class FightMain(JobRunner):
	opener=None
	profile=None
	friends=None
	member=0
	healthThreshold=25
	healthRecoveryThreshold=50
	game_config=GameConfig()
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.jobDescription='Fight'
		self.friends=FriendLoader(self.opener)
		self.appID=self.game_config.appID
		self.profile=ProfileLoader(f, self.game_config)
		self.member=0
	def loadFriends(self):
		#Load friends
		self.friends=FriendLoader(self.opener)
		friend_links = LinkSearcher(self.opener, 'stats\.php\?user=[0-9]+')
		friend_links.callback=self.friends
		friend_links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'group.php'
		#friend_links.load('')
		print ('Friends ('+self.game_config.appID+'):', self.friends.friendList)
	def hit (self):
		toAttack=[]
		toAvoid=[]
		if self.game_config.bountyLevel==0:
			return
		print ('Checking hit list ('+self.game_config.appID+') ...')
		page = Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'hits.php'
		html=self.opener.open(page)
		while self.profile.data["Health"]>=self.healthThreshold and self.profile.data["Stamina"]>0:
			p = SimpleFormParser('hits.php', {}, {})
			try:
				p.feed(html)
				p.close()
			except HTMLParseError:
				pass

			hitCount=0
			for f in p.forms:
				if 'target_id' in f.header and self.friends.friendList.count(f.header['target_id'])==0 and f.header['target_id']!=self.opener.thisUserID:
					if toAvoid.count(f.header['target_id'])==0 and toAttack.count(f.header['target_id'])==0:
						level = self.profile.getEnemyLevel(f.header['target_id'])
						if self.game_config.bountyLevel==0 and level<self.profile.data["Level"]:
							toAttack.append(f.header['target_id'])
						elif self.game_config.bountyLevel>0 and level<self.game_config.bountyLevel:
							toAttack.append(f.header['target_id'])
						else:
							toAvoid.append(f.header['target_id'])

					if toAvoid.count(f.header['target_id'])>0:
						pass
					elif toAttack.count(f.header['target_id'])>0:
						print ('Ready to hit: '+f.header['target_id'])
						html = f.submit(self.opener, page)
						hitCount=hitCount+1
						self.profile.loadProfile(html)
						if self.profile.data["Health"]<self.healthThreshold and self.profile.data["Stamina"]==0:
							break
						
			print ('Hit list count:',hitCount, 'To attack:', toAttack, 'To avoid:', toAvoid)
			if len(p.forms)==0 or hitCount==0:
				print ('No bounty to collect!')
				self.profile.loadProfile(html)
				break
	def fight (self, target_stamina):
		html = self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php')
		p = SimpleFormParser('fight.php', {}, {'action':'attack'})
		try:
			p.feed(html)
			p.close()
		except HTMLParseError:
			pass
		for f in p.forms:
			if self.profile.data["Health"]<self.healthThreshold or self.profile.data["Stamina"]<=target_stamina:
				break
			enemyID = f.header['opponent_id']
			enemyLevel = self.profile.getEnemyLevel(enemyID)
			if enemyLevel<self.profile.data["Level"] and self.friends.friendList.count(enemyID)==0:
				print ('Ready to fight: '+enemyID+' as level: '+str(enemyLevel))
				result = f.submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php')
				self.profile.loadProfile(result)
			elif enemyLevel>=self.profile.data["Level"]:
				break
	def getHealthRechargeTime(self, targetAmount):
		healthRechargeTime=(targetAmount-self.profile.data["Health"])*self.game_config.healthRechargeTime
		if healthRechargeTime<0:
			healthRechargeTime=0
		return healthRechargeTime
	def getStaminaRechargeTime(self, targetAmount):
		staminaRechargeTime=(targetAmount-self.profile.data["Stamina"])*self.game_config.staminaRechargeTime
		if staminaRechargeTime<0:
			staminaRechargeTime=0
		return staminaRechargeTime
	def updateSleepTime(self):
		#healthRechargeTime=self.getHealthRechargeTime(self.healthRecoveryThreshold-20)

		#if staminaRechargeTime==0 and healthRechargeTime==0:
			#self.sleepTime=[5*60, 10*60]
		#elif staminaRechargeTime>healthRechargeTime:
			#self.sleepTime=[staminaRechargeTime, staminaRechargeTime+60]
		#else:
			#self.sleepTime=[healthRechargeTime, healthRechargeTime+60]

		staminaRechargeTime=self.getStaminaRechargeTime(self.profile.data["StaminaMax"]-1)
		if staminaRechargeTime>=self.game_config.staminaRechargeTime/2:
			staminaRechargeTime=staminaRechargeTime-self.game_config.staminaRechargeTime/2
		else:
			staminaRechargeTime=0

		if self.game_config.doFight:
			self.sleepTime=[staminaRechargeTime, staminaRechargeTime+self.game_config.staminaRechargeTime/2]
		else:
			self.sleepTime=[15*60, 15*60+60]
	def foundLink(self, link):
		enemyID=''
		enemyLevel=0
		if self.profile.data["Stamina"]<self.profile.data["StaminaMax"]/2:
			return
		m=re.compile('stats\.php\?user=([0-9]+)').search(link)
		if m!=None:
			enemyID=m.group(1)
		if enemyID!='':
			enemyLevel=self.profile.getEnemyLevel(enemyID)

		if enemyLevel<self.profile.data["Level"] and self.profile.data["Stamina"]>=self.profile.data["StaminaMax"]/2 and self.friends.friendList.count(enemyID)==0:
			print ('Fighting:',enemyID,'Level:',enemyLevel)
			html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?opponent_id='+enemyID+'&action=attack')
			self.profile.loadProfile(html)

			while re.compile(self.game_config.winMsg).search(html)!=None and self.profile.data["Stamina"]>=self.profile.data["StaminaMax"]/2:
				print ('Fighting:',enemyID,'Level:',enemyLevel)
				html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?opponent_id='+enemyID+'&action=attack')
				self.profile.loadProfile(html)

	def fightFromFeed(self):
		print ('Fighting from feed')
		links = LinkSearcher(self.opener, self.game_config.feedTag+'[ ]*'+self.game_config.feedPrefix+'<a href="[^"]+"')
		links.callback=self
		links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+self.game_config.newsURL
		links.load('')
	def doJob(self):
		self.profile.loadProfile()
		while self.member<self.profile.data["Member"]:
			print ('Friends changed! Reloading...')
			self.loadFriends()
			if len(self.friends.friendList)>0:
				self.member=self.profile.data["Member"]
			print ('Known members:', len(self.friends.friendList), 'Real members:', self.profile.data["Member"])

		self.profile.displayProfile()

		if self.game_config.doFight:
			if self.profile.data["Health"]<self.profile.data["HealthMax"]:
				self.profile.heal()
			if self.profile.data["Stamina"]>0:
				self.hit()
				self.fight(self.profile.data["StaminaMax"]/2)
			if self.profile.data["Health"]<self.profile.data["HealthMax"]:
				self.profile.heal()

		if self.game_config.fightFromFeed:
			self.fightFromFeed()

		if self.profile.data["Cash"]>=self.game_config.minDeposit:
			self.profile.deposit()

		self.updateSleepTime()
class TreasureMain(JobRunner):
	lastTime=0
	profile=None
	repeat=1
	game_config=GameConfig()
	depleteTime=60*15
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Treasure'
		self.profile=ProfileLoader(f, self.game_config)
		self.repeat=1
	def getWaitingTime(self):
		html = self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/')
		self.profile.loadProfile(html)
		html=self.profile.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'buffs.php')
		totalTime=0
		pattern='Spin again in: <[^>]*>([0-9]+) hour[s]? and ([0-9]+) minute[s]?'
		m=re.compile(pattern).search(html)
		if m!=None:
			print (m.group())
			totalTime=int(m.group(1))*3600+int(m.group(2))*60+60
		elif re.compile('Cost to spin').search(html)!=None:
			totalTime=0
		else:
			print ('Cannot determine the pattern, default to 4 hours!')
			totalTime=3600*4
		return totalTime
	def noCongestion(self):
		currentTime=time.time()
		if TreasureMain.lastTime>0 and currentTime-TreasureMain.lastTime<self.depleteTime:
			print ('Delay the spin to avoid congestion')
			return False
		else:
			TreasureMain.lastTime=currentTime
			return True

	def spin(self):
		spinURL=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'buffs.php?doBuff=1'
		allRefresher=[]
		for i in range(0, self.repeat-1):
			refresher=FBAuto.refresher.refresher.SingleRefresher(self.opener, [spinURL])
			refresher.checkLogin=False
			refresher.diff=1
			refresher.start()
			time.sleep(0.01)
			allRefresher.append(refresher)

		time.sleep(10)
		for refresher in allRefresher:
			refresher.stopJob()

		self.opener.refresh(spinURL)
	def updateSleepTime(self, t):
		if t==0:
			s_time=self.depleteTime
		else:
			s_time=t
		self.sleepTime=[s_time, s_time+60]
	def doJob(self):
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'buffs.php')
		if re.compile('routine').search(html)!=None:
			print ('App under maintenance!')
			self.sleepTime=[3600, 7200]
			return
		t=self.getWaitingTime()
		if (t==0 or t==3600*4) and self.noCongestion():
			self.spin()
		t=self.getWaitingTime()
		self.updateSleepTime(t)
class HitMain(FightMain):
	game_config=None
	targetID=''
	def doJob(self):
		url=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?opponent_id='+self.targetID+'&action=attack'
		html=self.opener.open(url)
		self.profile.loadProfile(html)
		self.profile.displayProfile()
		while self.profile.data["Stamina"]>0 and self.profile.data["Health"]>20 and re.compile(self.game_config.winMsg).search(html)!=None:
			html=self.opener.open(url)
			print ('Hitting:', self.targetID)
			self.profile.loadProfile(html)
		if self.profile.data["StaminaMax"]>1:
			self.sleepTime=[self.game_config.staminaRechargeTime*1, self.game_config.staminaRechargeTime*self.profile.data["StaminaMax"]]
		else:
			self.sleepTime=[self.game_config.staminaRechargeTime*1, self.game_config.staminaRechargeTime*2]
		self.sleepTime=[self.game_config.staminaRechargeTime*1, self.game_config.staminaRechargeTime*1+1]


class FavorAllocMain(JobRunner):
	stats=None
	game_config=GameConfig()
	target=[]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
	def allocate(self, attr):
		link=attr[2]
		allThreads=[]
		threadStarted=False
		while self.stats.data[attr[0]]>attr[1]:
			if self.stats.data[attr[0]]-attr[1]>1000 and not threadStarted:
				print ('Creating mutliple threads to allocate',link)
				for i in range(0, 3):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.stats.getAjaxLink(link)], 1)
					refresher.diff=1
					refresher.display_msg=False
					refresher.start()
					allThreads.append(refresher)
				threadStarted=True
			self.stats.loadProfile(self.stats.openForAjax(link))
		for t in allThreads:
			t.stopJob()
	def doJob(self):
		self.stats=FPLoader(self.opener, self.game_config)
		self.stats.loadProfile(self.stats.openForAjax(self.game_config.fpURL))
		self.stats.displayProfile()
		for i in self.target:
			self.allocate(i)
		self.sleepTime=[60, 120]
class StatsAllocMain(JobRunner):
	stats=None
	game_config=GameConfig()
	target=[]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='stats'
	def allocate(self, attr):
		link=attr[2]
		allThreads=[]
		threadStarted=False
		threshold=100
		while self.stats.data[attr[0]]<attr[1] and self.stats.data["Available"]>2:
			if attr[1]-self.stats.data[attr[0]]>threshold and self.stats.data["Available"]>threshold and not threadStarted:
				print ('Creating mutliple threads to allocate',link)
				for i in range(0, 20):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.stats.getAjaxLink(link)], 1)
					refresher.diff=1
					refresher.display_msg=False
					refresher.start()
					allThreads.append(refresher)
				threadStarted=True
			elif (attr[1]-self.stats.data[attr[0]]<threshold or self.stats.data["Available"]<threshold) and threadStarted:
				for t in allThreads:
					t.stopJob()
					allThreads=[]
				threadStarted=False
			self.stats.loadProfile(self.stats.openForAjax(link))
		for t in allThreads:
			t.stopJob()
			allThreads=[]
	def doJob(self):
		self.stats=StatsLoader(self.opener, self.game_config)
		self.stats.loadProfile(self.stats.openForAjax(self.game_config.statsURL))
		self.stats.displayProfile()
		for i in self.target:
			self.allocate(i)
		self.sleepTime=[60, 120]
class PropertyMain(JobRunner):
	property_file=''
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='property'
	def doJob(self):
		d = self.opener.getConfigFile(self.game_config.appID+'_properties.txt')
		p=PropertyProcessor(self.opener)

		p.itemPrefix=self.game_config.propertyPrefix
		p.action='properties.php'
		p.postURL=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'properties.php'
		p.propertyID_name='property_id'
		p.amount_name='amount'
		p.action_type='buy'
		p.action_type_name='action'

		finished=False
		while not finished:
			p.loadInfoFromFile(d)
			if not p.loadInfoFromWeb(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'properties.php'):
				finished=True
				break
			money=p.getNeededMoney()

			profile=ProfileLoader(self.opener, self.game_config)
			profile.loadProfile()
			if profile.data["Cash"]>=money:
				p.buy()
			elif profile.data["Cash"]+profile.data["Bank"]-self.game_config.minBankBalance>=money:
				profile.withdraw(money-profile.data["Cash"])
				p.buy()
			else:
				finished=True

		#self.sleepTime=[60*60, 120*60]
		self.sleepTime=[1, 2]
class QuickFight(FightMain):
	fightPage=None
	hospitalLink='popup_action=Heal'
	freshmeat='1124071506'
	#regular=['525767664', '558671116']
	regular=['100000693146665']
	#regular=['1717585087']
	levelLimit=210
	healthreshold=1000
	sleepDelay=256
	def calculateSleepTime(self):
		print (self.sleepDelay)
		self.sleepTime=[self.sleepDelay, self.sleepDelay*2]
		if self.sleepDelay<256:
			self.sleepDelay=self.sleepDelay*4
	def resetSleepTime(self):
		if self.sleepDelay>1:
			self.sleepDelay=1
	def fightRegular(self):
		threadStarted=False
		allThreads=[]
		while self.profile.data["Health"]>=self.healthreshold and self.profile.data["Stamina"]>0:
			self.fightPage = self.profile.openForAjax('fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')
			self.profile.loadProfile(self.fightPage)

			if self.profile.data["Health"]-self.healthreshold>100 and self.profile.data["Stamina"]>10 and not threadStarted:
				print ('Creating mutliple threads to do fight')
				self.resetSleepTime()
				for i in range(0, 5):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink('fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')], 1)
					refresher.display_msg=False
					refresher.diff=1
					refresher.start()
					allThreads.append(refresher)

				threadStarted=True
		for t in allThreads:
			t.stopJob()
		self.profile.displayProfile()
	def refreshFightPage(self):
		count=random.randint(2, 10)
		for i in range (0, count):
			self.opener.refresh(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')

		self.fightPage=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')
		self.profile.loadProfile(self.fightPage)

	def processFightForms(self, forms):
		for f in forms:
			self.fightPage = f.submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php')
			self.profile.loadProfile(self.fightPage)
			if self.profile.data["Health"]<self.healthreshold:
				break
	def heal(self):
		self.fightPage=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.urlPrefix+'fight.php?'+self.hospitalLink)
		self.profile.loadProfile(self.fightPage)
		self.profile.displayProfile()

	def doJob(self):
		self.fightPage=self.profile.openForAjax('fight.php')
		if re.compile('routine').search(self.fightPage)!=None:
			print ('App under maintenance!')
			self.sleepTime=[3600, 7200]
			return
		self.profile.loadProfile(self.fightPage)
		self.healthreshold=0.2*self.profile.data["HealthMax"]
		self.profile.displayProfile()
		
		threadStarted=False
		fightThreads=[]
		healThreads=[]
		while self.profile.data["Stamina"]>0:
			self.fightPage=self.profile.openForAjax('fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')
			#self.profile.refreshAjax('stats.php?remove=1&user='+self.regular[random.randint(0, len(self.regular)-1)])
			self.profile.loadProfile(self.fightPage)
			if self.profile.data["Stamina"]>50 and not threadStarted:
				print ('Creating mutliple threads to do fight')
				self.resetSleepTime()
				for i in range(0, 10):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink('fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')], 1)
					refresher.display_msg=False
					refresher.diff=1
					refresher.start()
					fightThreads.append(refresher)
				for i in range(0, 1):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink('fight.php?popup_action=Heal')], 1)
					refresher.display_msg=False
					refresher.diff=1
					refresher.start()
					healThreads.append(refresher)
				threadStarted=True
			elif threadStarted:
				for t in fightThreads:
					t.url=[self.profile.getAjaxLink('fight.php?opponent_id='+self.regular[random.randint(0, len(self.regular)-1)]+'&action=attack')]
				for t in healThreads:
					t.url=[self.profile.getAjaxLink('fight.php?popup_action=Heal')]
		for t in fightThreads:
			t.stopJob()
		for t in healThreads:
			t.stopJob()
		if self.profile.data["Stamina"]>0 or self.profile.data["EnergyMax"]==0:
			self.sleepTime=[5, 10]
		else:
			self.calculateSleepTime()

class HealMain(QuickFight):
	profile=None
	game_config=GameConfig()
	hospitalThreshold=20
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Heal'
		self.profile=ProfileLoader(f, self.game_config)
	def doJob(self):
		self.fightPage=self.profile.openForAjax(self.game_config.statsURL)
		if re.compile('routine').search(self.fightPage)!=None:
			print ('App under maintenance!')
			self.sleepTime=[3600, 7200]
			return
		self.profile.loadProfile(self.fightPage)
		self.profile.displayProfile()
		
		threadStarted=False
		allThreads=[]
		while self.profile.data["Stamina"]>0:
			self.fightPage=self.profile.openForAjax(self.game_config.statsURL+'?popup_action=Heal')
			self.profile.loadProfile(self.fightPage)
			if not threadStarted:
				print ('Creating mutliple threads to do heal')
				self.resetSleepTime()
				for i in range(0, 1):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink(self.game_config.statsURL+'?popup_action=Heal')], 1)
					refresher.display_msg=False
					refresher.diff=1
					refresher.start()
					allThreads.append(refresher)
				threadStarted=True
		for t in allThreads:
			t.stopJob()
		if self.profile.data["Stamina"]>0 or self.profile.data["EnergyMax"]==0:
			self.sleepTime=[5, 10]
		else:
			self.calculateSleepTime()
class QuickJob(JobMain, QuickFight):
	game_config=GameConfig()
	def __init__(self, f):
		JobMain.__init__(self, f)
		self.keepRetry=False
	def doJob(self):
		self.fightPage=self.profile.openForAjax(self.game_config.jobURL)
		if re.compile('routine').search(self.fightPage)!=None:
			print ('App under maintenance!')
			self.sleepTime=[3600, 7200]
			return
		self.calcJobID()
		self.profile.loadProfile(self.fightPage)
		html=self.fightPage
		threadStarted=False
		allThreads=[]
		#while self.profile.data["Energy"]>self.jobList[self.counter][0] and (self.jobTimer.getWorkTime()>0 or self.jobTimer.getRestTime()==0):
		while True:
			html=self.profile.openForAjax(self.game_config.jobURL)
			#html = self.earn(self.jobList[self.counter][2], html)
			self.profile.loadProfile(html)
			m = re.compile(self.getJobBaseURL()+'\?[^"\'\\\\]*action=doJob[^"\'\\\\]+job='+self.jobList[self.counter][2]+'[^"\'\\\\]*').search(html)
			if m!=None:
				self.profile.openForAjax(m.group().replace('&amp;', '&'))
			else:
				print ('No job link found')

			jobLinks=[]
			for j in self.jobList:
				m = re.compile(self.getJobBaseURL()+'\?[^"\'\\\\]*action=doJob[^"\'\\\\]+job='+j[2]+'[^"\'\\\\]*').search(html)
				if m!=None:
					jobLinks.append(self.profile.getAjaxLink(m.group().replace('&amp;', '&')))

			if self.profile.data["Energy"]/self.jobList[self.counter][0]>50 and m!=None and not threadStarted:
				print ('Creating mutliple threads to do job',m.group())
				self.resetSleepTime()
				for i in range(0, 20):
					#refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink(m.group().replace('&amp;', '&'))], 1)
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, jobLinks, 1)
					refresher.diff=1
					refresher.display_msg=False
					refresher.start()
					allThreads.append(refresher)

				refillCount=0
				for i in range(0, refillCount):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink('stats.php?useBoostCons=249')], 1)
					refresher.diff=1
					refresher.display_msg=False
					refresher.start()
					allThreads.append(refresher)

				threadStarted=True
			elif len(jobLinks)>0 and threadStarted:
				for t in allThreads:
					t.url=jobLinks
			elif m==None:
				print ('No quick link found!')
		for t in allThreads:
			t.stopJob()
		self.profile.displayProfile()

		if self.profile.data["Energy"]<self.jobList[self.counter][0] and self.profile.data["EnergyMax"]>0:
			self.profile.checkRefill('energy')

		if self.profile.data["Energy"]>=self.jobList[self.counter][0] or self.profile.data["EnergyMax"]==0:
			self.sleepTime=[5, 10]
		else:
			self.calculateSleepTime()
class BossFight(JobMain, QuickFight):
	game_config=GameConfig()
	def __init__(self, f):
		JobMain.__init__(self, f)
		self.keepRetry=False
	def doJob(self):
		allThread=[]
		threadStarted=False

		html=self.profile.openForAjax('fight.php?popup_action=Heal')
		self.profile.loadProfile(html)
		while self.profile.data["Energy"]>self.profile.data["EnergyMax"]/5:
			if not threadStarted:
				for i in range(0, 10):
					refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink('bossFight.php', {'query[action]':'fight', 'query[boss]':'1', 'query[ajax]':'1'})], 1)
					refresher.display_msg=False
					refresher.diff=1
					refresher.start()
					allThread.append(refresher)

			html=self.profile.openForAjax('fight.php?popup_action=Heal')
			self.profile.loadProfile(html)

		for t in allThreads:
			t.stopJob()
		self.sleepTime[600, 1200]

class CloneAccountManager (AccountManager):
	gift_name=''
	def readPassword(self):
		self.accounts=[]
		f = open(self.getConfigFile(self.game_config.appID+'_clone.txt'), 'r')
		self.gift_name=f.readline().strip('\n')
		pattern='(^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}) (.*)'
		m=re.compile(pattern).search(f.readline())
		while m!=None:
			self.accounts.append((m.group(1), m.group(2)))
			m=re.compile(pattern).search(f.readline())
		f.close()
class GiftParser (HTMLParser):
	progress=0
	gift_name='Summon Conqueror Worm'
	link=''
	available=0
	def __init__(self, gift_name):
		HTMLParser.__init__(self)
		self.gift_name=gift_name
		self.progress=0
		self.link=''
		self.available=0
	def handle_starttag(self, tag, attrs):
		if tag.lower()=='tr' and self.progress==1:
			self.progress=2
		elif tag.lower()=='td' and self.progress==2:
			self.progress=3
		elif tag.lower()=='a' and self.progress==3:
			for a in attrs:
				if a[0].lower()=='href':
					m = re.compile('gift_give\.php[^\'"]*').search(a[1])
					if m!=None:
						self.link=m.group().replace('&amp;', '&')
						self.progress=4
	def handle_endtag(self, tag):
		if tag.lower()=='tr' and self.progress==2:
			self.progress=1
			self.link=''
		elif tag.lower()=='td' and self.progress==3:
			self.progress=2
	def handle_data(self, data):
		if self.progress==0 and re.compile('Give a gift').search(data)!=None:
			#print ('Found Give a gift string')
			self.progress=1
		elif self.progress>1 and self.progress<5:
			#if re.compile(self.gift_name).search(data)!=None:
			if data.strip()==self.gift_name:
				print ('Gift confirmed:', self.link, )
				self.progress=6
			else:
				#print ('Link found ('+data+') but not our gift!')
				self.progress=3
		elif self.progress==6:
			m=re.compile('You own: ([0-9,]+)').search(data)
			if m!=None:
				self.available=int(m.group(1).replace(',',''))
				print ('Available amount:', self.available)
				self.progress=7
class GiftSender (JobRunner):
	game_config=GameConfig()
	gift_name=''
	available_amount=0
	profile=None
	gift_interval=1
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.available_amount=0
		self.gift_interval=1
		self.profile=ProfileLoader(f, self.game_config)
	def getGiftLink(self, target, gift_name):
		html = self.profile.openForAjax('gift.php?user_id='+target)
		parser = GiftParser(gift_name)
		parser.feed(html)
		parser.close()
		self.available_amount=parser.available
		return parser.link
	def getAvailableAmount(self, target, gift_name):
		self.getGiftLink(target, gift_name)
		return self.available_amount
	def sendSlow (self, target, gift_name, amount):
		self.gift_name=gift_name
		link = self.getGiftLink(target, gift_name)
		if link=='':
			return
		refresher=FBAuto.refresher.refresher.AccurateRefresher(self.opener)
		refresher.min_interval=1
		refresher.refresh(self.profile.getAjaxLink(link), amount)
	def send (self, target, gift_name, amount=0):
		self.gift_name=gift_name
		link = self.getGiftLink(target, gift_name)
		#print ('Link:', link)
		if link=='':
			return

		if amount==0:
			amount=self.available_amount
			print ('Sending all amount:', amount)

		for i in range (0, int(amount/10)+1):
			reloaders=[]
			for i in range(0, 10):
				refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink(link)], 1)
				#refresher=FBAuto.refresher.refresher.Refresher(self.opener, [Facebook.appsURL+'/'+self.game_config.appID+'/'+link], 1)
				refresher.display_msg=False
				refresher.checkLogin=False
				refresher.diff=10
				reloaders.append(refresher)
				refresher.start()

			for refresher in reloaders:
				refresher.stopJob()
			time.sleep(self.gift_interval)
		time.sleep(0.5)
	def dup_send (self, target, gift_name):
		self.gift_name=gift_name
		link = self.getGiftLink(target, gift_name)
		#print ('Link:', link)
		if link=='':
			return

		if self.available_amount==0:
			return

		reloaders=[]
		for i in range(0, 80):
			refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [self.profile.getAjaxLink(link)], 1)
			refresher.display_msg=False
			refresher.checkLogin=False
			refresher.diff=1
			reloaders.append(refresher)
			refresher.start()

		while self.available_amount>0:
			self.getAvailableAmount(target, gift_name)

		for refresher in reloaders:
			refresher.stopJob()
class GiftCloner2 (JobRunner):
	gift_name='Summon Conqueror Worm'
	game_config=GameConfig()
	senders=[]
	def __init__(self, senders, gift_name):
		JobRunner.__init__(self, senders[0])
		self.opener=senders[0]
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.gift_name=gift_name
		self.senders=senders
	def doJob(self):
		print ('Gift cloner 2')
		g1=GiftClonerThread(self.gift_name, self.senders[0], self.senders[1], self.senders[2])
		g1.game_config=self.game_config
		g1.start()
		g2=GiftClonerThread(self.gift_name, self.senders[1], self.senders[0], self.senders[2])
		g2.game_config=self.game_config
		g2.start()
class GiftClonerThread (JobRunner):
	gift_name='Summon Conqueror Worm'
	sender=None
	receiver=None
	store=None
	game_config=GameConfig()
	profile=None
	storage_threshold=300
	def __init__(self, gift_name, sender, receiver, store):
		JobRunner.__init__(self, sender)
		self.sender=sender
		self.receiver=receiver
		self.store=store
		self.gift_name=gift_name

		self.appID=self.game_config.appID
		self.jobDescription='Inventory'

	def doJob(self):
		print ('Sending '+self.gift_name+' from:', self.sender.opener.email, 'to:', self.receiver.opener.email, 'Storing at:', self.store.opener.email)
		g = GiftSender(self.sender)
		g.profile=ProfileLoader(self.sender, self.game_config)
		g.game_config=self.game_config

		link=''
		while link=='':
			link = g.getGiftLink(self.receiver.opener.thisUserID, self.gift_name)

		reloaders=[]
		for i in range(0, 50):
			refresher=FBAuto.refresher.refresher.RefresherWithParam(self.opener, [g.profile.getAjaxLink(link)], 1)
			refresher.display_msg=False
			refresher.checkLogin=False
			refresher.diff=1
			reloaders.append(refresher)
			refresher.start()

		while True:
			link=g.getGiftLink(self.receiver.opener.thisUserID, self.gift_name)
			if link!='':
				for t in reloaders:
					t.url=[g.profile.getAjaxLink(link)]
			avail_amount = g.getAvailableAmount(self.receiver.opener.thisUserID, self.gift_name)
			print ('Updating URL for sender:', self.sender.opener.email, 'Available amount:', avail_amount)
class GiftCloner (JobRunner):
	gift_name='Summon Conqueror Worm'
	game_config=GameConfig()
	accounts=[]
	gift_interval=0.1
	rest_interval=10
	profile=None
	storage_threshold=20
	def __init__(self, accounts, gift_name):
		JobRunner.__init__(self, accounts[0])
		self.opener=accounts[0]
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.gift_name=gift_name
		self.accounts=accounts
	def clone(self):
		store=self.accounts[len(self.accounts)-1]
		idx=0
		while True:
			print ('idx:', idx, 'idx+1:',(idx+1)%(len(self.accounts)-1))
			sender=self.accounts[idx]
			self.opener=sender
			receiver=self.accounts[(idx+1)%(len(self.accounts)-1)]
			self.doSend(sender, receiver, store)
			idx=(idx+1)%(len(self.accounts)-1)
			time.sleep(self.rest_interval)

	def doJob(self):
		self.clone()
	def doSend (self, g_sender, g_receiver, g_store):
		print ('Sending '+self.gift_name+' from:', g_sender.opener.email, 'to:', g_receiver.opener.email, 'Storing (up to', self.storage_threshold, ') at:', g_store.opener.email)

		g = GiftSender(g_sender)
		g.profile=ProfileLoader(g_sender, self.game_config)
		g.gift_interval=self.gift_interval
		g.game_config=self.game_config
		avail_amount=g.getAvailableAmount(g_receiver.opener.thisUserID, self.gift_name)

		if avail_amount>self.storage_threshold:
			l = g.getGiftLink(g_store.opener.thisUserID, self.gift_name)
			concurrent_refresher=FBAuto.refresher.refresher.AccurateRefresher(g_sender)
			concurrent_refresher.refresh(g.profile.getAjaxLink(l), avail_amount-self.storage_threshold)
		elif avail_amount==0:
			s = GiftSender(g_store)
			s.profile=ProfileLoader(g_store, self.game_config)
			s.gift_interval=self.gift_interval
			s.game_config=self.game_config
			storage_amount=s.getAvailableAmount(g_sender.opener.thisUserID, self.gift_name)
			if storage_amount>self.storage_threshold:
				print ('Gift amount reached to 0, loading from storage...')
				s.send(g_sender.opener.thisUserID, self.gift_name, self.storage_threshold)
			elif storage_amount>0:
				print ('Gift amount reached to 0, loading from storage...')
				s.send(g_sender.opener.thisUserID, self.gift_name, storage_amount)

		g.dup_send(g_receiver.opener.thisUserID, self.gift_name)

import pythoncom
import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import time
#################
from FBAuto.common.SimpleFileServerCommon import *
from FBAuto.common.SimpleFileServer import *
#################

class AppServerSvc (win32serviceutil.ServiceFramework):
	_svc_name_ = "FBAuto File Server"
	_svc_display_name_ = "FBAuto File Server"

	server=None
	isAlive=False

	def __init__(self,args):
		win32serviceutil.ServiceFramework.__init__(self,args)
		self.hWaitStop = win32event.CreateEvent(None,0,0,None)
		socket.setdefaulttimeout(60)
		#################
		self.isAlive=False
		self.server = ServerBase(ProtocolProcessor.HOST, ProtocolProcessor.PORT)
		self.server.handlerClass=SimpleFileHandler
		self.server.serverStopped=False
		FileProcessor.fileDir='C:/Program Files/Programming/Python31/Lib/site-packages/facebook/FBAuto/config'
		#################

	def SvcStop(self):
		self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
		win32event.SetEvent(self.hWaitStop)

		#################
		self.isAlive=False
		#################

	def SvcDoRun(self):
		servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, servicemanager.PYS_SERVICE_STARTED, (self._svc_name_,''))

		#################
		self.isAlive=True
		self.server.listen()
		while self.isAlive:
			try:
				self.server.accept()
			except:
				pass
		#################

if __name__ == '__main__':
	win32serviceutil.HandleCommandLine(AppServerSvc)

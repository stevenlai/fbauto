import os.path
import sys
import traceback
import urllib.parse
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.treasurehunt.ProfileSearcher import *

class AjaxProcessor:
	ajax_url=''
	fb_mockajax_context_hash=''
	fb_mockajax_context=''
	post_form_id=''
	appNum=''
	ajax_url_suffix=''
	nectar_impid=''
	post_url=Facebook.appsURL+'/fbml/fbjs_ajax_proxy.php'
	header=None
	var={}

	def __init__(self):
		self.appNum=''
		self.ajax_url=''
		self.fb_mockajax_context_hash=''
		self.fb_mockajax_context=''
		self.post_form_id=''
		self.ajax_url_suffix=''
		self.nectar_impid=''
		self.post_url=Facebook.appsURL+'/fbml/fbjs_ajax_proxy.php'
		self.header=None
		self.var={'a[0-9]+_d\.getTime\(\)':str(int(time.time()))}
	
	def filter(self, page):
		for k,v in self.var.items():
			m=re.compile(k+'[ \t]*=[ \t]*[\'"]([^\'"]*)[\'"]').search(page)
			if m!=None and v==None:
				v=m.group(1)
				self.var.update({k:v})

		for k,v in self.var.items():
			iter=re.compile(k).finditer(page)
			for i in iter:
				page=page.replace(i.group(), '"'+v+'"')
				#print ('Replacing:',i.group(),'with','"'+v+'"')

		iter=re.compile('([\'"][ \t]*\+[ \t]*[\'"])').finditer(page)
		for i in iter:
			page=page.replace(i.group(), '')
			#print ('Replacing:',i.group(),'with','')

		return page
	def getPostFormID(self, page):
		m=re.compile('post_form_id=([0-9a-zA-Z]+)').search(page)
		if m!=None:
			return m.group(1)
		
		m=re.compile('post_form_id:"([0-9a-zA-Z]+)"').search(page)
		if m!=None:
			return m.group(1)
	def process(self, page):
		result=True
		page=self.filter(self.decode(page))
		m=re.compile('a([0-9]+)_ajax\.post\([\'"]([^\'"]+'+self.ajax_url_suffix.replace('.','\.').replace('?','\?')+')[\'"]').search(page)
		if m!=None:
			self.appNum=m.group(1)
			self.ajax_url=m.group(2)
		else:
			result=False
			print (re.compile('a[0-9]+_ajax\.post\(.*').findall(page))

		m=re.compile('app_'+self.appNum+'\.context[ ]*=[ ]*"([^"]+)"').search(page)
		if m!=None:
			self.fb_mockajax_context_hash=m.group(1)

		m=re.compile('app_'+self.appNum+'\.contextd = "(.*)";app_'+self.appNum+'\.data').search(page)
		if m!=None:
			self.fb_mockajax_context=self.decode(m.group(1))

		if self.fb_mockajax_context_hash=='' or self.fb_mockajax_context=='':
			m=re.compile('FBML\.Contexts\["([^"]+)"\][ ]*=[ ]*"(.*)"').search(page)
			if m!=None:
				self.fb_mockajax_context_hash=m.group(1)
				self.fb_mockajax_context=self.decode(m.group(2))

		self.post_form_id=self.getPostFormID(page)

		m=re.compile('Env\["nectar_last_impression_id"\]="([0-9a-zA-Z]+)"').search(page)
		if m!=None:
			self.nectar_impid=m.group(1)
		return result
	def getHeader(self):
		self.header={'url':self.ajax_url, 'type':'0', 'require_login':'false', 'fb_mockajax_context':self.fb_mockajax_context, 'fb_mockajax_context_hash':self.fb_mockajax_context_hash,'appid': self.appNum, 'post_form_id':self.post_form_id, 'post_form_id_source':'AsyncRequest', '__a':'1', 'nectar_impid':self.nectar_impid, 'nctrct':str(int(time.time()))}
		return self.header
	def decode(self, page):
		return page.replace('\\"','"').replace('\\/','/').replace('\\n','\n').replace('\\t','\t').replace('\\\\', '\\')
	def post(self, opener):
		start=time.time()
		#print (self.post_url, self.header)
		result=opener.open(self.post_url, urllib.parse.urlencode(self.header))
		total_time=time.time()-start
		if self.validate(result):
			print ('Success ('+str(total_time)+')')
		return self.decode(result)

	def refresh(self, opener):
		start=time.time()
		#print (self.post_url, self.header)
		result=opener.refresh(self.post_url, urllib.parse.urlencode(self.header))
		total_time=time.time()-start
		print ('Success ('+str(total_time)+')')

	def validate(self, result):
		if re.compile('No error').search(result)!=None or re.compile('"error":[ ]*0').search(result)!=None or result.strip()=="":
			return True
		else:
			print ('Failed!', self.header)
			#print ('Result page:', result)
			#return False
class ZyngaAjaxProcessor(AjaxProcessor):
	iframeURL=''
	def __init__(self):
		AjaxProcessor.__init__(self)
		self.var={}
		self.header={'ajax':'1', 'xw_client_id':'8'}
		self.iframeURL=''
	def update(self, ajaxURL):
		m = re.compile('http://[^/]+').search(ajaxURL)
		if m!=None:
			self.iframeURL=m.group()

		iter = re.compile('([^&?=]+)=([^&?=]+)').finditer(ajaxURL)
		for m in iter:
			key=m.group(1)
			value=m.group(2)
			if key=='cb' or re.compile('^sf_xw_').search(key)!=None or re.compile('^fb_sig').search(key)!=None:
				self.header[key]=value
		#print ("Ajax updated:", self.iframeURL, self.header)
	def process(self, page):
		result=True
		#page=self.filter(self.decode(page))
		m=re.compile("local_xw_sig[ ]*=[ ]*'([^']+)'").search(page)
		if m!=None:
			pass
			#self.header['sf_xw_sig']=m.group(1)
		else:
			#print ('No Ajax info found')
			result=False
		return result
	def getHeader(self):
		return self.header
	def getURL(self, link):
		fullURL=self.iframeURL+'/'
		m = re.compile('[^?]+').search(link)
		if m!=None:
			fullURL=fullURL+m.group()
			#fullURL=fullURL+link

		if re.compile('[?]').search(link)==None:
			fullURL=fullURL+'?cb='+self.header['cb']
		else:
			fullURL=fullURL+'?cb='+self.header['cb']

		#fullURL=fullURL+'&'+urllib.parse.urlencode({'next': link})
		fullURL=fullURL+'&'+'next='+link.replace('?', '&')

		for k, v in self.header.items():
			if re.compile('^sf_xw_').search(k)!=None:
				fullURL=fullURL+'&'+k+'='+v

		for k, v in self.header.items():
			if re.compile('^fb_sig').search(k)!=None:
				fullURL=fullURL+'&'+k+'='+v

		#print ('Ajax URL:', fullURL)
		return fullURL
if __name__ == "__main__":
	#f = Facebook('facebook')
	ajax=AjaxProcessor()
	#ajax.validate('abc')
	AjaxProcessor.validate('abc')

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
import socket
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *

class SearchParser(HTMLParser):
	nameList=[]
	idList=[]
	detector=0
	def __init__(self):
		HTMLParser.__init__(self)
		self.nameList=[]
		self.idList=[]
		self.detector=0
	def handle_starttag(self, tag, attrs):
		if tag=='div':
			for a in attrs:
				if a[0]=='id' and a[1]=='search_results':
					self.detector=1
		elif tag=='a' and self.detector==1:
			for a in attrs:
				if a[0]=='href' and re.compile('profile\.php\?id=[0-9]+').search(a[1])!=None:
					self.detector=2
					id=re.compile('[0-9]+').search(re.compile('profile\.php\?id=[0-9]+').search(a[1]).group()).group()
					if self.idList.count(id)==0:
						self.idList.append(id)
	def handle_data (self, data):
		if self.detector==2 and self.nameList.count(data)==0:
			self.nameList.append(data)
			self.detector=1
	def handle_endtag(self, tag):
		if tag=='a' and self.detector==2:
			self.detector=1
class FriendFilter2:
	appID=''
	opener=None
	allID=[]
	def __init__ (self, opener, appID):
		self.opener=opener
		self.appID=appID
		self.allID=[]
	def loadID(self):
		html=self.opener.open(Facebook.facebookURL+'/home.php?app_id='+self.appID)
		m=re.compile('HomeFeed\.getInstance\(\)\.loadMore\("[0-9,]+"').search(html)
		if m!=None:
			storyKey=re.compile('[0-9,]+').search(m.group()).group()
			input=urllib.parse.urlencode({'story_keys':storyKey,'apps[0]':self.appID, '_log_dst_tab_name':'','ajax_log':'1','_ecdc':'false'})
			html=self.opener.open(Facebook.facebookURL+'/ajax/feed.php?'+input)
			self.grabID(html)
			oldestTime=self.grabTime(html)

			i=0
			while oldestTime!='' and i<500:
				input=urllib.parse.urlencode({'oldest':oldestTime,'apps[0]':self.appID, '_log_dst_tab_name':'','ajax_log':'1','_ecdc':'false'})
				#print ('Input:',input)
				html=self.opener.open(Facebook.facebookURL+'/ajax/feed.php?'+input)
				self.grabID(html)
				oldestTime=self.grabTime(html)
				i=i+1

			print ('All ID:',self.allID)

	def grabTime(self, html):
		foundTime=''
		m=re.compile('"oldestStoryTime":[ \t]*[0-9]+').search(html)
		if m!=None:
			foundTime=re.compile('[0-9]+').search(m.group()).group()
		return foundTime
	def grabID(self, html):
		tmpList=[]
		idLinks = re.compile('profile\.php\?id=[0-9]+').findall(html)
		for link in idLinks:
			id=re.compile('[0-9]+').search(link).group()
			if tmpList.count(id)<1:
				tmpList.append(id)
		for id in tmpList:
			if self.allID.count(id)<1:
				self.allID.append(id)

class FriendFilter:
	appID=''
	searchKey='100004000'
	inviteURL=''
	opener=None
	allNames=[]
	allID=[]
	keepRetry=False
	def __init__ (self, opener, appID):
		self.opener=opener
		self.appID=appID
		self.allNames=[]
		self.allID=[]
		self.keepRetry=False
	def search(self):
		self.loadNames()
		print (self.allNames)
		print (self.allID)
		print ('Total:',len(self.allNames))
		invitePage=self.oepner.loadPage(self.inviteURL)
		print ('Invitation to send: '+self.inviteURL)
		for n in self.allNames:
			if re.compile(n).search(invitePage)!=None:
				print (n)
	def loadNames(self):
		idx=10
		#page=Facebook.facebookURL+'/s.php?k='+self.searchKey+'&app_id='+self.appID
		page=Facebook.facebookURL+'/social_graph.php?node_id='+self.appID+'&class=AppUserManager&edge_type=mutual'
		html=urldecode(self.opener.loadPage(page))
		#m=re.compile('k='+self.searchKey+'&app_id='+self.appID+'&sid=[a-zA-Z0-9]+').search(html)
		#print ('Searching in: '+page)
		#sp,html = self.__parseFile(Facebook.facebookURL+'/'+page)
		idList,html = self.getID(page)
		#self.allNames=self.allNames+sp.nameList
		self.allID=self.allID+idList
		m=re.compile('social_graph\.php\?node_id='+self.appID+'&class=AppUserManager&edge_type=mutual&start='+str(idx)).search(html)
		while m!=None:
			decoded_page=m.group().replace('&amp;','&')
			#print ('Searching in: '+decoded_page)
			idList,html = self.getID(Facebook.facebookURL+'/'+decoded_page)
			#self.allNames=self.allNames+sp.nameList
			self.allID=self.allID+idList
			idx=idx+10
			m=re.compile('social_graph\.php\?node_id='+self.appID+'&class=AppUserManager&edge_type=mutual&start='+str(idx)).search(html)
	def getID(self, url):
		html=self.opener.loadPage(url)
		m = re.compile('\?compose&amp;id=([0-9]+)').finditer(html)
		idList=[]
		for i in m:
			idList.append(i.group(1))
		return idList, html
	def __parseFile(self, url):
		finished=False
		sp=SearchParser()
		while not finished:
			try:
				html=urldecode(self.opener.loadPage(url))
				sp=SearchParser()
				sp.feed(html)
				sp.close()
				finished=True
			except:
				#if len(sp.nameList)==0:
					#print ('Error occurred')
					#traceback.print_exc(file=sys.stdout)
				if len(sp.nameList)>0:
					finished=True
				elif not self.keepRetry:
					print ('Error occurred not retrying')
					traceback.print_exc(file=sys.stdout)
					finished=True
				else:
					print ('Error occurred and retrying')
					traceback.print_exc(file=sys.stdout)
					finished=False
		return sp,html
def findCheat(f):
	f.login()
	appID='dragonwars'
	i=812
	html=f.open(Facebook.appsURL+'/'+appID+'/jobs.php?action=Do+Job&job='+str(i))
	while not f.validLogin(html):
		try:
			f.login()
		except:
			pass
		html=f.open(Facebook.appsURL+'/'+appID+'/jobs.php?action=Do+Job&job='+str(i))
	while re.compile('appear to be valid').search(html)!=None and i<3000:
		i=i+1
		print ('Searching in:', i)
		html=f.open(Facebook.appsURL+'/'+appID+'/jobs.php?action=Do+Job&job='+str(i))
		while not f.validLogin(html):
			try:
				f.login()
			except:
				pass
			html=f.open(Facebook.appsURL+'/'+appID+'/jobs.php?action=Do+Job&job='+str(i))
	print ('item found at page:', i, 'for app: '+appID)
	f.logout()
if __name__ == "__main__":
	f = Facebook('facebook')
	#findCheat(f)

	filter=FriendFilter(f, '49563868025')
	filter.inviteURL=Facebook.appsURL+'/nitrousracing/index.php?c=recruit&viewall=1'
	filter.loadNames()
	header=[]
	for id in filter.allID:
		header.append(('ids[]', id))
	f.open(Facebook.appsURL+'/nitrousracing/index.php?c=recruit&sent=true&creative=3', urllib.parse.urlencode(header))
	#filter=FriendFilter(f, '20121071810')
	#filter.inviteURL=Facebook.appsURL+'/crusades/invite'
	#filter=FriendFilter(f, '28083325917')
	#filter.inviteURL=Facebook.appsURL+'/bloodlust/party.php?all=1'
	#filter=FriendFilter(f, '21371199019')
	#filter.inviteURL=Facebook.appsURL+'/elvenblood/party.php?all=1'
	#filter=FriendFilter(f, '47071179128')
	#filter.inviteURL=Facebook.appsURL+'/greatwarlords/recruit'
	#filter=FriendFilter(f, '28645543422')
	#filter.inviteURL=Facebook.appsURL+'/playsexgames/family.php?op=invite_friends'
	#filter=FriendFilter(f, '28978999743')
	#filter.inviteURL=Facebook.appsURL+'/knighted/family.php?op=invite_friends'
	#filter=FriendFilter(f, '16510419418')
	#filter.inviteURL=Facebook.appsURL+'/gangsterbattle/family.php?op=invite_friends'
	#filter=FriendFilter(f, '39114431139')
	#filter.inviteURL=Facebook.appsURL+'/darkages/mob.php'
	#filter=FriendFilter(f, '25609511981')
	#filter.inviteURL=Facebook.appsURL+'/saveourearth/invite'
	#filter=FriendFilter(f, '17050249638')
	#filter.inviteURL=Facebook.appsURL+'/realtimeracing/invite'
	#filter=FriendFilter(f, '44080588480')
	#filter.inviteURL=Facebook.appsURL+'/truegangsters/index.php?c=recruit'
	#filter=FriendFilter(f, '44080588480')
	#filter.inviteURL=Facebook.appsURL+'/truegangsters/index.php?c=recruit'
	#filter=FriendFilter(f, '13524764269')
	#filter.inviteURL=Facebook.appsURL+'/playgangwars/invite/send_invitation'
	#filter=FriendFilter(f, '16421175101')
	#filter.inviteURL=Facebook.appsURL+'/piratesrule/recruit.php'
	#filter=FriendFilter(f, '17217211796')
	#filter.inviteURL=Facebook.appsURL+'/dragonwars/recruit.php'
	#filter=FriendFilter(f, '25287267406')
	#filter.inviteURL=Facebook.appsURL+'/vampiresgame/recruit.php'
	#filter=FriendFilter(f, '10979261223')
	#filter.inviteURL=Facebook.appsURL+'/inthemafia/recruit.php'
	#filter=FriendFilter(f, '36842288331')
	#filter.inviteURL=Facebook.appsURL+'/spacewarsgame/recruit.php'
	#filter=FriendFilter(f, '25025900670')
	#filter.inviteURL=Facebook.appsURL+'/wayoftheninja/dojo.php'
	#filter=FriendFilter(f, '6058549658')
	#filter.inviteURL=Facebook.appsURL+'/triumph/index.php?c=advisorinvite'
	#filter=FriendFilter(f, '17326627347')
	#filter.inviteURL=Facebook.appsURL+'/street-wars/invite.php'

	#filter.loadNames()

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *
from FBAuto.treasurehunt.ProfileSearcher import *

class GameConfig:
	appID=''
	appNum=''
	entertainStr=''
	happinessPrefix='<span [^>]+happiness[^>]+>'
	happinessThreshold=90
class JobMain(JobRunner):
	scheduled_sleepTime=[5*60, 20*60]
	scheduled_url='requests.php'
	sub_url='connections\.php\?show=members&amp;offset=[0-9]+'
	game_config=GameConfig()
	recursive=False
	alwaysGiveYoung=False
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription=self.game_config.appID
		self.alwaysGiveYoung=False
		self.recursive=False
	def getHappiness(self, page):
		happiness=0
		m=re.compile(self.game_config.happinessPrefix+'[0-9]+').search(page)
		if m!=None:
			allStr=m.group()
			prefixStr=re.compile(self.game_config.happinessPrefix).search(allStr).group()
			happinessStr=allStr.replace(prefixStr,'')
			happiness = int(happinessStr)
		return happiness

	def foundLink(self, link):
		ownerID=re.compile('[0-9]+').search(link).group()

		a = AjaxProcessor()
		page=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'players.php?id='+ownerID)
		a.process(page)

		action='entertain'

		doid=None
		m=re.compile('value="([^"]+)"[ ]*name="doid"').search(page)
		if m!=None:
			doid=m.group(1)

		tnow=None
		m=re.compile('value="([^"]+)"[ ]*name="tnow"').search(page)
		if m!=None:
			tnow=m.group(1)

		if re.compile('does not want').search(page)!=None or (self.getHappiness(page)<self.game_config.happinessThreshold and not self.alwaysGiveYoung):
			action='entertain'
		else:
			action='giveYoung'

		print ('Helping ('+self.game_config.appID+'): '+ownerID+' Happiness:',self.getHappiness(page),'Action: '+action)

		if re.compile('<img src="[^<>]*happiness.gif"[^<>]*>[^<>]*<span[^<>]*>'+self.game_config.entertainStr+'</span>[^<>]*<img src="[^<>]*happiness.gif"[^<>]*>').search(page)==None:
			print ('Cannot do anything')
			return

		info = a.getHeader()
		info["query[action]"]=action
		info["query[id]"]=ownerID
		if doid!=None:
			info["query[do]"]=doid
		if tnow!=None:
			info["query[tnow]"]=tnow
		a.post(self.opener)
	def search(self, url):
		print ('Searching in:', url)
		links = set(re.compile('players\.php\?id=[0-9]+').findall(self.opener.open(url)))
		for l in links:
			self.foundLink(l)
	def doJob(self):
		if not self.recursive:
			self.search(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.scheduled_url)
		else:
			sublinks=set(re.compile(self.sub_url).findall(self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+self.scheduled_url)))
			print (sublinks)
			for l in sublinks:
				self.search(Facebook.appsURL+'/'+self.game_config.appID+'/'+l.replace('&amp;', '&'))

		self.sleepTime=self.scheduled_sleepTime


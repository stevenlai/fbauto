import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.ProfileLoader import *
from FBAuto.common.FormParser import *
from FBAuto.common.JobRunner import *
from FBAuto.common.AjaxProcessor import *

class GameConfig:
	appID=''
	gangURL='viewgang'
	individualGang='upgrademember&amp;who=[0-9]+'
	energyPrefix=''
	energyMidfix='/'
	cashPrefix='Money:<[^<>]*><[^<>]*>\$'
	ownedSuffix='<[^<>]*><[^<>]*>Owned'
	pricePrefix='Price: <[^<>]*>\$'
	buying_action1='upgrademember&amp;who='
	buying_action2='&amp;buy=1&amp;weapon='
	amountName='amount'
	friendURL='viewgang'
	lootStr='Take It!'
	energyRechargeTime=300
	ajax={}
	inventoryList=["", "Bar of Soap", "Death Spoon", "Adult Magazines", "Book", "Attack Comb", "Sharpened Ruler ", "Extra Layers"]
	targetList=[0, 28, 25, 9, 0, 0, 0, 0]
class InventoryParser(HTMLParser):
	begin=False
	header=[]
	action=''
	amount=''
	input=None
	game_config=None

	def __init__(self, game_config, action, amount):
		HTMLParser.__init__(self)
		self.game_config=game_config
		self.header=[]
		self.begin=False
		self.action=action
		self.amount=str(amount)
		self.input=None
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			for a in attrs:
				if a[0]=='action' and a[1]==self.action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]

			if name==self.game_config.amountName:
				value=self.amount

			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			self.input = urllib.parse.urlencode(self.header)
			self.begin=False
			self.header=[]
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	util=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		self.util=Util(f, game_config)
		profile_config = ProfileConfig()
		profile_config.appID=game_config.appID
		profile_config.profileURL=Facebook.appsURL+'/'+game_config.appID+'/index.php?c='+self.game_config.gangURL
		profile_config.meta=[('Energy', game_config.energyPrefix, game_config.energyMidfix), ('Cash', game_config.cashPrefix, None), ('Member', game_config.memberPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)

class Util:
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
	def refreshLink(self, link, html=None):
		self.opener.refresh(Facebook.appsURL+'/'+self.game_config.appID +'/index.php?c='+link)
	def openLink(self, link, html=None):
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID +'/index.php?c='+link)
		while re.compile('The page you are looking for was not found').search(html)!=None:
			print ('Page error ('+self.game_config.appID+'): '+link)
			html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID +'/index.php?c='+link)
		return self.filterPage(html)
	def openLinkAjax(self, link, html=None):
		ajax=self.getAjax(link, html)
		return self.filterPage(ajax.post(self.opener))
	def getAjax(self, link, html=None):
		if html==None:
			html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/')
		linkHeader=Facebook.appsURL+'/'+self.game_config.appID+'/'
		
		ajax = AjaxProcessor()
		ajax.var.update({'a[0-9]+_codeBase': None, 'a[0-9]+_c':link})
		ajax.ajax_url_suffix=link
		ajax.process(html)
		header = ajax.getHeader()
		header.update(self.game_config.ajax)
		return ajax
	def grabLoot(self, html):
		if re.compile(self.game_config.lootStr).search(html)!=None:
			print ('Picking up loot ('+self.game_config.appID+')...')
			self.openLinkAjax('grabloot', html)
	def filterPage(self, html):
		return html.replace('&#039;',"'")

class InventoryLoader:
	inventoryList=[]
	targetList=[]
	opener=None
	money=0
	pendingToBuy=False
	game_config=None
	profile=None
	def __init__(self, f, game_config, money):
		self.opener=f
		self.game_config=game_config
		self.profile=ProfileLoader(f, game_config)
		self.money=money
		self.pendingToBuy=False
		self.inventoryList=self.game_config.inventoryList
		self.targetList=self.game_config.targetList
	def foundLink(self, link):
		if self.pendingToBuy:
			return
		decoded_link = link.replace('&amp;', '&')
		who=re.compile('who=([0-9]+)').search(decoded_link).group(1)
		#print ('Found member, who: '+who)

		page=self.profile.util.openLink(decoded_link)
		self.profile.loadProfile(page)
		while self.profile.data['EnergyMax']==0:
			print ('Problem loading the inventory... Retrying')
			self.opener.login()
			page=self.profile.util.openLink(decoded_link)
			self.profile.loadProfile(page)

		iterator=re.compile('[0-9,]+'+self.game_config.ownedSuffix).finditer(page)
		inventoryQuantity=[0]
		for match in iterator:
			allStr=match.group()
			suffixStr=re.compile(self.game_config.ownedSuffix).search(allStr).group()
			numStr=allStr.replace(suffixStr,'').replace(',','')
			q=int(numStr)

			inventoryQuantity.append(q)
		#print (inventoryQuantity)

		iterator=re.compile(self.game_config.pricePrefix+"[0-9,]+").finditer(page)
		inventoryPrice=[0]
		for match in iterator:
			allStr=match.group()
			prefixStr=re.compile(self.game_config.pricePrefix).search(allStr).group()
			numStr=allStr.replace(prefixStr,'').replace(',','')
			q=int(numStr)
			inventoryPrice.append(q)
		#print (inventoryPrice)

		print ('Inventory for '+who+': ',)
		for i in range(1, len(self.inventoryList)):
			print (self.inventoryList[i]+'('+str(inventoryQuantity[i])+') $'+str(inventoryPrice[i]),)
		print ('')

		for i in range(1, len(self.inventoryList)):
			if self.targetList[i]>inventoryQuantity[i]:
				if (self.targetList[i]-inventoryQuantity[i])*inventoryPrice[i]<=self.money and not self.pendingToBuy:
					#Buying
					buying_action='index.php?c='+self.game_config.buying_action1+who+self.game_config.buying_action2+str(i)
					d_buying_action = buying_action.replace('&amp;', '&')
					ip=SimpleFormParser(d_buying_action, {'amount':str(self.targetList[i]-inventoryQuantity[i])}, {})
					ip.feed(page)
					ip.close()
					if len(ip.forms)>0:
						print ('Buying',i,'for ',who,'quantity:',(self.targetList[i]-inventoryQuantity[i]))
						ip.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+ip.forms[0].action)
					self.money=self.money-(self.targetList[i]-inventoryQuantity[i])*inventoryPrice[i]
				elif (self.targetList[i]-inventoryQuantity[i])*inventoryPrice[i]>self.money:
					self.pendingToBuy=True
class JobMain(JobRunner):
	opener=None
	game_config=GameConfig()
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.profile=ProfileLoader(self.opener, self.game_config)
		self.jobDescription='Treasure Hunt'
	def doJob(self):
		links=re.compile('tag&amp;location=[0-9]+').findall(self.profile.util.openLink('tag'))
		if len(links)>0:
			self.profile.util.openLink(links[random.randint(0, len(links)-1)].replace('&amp;','&'))

		self.sleepTime=[14*60, 15*60]
class PropertyMain(JobRunner):
	profile=None
	game_config=GameConfig()
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.jobDescription='Property'
		self.profile=ProfileLoader(f, self.game_config)
	def upgrade(self, link):
		html=self.profile.util.openLink(link)
		links = re.compile('upgradeproperty&amp;p=[0-9]+&amp;item=[0-9]+').findall(html)
		for l in links:
			print ('Installing:', l)
			self.profile.util.openLink(l.replace('&amp;', '&'))
	def doJob(self):
		html=self.profile.util.openLink('properties')
		links = re.compile('upgradeproperty&amp;p=[0-9]+').findall(html)
		for l in links:
			print ('Upgrading:', l)
			self.upgrade(l.replace('&amp;', '&'))

		html=self.profile.util.openLink('loot')
		links = re.compile('loot&amp;sell=[0-9]+[^"]*').findall(html)
		for l in links:
			print ('Selling:', l)
			self.profile.util.openLink(l.replace('&amp;', '&'))

		self.sleepTime=[30*60, 60*60]

class FightMain(JobRunner):
	opener=None
	profile=None
	member=0
	game_config=GameConfig()
	friends=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.member=0
		self.appID=self.game_config.appID
		self.friends=[]
		self.jobDescription='Fight'
		self.profile=ProfileLoader(f, self.game_config)
	def loadFriends(self):
		self.friends=[]
		w = WhiteListLoader(self.opener)
		self.friends=self.friends+w.whiteList
		links=re.compile('img uid="[0-9]+"').findall(self.profile.util.openLink(self.game_config.friendURL))
		for l in links:
			m=re.compile('[0-9]+').search(l)
			if m!=None and self.friends.count(m.group)==0:
				self.friends.append(m.group())
		print ('Friends ('+self.game_config.appID+', '+str(len(self.friends))+'):', self.friends)
	def doJob(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		while self.profile.data['Member']>self.member:
			print ('Friends changed! Reloading...')
			self.loadFriends()
			if len(self.friends)>0:
				self.member=self.profile.data["Member"]
		self.fight('driveby', 'driveby&[^"\\\\]+', True)
		self.fight('attack', 'attack[^"\\\\]+', False)
		self.updateSleepTime()

	def fight(self, mainpage, linkPattern, fightFriends):
		links=set(re.compile(linkPattern).findall(self.profile.util.openLink(mainpage)))
		print ('Fighting: '+mainpage, links)
		for l in links:
			if self.profile.data['Energy']<=self.profile.data['EnergyMax']/2:
				break
			decoded_l=l.replace('&amp;','&')
			m=re.compile('who=[0-9]+').search(decoded_l)
			if m!=None:
				id=re.compile('[0-9]+').search(m.group()).group()
				if fightFriends or self.friends.count(id)==0:
					print ('Fighting: '+decoded_l)
					html=self.profile.util.openLink(decoded_l)
					self.profile.util.grabLoot(html)
					self.profile.loadProfile(html)
				else:
					print ('Do not fight friends: '+decoded_l)
			elif fightFriends:
				print ('Fighting: '+decoded_l)
				html=self.profile.util.openLink(decoded_l)
				self.profile.util.grabLoot(html)
				self.profile.loadProfile(html)
	def updateSleepTime(self):
		sleepTime=self.game_config.energyRechargeTime*(self.profile.data['EnergyMax']-self.profile.data['Energy'])
		if sleepTime>=self.game_config.energyRechargeTime/2:
			sleepTime=sleepTime-self.game_config.energyRechargeTime/2
		else:
			sleepTime=0

		self.sleepTime=[sleepTime, sleepTime+self.game_config.energyRechargeTime/2]
class InventoryMain(JobRunner):
	profile=None
	game_config=GameConfig()
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=self.game_config.appID
		self.jobDescription='Inventory'
		self.profile=ProfileLoader(f, self.game_config)
	def doJob(self):
		self.profile.loadProfile()
		iLoader=InventoryLoader(self.opener, self.game_config, self.profile.data['Cash'])
		links=re.compile(self.game_config.individualGang).findall(self.profile.util.openLink('viewgang'))
		random.shuffle(links)
		for l in links:
			iLoader.foundLink(l)
		self.sleepTime=[40*60, 60*60]


import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.treasurehunt.ProfileSearcher import *
from .ProfileLoader import ProfileLoader

appID='crusades'
class FriendLoader:
	friendList=[]
	def __init__(self):
		self.friendList=[]
	def foundLink(self, link):
		friend=re.compile('[0-9]+').search(link)
		if friend!=None:
			self.friendList.append(friend.group())
class Target:
	name=''
	level=0
	warriors=0
	header=[]
	action=''
	def __init__ (self):
		self.name=''
		self.level=0
		self.warriors=0
		self.action='/'+appID+'/battle'
		self.header=[]
	def display(self):
		print ('Name:', self.name, ', Level:', self.level, 'Warriors:', self.warriors, 'Action:', self.action)
		print (self.header)
class FightParser(HTMLParser):
	detector=0
	currentTarget=None
	target=None
	friendList=[]

	def __init__(self):
		HTMLParser.__init__(self)
		self.detector=0
		self.currentTarget=None
		self.target=None
		self.friendList=[]
	def handle_starttag(self, tag, attrs):
		if self.detector==0 and tag=='table':
			for a in attrs:
				if a[0]=='class' and a[1]=='grid':
					#Begin to enter the table
					self.detector=1
		elif self.detector==1 and tag=='tr':
			#Table head
			self.detector=2
		elif self.detector==2 and tag=='tr':
			#Table data
			self.currentTarget=Target()
			self.detector=3
		elif self.detector==3 and tag=='td':
			#Name
			self.detector=4
		elif self.detector==5 and tag=='td':
			#Level
			self.detector=6
		elif self.detector==7 and tag=='td':
			#No. of warriors
			self.detector=8
		elif self.detector==9 and tag=='form':
			for a in attrs:
				if a[0]=='action' and re.compile('/'+appID+'/battle/attack/[0-9]+').search(a[1])!=None:
					self.currentTarget.action=a[1]
		elif self.detector==9 and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.currentTarget.header.append((name, value))

	def handle_data (self, data):
		if self.detector==4:
			self.currentTarget.name=data
			#Level <td> tag
			self.detector=5
		elif self.detector==6:
			m=re.compile('[0-9]+').search(data)
			if m!=None:
				self.currentTarget.level=int(m.group())
				#No. of warriors <td> tag
				self.detector=7
		elif self.detector==8:
			m=re.compile('[0-9]+').search(data)
			if m!=None:
				self.currentTarget.warriors=int(m.group())
				#Form
				self.detector=9
	def handle_endtag(self, tag):
		if self.detector==9 and tag=='tr':
			if self.isFriend(self.currentTarget):
				print ('Friend: '+self.currentTarget.action+' found! Do NOT attack!')
			elif self.target==None:
				self.target=self.currentTarget
			elif random.randint(0, 1)==0:
				self.target=self.currentTarget
			self.detector=2
	def isFriend(self, target):
		for f in self.friendList:
			if re.compile(f).search(self.currentTarget.action)!=None:
				return True
		return False

class FightMain(JobRunner):
	profile=None
	hpRechargeTime=150
	strengthRechargeTime=30
	friends=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.profile=ProfileLoader(f)
		self.appID=appID
		self.jobDescription='Fight'
		self.friends=None

	def getHealthRechargTime(self):
		t=(10-self.profile.health)*self.hpRechargeTime
		if t<0:
			t=0
		return t
	def getStrengthRechargTime(self):
		t=(3-self.profile.stamina)*self.strengthRechargeTime
		if t<0:
			t=0
		return t
	def getRechargeTime(self):
		time1=self.getHealthRechargTime()
		time2=self.getStrengthRechargTime()
		if time1>=time2:
			return time1
		else:
			return time2
	def finishJob(self):
		print ('Finishing job ('+appID+')')
		self.profile.loadProfile()
		while self.profile.health>=10:
			noPlayer=self.fight()

			if self.profile.health<10:
				break
			elif noPlayer:
				self.sleepTime=[300-60, 300+60]
			else:
				sleepTime=self.getStrengthRechargTime()
				self.sleepTime=[sleepTime, sleepTime+60]
			time.sleep(random.randint(self.sleepTime[0], self.sleepTime[1]))
			self.profile.loadProfile()
		print ('Job finished ('+appID+')')
	def initJob(self):
		#Load friends
		self.opener.login()
		self.friends=FriendLoader()
		friend_links = LinkSearcher(self.opener, 'inventory/view/[0-9]+')
		friend_links.callback=self.friends
		friend_links.prefix=Facebook.appsURL+'/'+appID+'/invite/view/all?all=1'
		friend_links.load('')
		print ('Friends:', self.friends.friendList)
		self.opener.logout()
	def fight(self):
		noPlayer=False
		fp=FightParser()
		fp.friendList=self.friends.friendList
		html=self.opener.open(Facebook.appsURL+'/'+appID+'/battle')
		while self.profile.health>=10 and self.profile.stamina>=3:
			if re.compile('There are no players to battle right now').search(html)!=None:
				noPlayer=True
				break
			fp.feed(html)
			fp.close()
			if fp.target!=None:
				fp.target.display()
				html=self.opener.open(Facebook.appsURL+fp.target.action, urllib.parse.urlencode(fp.target.header))
			else:
				html=self.opener.open(Facebook.appsURL+'/'+appID+'/battle')
			self.profile.loadProfile()
		return noPlayer

	def doJob(self):
		self.profile.loadProfile()
		noPlayer=self.fight()

		if noPlayer:
			self.sleepTime=[300-60, 300+60]
		else:
			sleepTime=self.getRechargeTime()
			if sleepTime<0:
				sleepTime=0
			self.sleepTime=[sleepTime, sleepTime+60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		fight = FightMain(f)
		fight.initJob()
		f.login()
		fight.finishJob()
		f.logout()
	else:
		fight = FightMain(f)
		fight.start()


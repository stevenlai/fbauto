import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from .ProfileLoader import ProfileLoader
from .fight import FightMain

appID='crusades'
class JobParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	jobID=''
	action='/'+appID+'/quests/do_quest/'

	def __init__(self, opener, jobID):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
		self.jobID=jobID
		self.action='/'+appID+'/quests/do_quest/'+jobID
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			for a in attrs:
				if a[0]=='action' and a[1]==self.action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Using action ('+appID+'): '+self.jobID)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			if re.compile("http://").search(self.action)==None:
				self.opener.open(Facebook.appsURL+self.action, input)
			else:
				self.opener.open(self.action, input)
			self.begin=False

class JobMain(JobRunner):
	opener=None
	profile=None
	counter=0
	jobList=[[40, 1, '194', ['daylight']], [20, 1, '190', ['nightfall']], [20, 1, '191', ['sunrise', 'nightfall approaching', 'dawn']]]
	#jobList=[[20, 1, '190', ['sunrise', 'daylight', 'nightfall approaching']], [20, 1, '191', ['nightfall', 'dawn']]]
	#jobList=[[6, 1, '182', None]]
	#jobList=[[4, 1, '185', ['sunrise', 'daylight', 'nightfall approaching']], [2, 1, '178', ['nightfall', 'dawn']]]
	energyRechargeTime=280
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.profile=ProfileLoader(f)
		self.appID=appID
		self.jobDescription='Earn'
	def earn (self, jobID):
		print ('Earning ('+appID+') ...')
		page = Facebook.appsURL+'/'+appID+'/quests'
		ap = JobParser(self.opener, jobID)
		ap.feed(self.opener.open(page))
		ap.close()
	def calcJobID(self):
		self.counter=0
		jobFound=False
		for j in self.jobList:
			if j[3]==None:
				break
			for t in j[3]:
				if re.compile(t).search(self.profile.time)!=None:
					jobFound=True
				if jobFound:
					break
			if jobFound:
				break
			self.counter=self.counter+1

	def doJob(self):
		self.profile.loadProfile()
		self.calcJobID()
		energy=self.profile.energy
		if energy>=self.jobList[self.counter][0]:
			for i in range(0, self.jobList[self.counter][1]):
				self.earn(self.jobList[self.counter][2])
			self.profile.loadProfile()
			#Job done (energy deducted)
			if energy>self.profile.energy:
				#Calculate the energy required for the next job
				sleepTime=(self.jobList[self.counter][0]-self.profile.energy)*self.energyRechargeTime
				if sleepTime<0:
					sleepTime=0
				self.sleepTime=[sleepTime, sleepTime+60]
			else:
				self.sleepTime=[0, 60]
		else:
			#Not enough energy for the current job
			sleepTime=(self.jobList[self.counter][0]-energy)*self.energyRechargeTime
			self.sleepTime=[sleepTime, sleepTime+60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = JobMain(f)
		job.doJob()
		f.logout()
	else:
		#crusade_timer=JobTimer([4*3600, 6*3600], [18*3600, 20*3600])
		job = JobMain(f)
		#job.jobTimer=crusade_timer
		job.start()
		time.sleep(random.randint(20, 60))
		fight = FightMain(f)
		#fight.jobTimer=crusade_timer
		fight.start()

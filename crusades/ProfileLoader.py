import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

appID='crusades'
class ProfileLoader:
	energy=0
	health=0
	stamina=0

	gold=0
	wood=0
	stone=0

	time=''
	opener=None
	jailed=False
	page=''

	def __init__(self, f):
		self.opener=f
		self.energy=0
		self.health=0
		self.stamina=0
		self.gold=0
		self.wood=0
		self.stone=0
		self.time=''
		self.jailed=False
		self.page=''
	def loadPage(self):
		self.page=self.opener.open('http://apps.facebook.com/'+appID+'/')
		while re.compile('[0-9,]+<br').search(self.page)==None:
			print ('Error loading the profile! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			self.page=self.opener.open('http://apps.facebook.com/'+appID+'/')
	def loadProfile (self):
		self.loadPage()
		iterator=re.compile("[ \t]+[0-9,]+/[0-9,]+").finditer(self.page)
		i=0
		for match in iterator:
			nostr=re.compile("[0-9,]+").search(re.compile("[0-9,]+/").search(match.group()).group()).group()
			if i==0:
				self.health=int(nostr.replace(',', ''))
			elif i==1:
				self.energy=int(nostr.replace(',', ''))
			else:
				self.stamina=int(nostr.replace(',', ''))
			i=i+1

		goldstr=re.compile("[0-9,]+").search(re.compile('[0-9,]+<br').search(self.page).group()).group()
		self.gold=int(goldstr.replace(',', ''))

		iterator=re.compile("[ \t]+[0-9,]+").finditer(self.page)
		i=0
		for match in iterator:
			nostr=re.compile("[0-9,]+").search(match.group()).group()
			if i==0:
				self.wood=int(nostr.replace(',', ''))
			elif i==1:
				self.stone=int(nostr.replace(',', ''))
			i=i+1

		self.time=re.compile("Currently [a-z]+").search(self.page).group()
		print ('Profile loaded. Energy:',self.energy,'Health:',self.health,'Stamina:',self.stamina,'Gold:',self.gold,'Wood:',self.wood,'Stone:',self.stone,'Time:',self.time)


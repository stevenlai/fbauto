from FBAuto.common.facebook import Facebook
import FBAuto.common.zynga_common

appID='prisonlockdowngame'
config = FBAuto.common.zynga_common.GameConfig()
config.appID=appID
config.energyPrefix='Energy:.*<strong><span id="app[0-9]+_current_energy" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.healthPrefix='Health:.*<strong><span id="app[0-9]+_current_health" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.staminaPrefix='Adrenaline:.*<strong><span id="app[0-9]+_current_stamina" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.cashPrefix='Cigs:.*<strong class="money"><img src="[^>]*>[ ]*'
config.bankPrefix='Total Cigs:.*<b class="money"><img src=[^>]+>'
config.propertyPrefix='Owned:[^0-9]+'
config.memberPrefix='my gang[ ]*\('
config.jailstr='Jail'
config.fightMsg='You fought against'
config.minDeposit=10
config.energyRechargeTime=200

if __name__ == "__main__":
	f = Facebook('facebook')
	f.login()
	p=FBAuto.common.zynga_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()
	f.logout()

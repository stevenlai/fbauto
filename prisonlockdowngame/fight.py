import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
from .common import *
import FBAuto.common.zynga_common

class FightMain(FBAuto.common.zynga_common.FightMain):
	game_config=config
class HealMain(FBAuto.common.zynga_common.HealMain):
	game_config=config
if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		fight = HealMain(f)
		fight.doJob()
		f.logout()
	else:
		fight = FightMain(f)
		fight.start()
		#time.sleep(random.randint(20, 60))
		#heal=HealMain(f)
		#heal.start()

import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.FormParser import *
from FBAuto.common.AjaxProcessor import *
from FBAuto.common.ProfileLoader import *
from FBAuto.treasurehunt.ProfileSearcher import *

class GameConfig:
	appID='fbfairy'
	goldPrefix='<div id="app[0-9]+_goldcount"[^>]*>'
	diamondPrefix='<div id="app[0-9]+_diamondcount"[^>]*>'
	myGardenID='1736660'
	shopURL='shop.php'
	gameID='202'
	goldThreshold=500
class ProfileLoader(GeneralProfileLoader):
	game_config=None
	opener=None
	def __init__(self, f, game_config):
		self.opener=f
		self.game_config=game_config
		profile_config = ProfileConfig()
		profile_config.appID=self.game_config.appID
		profile_config.keepRetry=True
		profile_config.profileURL=Facebook.appsURL+'/'+self.game_config.appID+'/'+self.game_config.shopURL
		profile_config.meta=[('Gold', self.game_config.goldPrefix, None), ('Diamond', self.game_config.diamondPrefix, None)]
		GeneralProfileLoader.__init__(self, f, profile_config)
	def loadProfile (self, html=None):
		GeneralProfileLoader.loadProfile(self, html)
		m=re.compile('fairygarden\.php\?id=([0-9]+).*pixel\.gif').search(self.page)
		if m!=None:
			self.data["myGardenID"]=m.group(1)
		else:
			self.data["myGardenID"]=self.game_config.myGardenID

class FeedMain(JobRunner):
	game_config=GameConfig()
	searchingDepth=0
	maxDepth=20
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.profile=ProfileLoader(self.opener, self.game_config)
		self.jobDescription='Feed'

	def feedIndividualPlant(self, gardenID, plantID, ajax):
		print ('Watering:', plantID, 'for garden:', gardenID, '...')
		info = ajax.getHeader()
		info["type"]='1'
		info["require_login"]='true'
		#info["query[action]"]='water'
		info["query[item]"]=plantID
		info["query[id]"]=gardenID
		info["query[type]"]=''
		return ajax.post(self.opener)
	def collectDiamond(self, page, ajax, gardenID):
		diamonds = re.compile('collectdiamond\([0-9]{3, }\)').findall(page)
		print ('All diamonds:',diamonds)
		diamond_ajax = AjaxProcessor()
		diamond_ajax.ajax_url_suffix='gardenajaxcollect\.php'
		diamond_ajax.process(page)

		info=diamond_ajax.getHeader()
		info["url"]=ajax.header["url"].replace('gardenajaxwater', 'gardenajaxcollect')
		info["appid"]=ajax.header["appid"]
		info["type"]='1'
		info["require_login"]='true'
		info["query[action]"]='collectdiamond'
		info["query[id]"]=gardenID

		if len(diamonds)>0:
			info["query[item]"]=re.compile('[0-9]+').search(diamonds[random.randint(0, len(diamonds)-1)]).group()
			print ('Diamond going to collect:', info["query[item]"])
			result=diamond_ajax.post(self.opener)
		
			if re.compile('Error collecting Diamond.').search(result)!=None:
				print ('Error collecting Diamond.')
			else:
				print ('Diamond collected successfully!')
				return True
		else:
			print ('Watered but no diamond!')
		return False
	def feedPlant(self, gardenID, plantID, ajax):
		page=self.feedIndividualPlant(gardenID, plantID, ajax)
		finishedGarden=False

		if re.compile('has watered the .* - thank you!').search(page)!=None:
			self.collectDiamond(page, ajax, gardenID)
			finishedGarden=True
		else:
			print ('Cannot water!')
			finishedGarden=True

		self.profile.loadProfile()

		if self.profile.data["Gold"]<self.game_config.goldThreshold:
			return True
		else:
			return finishedGarden
	def feed(self, garden_link):
		html=self.profile.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+garden_link)
		m = re.compile('a[0-9]+_gardenid = ([0-9]+)').search(html)
		if m!=None:
			gardenID=m.group(1)
		else:
			return
		print ('Watering:', gardenID, 'Depth:', self.searchingDepth)

		a = AjaxProcessor()
		a.ajax_url_suffix='gardenajaxwater.php'

		a.var.update({'a[0-9]+_callback': None})	
		a.var.update({'a[0-9]+_url': 'gardenajaxwater'})	
		a.process(html)

		if re.compile('Mini Mushrooms!').search(html)!=None:
			water_links = re.compile('a[0-9]+_water\([0-9]+.*Water It').findall(html)

			if len(water_links)==0:
				print ('No water link found!')
			for w in water_links:
				plantID=re.compile('a[0-9]+_water\(([0-9]+)').search(w).group(1)
				if self.feedPlant(gardenID, plantID, a):
					break
		else:
		      print ('No mushroom game found!')
	def visitGarden(self, gardenLink):
		if self.searchingDepth>self.maxDepth:
			return

		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+gardenLink)
		garden_links = set(re.compile('fairygarden\.php\?uid=[0-9]+').findall(html))
		garden_links.discard('fairygarden.php?id='+self.profile.data["myGardenID"])

		for g in garden_links:
			if self.profile.data["Gold"]>=self.game_config.goldThreshold:
				self.feed(g)
		for g in garden_links:
			if self.profile.data["Gold"]>=self.game_config.goldThreshold:
				self.searchingDepth=self.searchingDepth+1
				self.visitGarden(g)
				self.searchingDepth=self.searchingDepth-1
	def doJob(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		self.searchingDepth=0
		self.visitGarden('fairygarden.php?id='+self.profile.data["myGardenID"])
		self.sleepTime=[3600*5, 3600*10]

	def doJob_test(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		#self.opener.open('http://apps.facebook.com/fbfairy/fairygarden.php?id=3728')
		self.feed('fairygarden.php?id=1768729')
class FeedNoStop(FeedMain):
	accounts=[]
	target=''
	scheduledSleepTime=[10*60, 15*60]
	def feed(self, garden_link):
		html=self.profile.loadPage(Facebook.appsURL+'/'+self.game_config.appID+'/'+garden_link)
		m = re.compile('a[0-9]+_gardenid = ([0-9]+)').search(html)
		if m!=None:
			gardenID=m.group(1)
		else:
			return
		print ('Watering:', gardenID, 'Depth:', self.searchingDepth)

		a = AjaxProcessor()
		a.ajax_url_suffix='gardenajaxwater.php'

		a.var.update({'a[0-9]+_callback': None})
		a.var.update({'a[0-9]+_url': 'gardenajaxwater'})
		a.process(html)

		if re.compile('Mini Mushrooms!').search(html)!=None:
			water_links = re.compile('a[0-9]+_water\([0-9]+.*Water It').findall(html)

			random.shuffle(water_links)
			if len(water_links)==0:
				print ('No water link found!')
			for w in water_links:
				plantID=re.compile('a[0-9]+_water\(([0-9]+)').search(w).group(1)
				page=self.feedIndividualPlant(gardenID, plantID, a)
				if re.compile('you were just beaten').search(page)==None:
					break
		else:
		      print ('No mushroom game found!')
	def doJob(self):
		if len(self.accounts)>0:
			self.opener=self.accounts[random.randint(0, len(self.accounts)-1)]
			print ('Using acct:',self.opener.email)
			self.profile=ProfileLoader(self.opener, self.game_config)

		self.profile.loadProfile()
		self.profile.displayProfile()
		self.searchingDepth=0
		self.feed(self.target)
		self.sleepTime=self.scheduledSleepTime
class JobMain(JobRunner):
	game_config=GameConfig()
	profile=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=self.game_config.appID
		self.profile=ProfileLoader(self.opener, self.game_config)
		self.jobDescription='Treasure Hunt'
	def foundLink(self, link):
		m=re.compile('[0-9]+').search(link)
		if m!=None:
			links = LinkSearcher(self.opener, 'earngold\.php\?[^"]+')
			links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/'+link
			links.callback=None
			links.load('')
			for l in links.linkList:
				decoded_l=l.replace('&amp;', '&')
				print ('Claiming gold:',decoded_l)
				self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+decoded_l)
	def buyGame(self):
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/shop.php?mode=game&gardenid='+self.profile.data["myGardenID"])
		p = SimpleFormParser('shopbuy.php', {'itemid':self.game_config.gameID}, {})
		p.feed(html)
		p.close()
		if len(p.forms)>0:
			p.forms[0].submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+'shopbuy.php')
	def doJob(self):
		self.profile.loadProfile()
		self.profile.displayProfile()
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/fairyfriends.php')
		p = SimpleFormParser('earngold.php?uid=[0-9]+', {}, {})
		p.feed(html)
		p.close()
		while len(p.forms)>0:
			for f in p.forms:
				f.submit(self.opener, Facebook.appsURL+'/'+self.game_config.appID+'/'+f.action)
			html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/fairyfriends.php')
			p = SimpleFormParser('earngold.php?uid=[0-9]+', {}, {})
			p.feed(html)
			p.close()



		links = LinkSearcher(self.opener, 'earngold\.php\?[^"]+')
		links.prefix=Facebook.appsURL+'/'+self.game_config.appID+'/earngold.php'
		links.callback=None
		links.load('')
		for l in links.linkList:
			decoded_l=l.replace('&amp;', '&')
			print ('Claiming gold:',decoded_l)
			self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+decoded_l)

		#Check the game status:
		html=self.opener.open(Facebook.appsURL+'/'+self.game_config.appID+'/'+'fairygarden.php?id='+self.profile.data["myGardenID"])
		if re.compile('Garden Game').search(html)!=None and re.compile('Expires').search(html)==None:
			print ('Game expired, buying ...')
			self.buyGame()

		self.sleepTime=[40*60, 80*60]

if __name__ == "__main__":
	f = Facebook('facebook')

	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = FeedMain(f)
		job.doJob()
	else:
		job = FeedNoStop(f)
		#job.target='fairygarden.php?id=2039038'
		#job.target='fairygarden.php?id=2125841'
		job.target='fairygarden.php?id=1736660'
		#job.accounts=[Facebook('facebook', ('eve.engel@rocketmail.com', '64446103phoenixsexengel'))]
		job.accounts=[Facebook('facebook', ('kimibunny22@gmail.com', '@vichet2')), Facebook('facebook', ('eve.engel@rocketmail.com', '64446103phoenixsexengel'))]
		job.scheduledSleepTime=[15*60, 20*60]
		job.start()


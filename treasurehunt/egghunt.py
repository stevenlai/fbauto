import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.CaptchaProcessor import *
from FBAuto.common.FormParser import *
from .ProfileSearcher import *

appID='egghunt'
class PetFeeder:
	feedList=[]
	opener=None
	def __init__(self, f):
		self.feedList=[]
		self.opener=f
	def foundLink(self, link):
		self.feedList.append(link)
	def close(self):
		#Just in case there's no pet at the moment
		if len(self.feedList)>0:
			random_pet=self.feedList[random.randint(0, len(self.feedList)-1)]
			url=Facebook.appsURL+'/'+appID+'/'+random_pet
			print ('Feeding to pet:', url)
			self.opener.open(url)
class HatchlingLoader:
	opener=None
	def __init__(self, f):
		self.opener=f
	def load(self):
		html = self.opener.open(Facebook.appsURL+'/'+appID+'/'+'givetopet.php')
		pets = re.compile('[a-z]+/hatched\.png').findall(html)
		unique_p = set(pets)
		others = set(re.compile('[a-z]+/hatched\.png').findall(self.opener.open(Facebook.appsURL+'/'+appID+'/'+'basket.php?id=603067652&showAll')))
		alleggs=others|unique_p
		for p in alleggs:
			if pets.count(p)>1:
				print ('Duplicated:',p.replace('/hatched.png', ''))
			if not p in unique_p:
				print ('To find:',p.replace('/hatched.png', ''))
			#print (p.replace('/hatched.png', ''))
class FeedMain(JobRunner):
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.appID=appID
		self.jobDescription='Feed'
	def doJob(self):
		html = self.opener.open(Facebook.appsURL+'/'+appID+'/'+'givetopet.php')
		feed = re.compile('givetopet\.php\?pid=[^"]+').findall(html)
		for f in feed:
			print ('Feeding:',f)
			self.opener.refresh(Facebook.appsURL+'/'+appID+'/'+f.replace('&amp;','&'))
		self.sleepTime=[6*3600, 7*3600]
class EggParser(NoInvisibleParser):
	data=''
	linkExist=False
	eggLinks=[]
	findStr='You found an egg!'
	linkPattern='found\.php\?location=[0-9]+&sig=[0-9]+&ub=[0-9a-zA-Z]+'

	def __init__(self):
		NoInvisibleParser.__init__(self)
		self.data=''
		self.linkExist=False
		self.eggLinks=[]
	def handle_starttag(self, tag, attrs):
		#if tag.lower()=='a' and self.linkExist:
		if tag.lower()=='a':
			for a in attrs:
				if a[0].lower()=='href':
					m=re.compile(self.linkPattern).search(a[1])
					if m!=None and self.eggLinks.count(m.group())==0:
						print (m.group())
						self.eggLinks.append(m.group())
			self.linkExist=False
		self.data=''
	def handle_data(self, data):
		#print ('Data: '+self.data)
		#self.data=self.data+' '+data
		self.data=data
		if re.compile(self.findStr).search(self.data)!=None:
			print ('Egg found ('+self.data+')',)
			self.linkExist=True
	def handle_endtag(self, tag):
		pass
	def getLink(self):
		if len(self.eggLinks)>1:
			print ('Egg link is not unique!', self.eggLinks)
			return None
		elif len(self.eggLinks)==1:
			return self.eggLinks[0]
		else:
			return None
	def findLink(self, html):
		iter=re.compile(self.findStr+'[^>]*('+self.linkPattern.replace('&', '&amp;')+')').finditer(html)
		for i in iter:
			print ('Egg found ('+i.group()+')')
			self.eggLinks.append(i.group(1))
class JobMain(JobRunner):
	opener=None
	profiles=None
	links=None
	maxDepth=20
	eggCount=0
	eggMaxCount=150
	recursiveSearch=False
	eggList=[]
	searchingDepth=0
	captchaFound=False
	captchaFailure=0
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.maxDepth=20
		self.jobDescription='Treasure Hunt'
		self.eggList=[]
		self.searchingDepth=0
		self.eggCount=0
		self.eggMaxCount=150
		JobMain.captchaFound=False
		JobMain.captchaFailure=0

	def isLimitedHatchable(self, html):
		self.loadLimitedEggs()
		m=re.compile('[a-zA-Z]+/egg-found').search(html)
		if m!=None:
			eggname=m.group()
			print ('Found egg: '+eggname)
			for e in self.eggList:
				if re.compile(e+'/egg-found').search(eggname)!=None and re.compile('incubate\.php').search(html)!=None:
					print ('Limited edition egg: '+e+' is found!')
					return True
		elif not JobMain.captchaFound:
			print ('Cannot find egg type!')
		return False
	def hatch(self, html):
		m=re.compile('incubate\.php\?[^"]+').search(html)
		if m!=None:
			decoded_link=m.group().replace('&amp;', '&')
			self.opener.open(Facebook.appsURL+'/'+appID+'/'+decoded_link)
	def processEgg(self, eggLink, html):
		location_id=re.compile('location=([0-9]+)').search(eggLink).group(1)
		sig=re.compile('sig=([0-9]+)').search(eggLink).group(1)
		#save_url=Facebook.appsURL+'/'+appID+'/foundAjax.php?fbid='+self.opener.thisUserID+'&location='+location_id+'&sig='+sig
		#print ('Processing egg:', save_url)
		html = self.opener.open(Facebook.appsURL+'/'+appID+'/'+eggLink)

		parser = SimpleFormParser(eggLink, {}, {})
		processor = CaptchaProcessor(self.opener, appID, 'Security Check', RecaptchaGetter(self.opener))
		JobMain.captchaFound = processor.process(html, parser, Facebook.appsURL+'/'+appID+'/'+eggLink)

		self.securityProcess()
		if self.isLimitedHatchable(html):
			#html = self.opener.open(save_url)
			self.hatch(html)
		elif random.randint(0, 9)==0:
			feeder=PetFeeder(self.opener)
			feeder_link=LinkSearcher(self.opener, 'givetopet\.php\?pid=[0-9]+')
			feeder_link.callback=feeder
			feeder_link.prefix=Facebook.appsURL+'/'+appID+'/givetopet.php'
			feeder_link.load('')
			feeder.close()

	def found(self, link, html):
		decoded_link = link.replace('&amp;', '&')
		#print (decoded_link)
		location_id=re.compile('location=([0-9]+)').search(decoded_link).group(1)
		self.processEgg(decoded_link, html)

		if self.recursiveSearch:
			#Further searching the baskets
			self.searchingDepth=self.searchingDepth+1
			if self.searchingDepth>=self.maxDepth:
				return
			self.search(Facebook.appsURL+'/'+appID+'/basket.php?id='+location_id)
			print ('Finished searching:', location_id)
			self.searchingDepth=self.searchingDepth-1
	def loadLimitedEggs(self):
		self.eggList=[]
		d = self.opener.getConfigFile(appID+'.txt')
		f = open(d, 'r')
		m = re.compile('[a-z]+').search(f.readline())
		if m!=None:
			egg = m.group()
		else:
			egg=''
		while egg!='':
			self.eggList.append(egg)
			m = re.compile('[a-z]+').search(f.readline())
			if m!=None:
				egg = m.group()
			else:
				egg=''
		f.close()
		#print ('Limited eggs:', self.eggList)
	def getEggLink(self, html):
		p = EggParser()
		p.findLink(html)
		#p.feed(html)
		#p.close()
		return p.getLink()
	def removeInvisible(self, html):
		p = EggParser()
		p.feed(html)
		p.close()
		for r in p.regions:
			r.displayPos()
		lineNo=1
		colNo=0
		visible=''
		invisible=''
		print (len(html))
		for i in range(0, len(html)):
			if html[i]=='\n':
				lineNo=lineNo+1
				colNo=0
			else:
				colNo=colNo+1
			isVisible=True
			for r in p.regions:
				if r.contain((lineNo, colNo)):
					isVisible=False
					break
			if isVisible:
				visible=visible+html[i]
			else:
				invisible=invisible+html[i]
		print ("Visible:",visible)
		print ("Invisible:",invisible)
		return visible
	def search(self, url):
		if not self.jobIsRunning:
			return

		print ('Searching in:', url)

		baskets = LinkSearcher(self.opener, 'basket\.php\?id=[0-9]+[^<"]*')
		baskets.callback=None
		baskets.prefix=url
		baskets.load('')

		random.shuffle(baskets.linkList)
		for l in baskets.linkList:
			html=self.opener.open(Facebook.appsURL+'/'+appID+'/'+l.replace('&amp;', '&'))
			eggLink=self.getEggLink(html)
			if eggLink!=None:
				self.found(eggLink, html)
				self.securityCheck()
	def securityProcess(self):
		if JobMain.captchaFound and self.eggCount>20:
			print ('Max Count ('+str(self.eggMaxCount)+') -> Current Count ('+str(self.eggCount)+')')
			self.eggMaxCount=self.eggCount
			self.eggCount=0
			JobMain.captchaFailure=JobMain.captchaFailure+1
			if JobMain.captchaFailure>20:
				self.stopJob()
		elif not JobMain.captchaFound:
			self.eggCount=self.eggCount+1
			JobMain.captchaFailure=0
	def securityCheck(self):
		if JobMain.captchaFound:
			time.sleep(random.randint(60, 120))
		elif self.eggCount>self.eggMaxCount:
			print ('Sleep for security:', str(self.eggCount)+'/'+str(self.eggMaxCount))
			time.sleep(random.randint(1, (self.eggCount-self.eggMaxCount)%30))
	def doJob(self):
		self.searchingDepth=0
		self.loadLimitedEggs()
		jobList=[(False, 'http://www.fordetectives.com/networks/External.html'), (False, 'http://www.fordetectives.com/networks/External2.html'), (False, 'http://www.fordetectives.com/networks/External3.html'), (False, Facebook.appsURL+'/'+appID+'/friends.php'), (True, Facebook.appsURL+'/'+appID+'/network.php?id=67109267')]
		random.shuffle(jobList)

		for j in jobList:
			self.recursiveSearch=j[0]
			self.search(j[1])

		self.sleepTime=[0*60, 1*60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "ldt")
	runOnce=False
	displayEggs=False
	test=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
		if o == "-d":
			displayEggs=True
		if o == "-t":
			test=True
	if displayEggs:
		l = HatchlingLoader(f)
		l.load()
	elif test:
		print ('Testing...')
		f.login()
		job = JobMain(f)
		html = f.open(Facebook.appsURL+'/'+appID+'/basket.php?id='+f.thisUserID)
		p = EggParser()
		p.linkPattern='found\.php[^\'"]*'
		p.findLink(html)
		print (p.getLink())
		p = EggParser()
		p.linkPattern='found\.php[^\'"]*'
		p.feed(html)
		p.close()
		print (p.getLink())
	elif runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = FeedMain(f)
		job.doJob()
	else:
		#f.rememberPass=True
		try:
			job = JobMain(f)
			job.start()
		except KeyboardInterrupt:
			self.opener.logout()
			sys.exit()


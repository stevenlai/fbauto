import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from .ProfileSearcher import *

appID='scavengermouse'
class JobMain(JobRunner):
	opener=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.jobDescription='Treasure Hunt'
	def foundLink(self, link):
		decoded_link = link.replace('&amp;', '&')
		m=re.compile('[0-9]+').search(decoded_link)
		item_url=Facebook.appsURL+'/'+appID+'/'+decoded_link
		print ('Retrieving item:', item_url)
		self.opener.open(item_url)
	def doJob(self):
		links = LinkSearcher(self.opener, 'retrieveitem\.php\?[^"]+')
		links.callback=self
		links.prefix=Facebook.appsURL+'/'+appID+'/profiles.php?page=look'
		links.load('')
		links.prefix=Facebook.appsURL+'/'+appID+'/peekatstrangers.php?page=look'
		links.load('')
		self.sleepTime=[20*60, 40*60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = JobMain(f)
		job.doJob()
		f.logout()
	else:
		job = JobMain(f)
		job.start()




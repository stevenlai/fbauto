import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class FairyCalc:
	pattern='idx\([0-9]+\)\]="[0-9]+"'
	target='''a6956662042_item[idx(5)]="117";a6956662042_item[idx(4)]="64";a6956662042_item[idx(2)]="72";a6956662042_item[idx(1)]="5";a6956662042_item[idx(3)]="56";a6956662042_itemdesc[idx(1)]="1 Gold";a6956662042_itemdesc[idx(2)]="2 Gold";a6956662042_itemdesc[idx(3)]="3 Gold";a6956662042_itemdesc[idx(4)]="Sunshine Dust";a6956662042_itemdesc[idx(5)]="Love Dust";a6956662042_itemdesc[idx(6)]="Naughty Dust";a6956662042_itemdesc[idx(7)]="Sparkle Dust";a6956662042_itemdesc[idx(8)]="A Diamond!";a6956662042_itemdesc[idx(9)]="5 Gold!";a6956662042_itemdesc[idx(10)]="10 Gold!";a6956662042_itemdesc[idx(11)]="Nothing Here";a6956662042_itemdesc[idx(12)]="A Lost Sock!";a6956662042_itemdesc[idx(13)]="A Dragon's Scale";a6956662042_itemdesc[idx(14)]="Rare Fungus";a6956662042_itemdesc[idx(15)]="A Crystal";'''
	offset=4

	items=['', '1 Gold', '2 Gold', '3 Gold', 'Sunshine Dust', 'Love Dust', 'Naughty Dust', 'Sparkle Dust', 'A Diamond!', '5 Gold!', '10 Gold!', 'Nothing Here', 'A Lost Sock!', 'A Dragons Scale', 'Rare Fungus', 'A Crystal']
	def __init__(self):
		self.load()
	def load (self):
		iterator=re.compile(self.pattern).finditer(self.target)
		for match in iterator:
			num1num2=match.group()
			num1=re.compile('[0-9]+').search(re.compile('idx\([0-9]+').search(num1num2).group()).group()
			num2=re.compile('[0-9]+').search(re.compile('"[0-9]+').search(num1num2).group()).group()
			result=int(round(float(num2)/(float(num1)+self.offset)))
			if result<=15:
				print ('Result for', num1, ':', self.items[result])

if __name__ == "__main__":
	f = FairyCalc()
	#time.sleep(300)

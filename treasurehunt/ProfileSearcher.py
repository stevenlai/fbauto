import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class ProfileSearcher:
	pattern='profile\.php\?id=[0-9]+'
	opener=None
	linkSearcher=None
	blockList=[]
	debug=False
	def __init__(self, f, pattern):
		self.opener=f
		self.pattern=pattern
		self.debug=False
		self.blockList=[]
	def load (self, url):
		idList=[]
		page=self.opener.open(url)
		iterator=re.compile(self.pattern).finditer(page)
		for match in iterator:
			id_str=match.group()
			id=re.compile('[0-9]+').search(id_str).group()
			if idList.count(id)==0 and self.blockList.count(id)==0:
				idList.append(id)
		if self.debug:
			print ('Found profiles:',idList)
		for id in idList:
			if self.linkSearcher!=None:
				if self.debug:
					print ('Searching in:',id)
				self.linkSearcher.load(id)
class LinkSearcher:
	pattern=''
	opener=None
	callback=None
	blockList=[]
	linkList=[]
	prefix=Facebook.facebookURL+'/profile.php?id='
	suffix=''
	allowDuplicate=False
	linksFound=0
	debug=False
	def __init__(self, f, pattern):
		self.opener=f
		self.pattern=pattern
		self.blockList=[]
		self.linkList=[]
		self.allowDuplicate=False
		self.linksFound=0
		self.debug=False
	def load (self, id):
		self.linksFound=0
		self.linkList=[]
		if self.debug:
			print ('Opening:',self.prefix+id)
		page=self.opener.open(self.prefix+id+self.suffix)
		iterator=re.compile(self.pattern).finditer(page)
		for match in iterator:
			link=match.group()
			if (self.linkList.count(link)==0 or self.allowDuplicate) and self.blockList.count(link)==0:
				self.linkList.append(link)
		if self.debug:
			print ('Found links:', self.linkList)
		self.linksFound=len(self.linkList)
		for link in self.linkList:
			if self.callback!=None:
				#print ('Calling back')
				self.callback.foundLink(link)


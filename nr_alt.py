import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampire.vampire
import FBAuto.crusades.fight
import FBAuto.crusades.earn
import FBAuto.gangsterbattle.earn
import FBAuto.gangsterbattle.fight
import FBAuto.gangsterbattle.property
import FBAuto.knighted.earn
import FBAuto.knighted.fight
import FBAuto.knighted.property
import FBAuto.playsexgames.earn
import FBAuto.playsexgames.fight
import FBAuto.playsexgames.property
import FBAuto.playgangwars.earn
import FBAuto.mafiacities.earn
import FBAuto.mafia.earn
import FBAuto.mobwars.property
import FBAuto.mobwars.earn
import FBAuto.saveourearth.property
import FBAuto.realtimeracing.property
import FBAuto.bandbattle.property
import FBAuto.mightofmany.earn
import FBAuto.mightofmany.fight
import FBAuto.mightofmany.property
import FBAuto.piratesrule.earn
import FBAuto.piratesrule.fight
import FBAuto.piratesrule.property
import FBAuto.dragonwars.earn
import FBAuto.dragonwars.fight
import FBAuto.dragonwars.property
import FBAuto.vampiresgame.common
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.inthemafia.earn
import FBAuto.inthemafia.fight
import FBAuto.inthemafia.property
import FBAuto.spacewarsgame.earn
import FBAuto.spacewarsgame.fight
import FBAuto.spacewarsgame.property
import FBAuto.heroesvillains.earn
import FBAuto.heroesvillains.fight
import FBAuto.heroesvillains.property
import FBAuto.prisonlockdowngame.earn
import FBAuto.prisonlockdowngame.fight
import FBAuto.prisonlockdowngame.property
import FBAuto.gangwarsgame.earn
import FBAuto.gangwarsgame.fight
import FBAuto.gangwarsgame.property
import FBAuto.streetracinggame.common
import FBAuto.streetracinggame.earn
import FBAuto.streetracinggame.fight
import FBAuto.streetracinggame.property
import FBAuto.fashionwarsgame.earn
import FBAuto.fashionwarsgame.fight
import FBAuto.fashionwarsgame.property
import FBAuto.footballwars.earn
import FBAuto.footballwars.fight
import FBAuto.footballwars.property
import FBAuto.gangster_wars.property
import FBAuto.street_fights.property
import FBAuto.truegangsters.inventory
import FBAuto.truegangsters.earn
import FBAuto.truegangsters.fight
import FBAuto.ability.ability2
import FBAuto.refresher.refresher
import FBAuto.treasurehunt.treasurechest
import FBAuto.treasurehunt.egghunt
import FBAuto.treasurehunt.fbfairy
import FBAuto.treasurehunt.scavenge
import FBAuto.treasurehunt.scavengermouse
import FBAuto.treasurehunt.myhome_thing
import FBAuto.pets.hatchingpets
import FBAuto.pets.fluff
import FBAuto.pets.hatchery
import FBAuto.pets.metropolisgame
import FBAuto.pets.zoobuilder
import FBAuto.pets.helptheplanet
import FBAuto.liloceanpatch.sell
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite

def compareGift(g1, g2):
	return cmp(g1[1], g2[1])
if __name__ == "__main__":
	controller=SessionController()
	controller.start()
	accounts=AccountManager()
	senders=[]
	for f in accounts.logins:
			senders.append(SessionController(f))
	#gifts =[['326', 1], ['327', 1], ['328', 1], ['329', 1]]
	#gifts =[['314',1], ['315',1], ['316', 1], ['317', 1]]
	gifts =[['358',9], ['359',10], ['360', 9], ['362', 8]]
	while len(senders)>0 and len(gifts)>0:
		sender=senders[0]
		gifts.sort(compareGift, reverse=True)
		gift_id=gifts[0][0]
		print (sender.opener.email+' is giving gift', gift_id)
		info = sender.openWithHeader(Facebook.appsURL+'/nitrousracing/index.php?c=invitesplash&gift_selected='+gift_id)
		if re.compile('giftmax=1').search(info[0])!=None:
			print ('Sender out:', sender.opener.email)
			senders.pop(0)
			continue
		sender.open(Facebook.appsURL+'/nitrousracing/index.php?c=homecar&giftsent=true', urllib.parse.urlencode({'gift':gift_id, 'ids[]':'622577982'}))
		controller.refresh(Facebook.appsURL+'/nitrousracing/fbaccept.php?creative=1&type=gift&uid='+sender.thisUserID+'&giftitem='+gift_id)
		gifts[0][1]=gifts[0][1]-1
		if gifts[0][1]==0:
			print ('Finished gift:', gift_id)
			gifts.pop(0)
	print ('Finished sending')

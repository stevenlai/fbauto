from FBAuto.common.facebook import Facebook
import FBAuto.common.gangsterbattle_common

appID='playsexgames'
config = FBAuto.common.gangsterbattle_common.GameConfig()
config.appID=appID
config.energyPrefix='Mojo:[ ]*'
config.energyMidfix='/'
config.healthPrefix='Self-Respect:[ ]*'
config.healthMidfix='/'
config.staminaPrefix='Stamina:[ ]*'
config.staminaMidfix='/'
config.cashPrefix='<span>(Cash:[ ]*)*\$'
config.bankPrefix='<h1>\$'
config.propertyPrefix='Owned: <b>'
config.jailstr='Your social isolation'
config.itemURL=Facebook.appsURL+'/'+appID+'/arsenal.php?page=items'
config.stayInHospital=False
config.minDepositAmount=25
config.energyRechargeTime=289
if __name__ == "__main__":
	f = Facebook('facebook')

	p=FBAuto.common.gangsterbattle_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()

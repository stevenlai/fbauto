import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from .ProfileLoader import *

class MobBountyParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	hitCount=0
	html=''
	bonty_id=''

	def __init__(self, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.hitCount=0
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action='/mobwars/fight/do.php'
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name=='bounty_id':
				self.bonty_id=value
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Ready to hit:',self.bonty_id)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.html=self.opener.open(Facebook.appsURL+'/mobwars/fight/do.php', input)
			self.header=[]
			self.begin=False
			self.hitCount=self.hitCount+1

class MobWarsBounty(threading.Thread):
	opener=None
	profile=None
	def __init__(self, f):
		threading.Thread.__init__(self)
		self.opener=f
		self.profile=ProfileLoader(f)
	def hit (self):
		print ('Checking hit list (Mob Wars) ...')
		page = Facebook.appsURL+'/mobwars/hitlist/'
		html=self.opener.open(page)
		while self.profile.health>=30 and self.profile.stamina>0:
			ap = MobBountyParser(self.opener)
			ap.feed(html)
			ap.close()
			html=ap.html
			print ('Hit list count:',ap.hitCount)
			if ap.hitCount==0:
				break
			else:
				self.profile.loadProfile()
				if self.profile.cash!='0':
					self.deposit()
	def deposit (self):
		print ('Depositing (Mob Wars) ...')
		page = Facebook.appsURL+'/mobwars/bank/'
		ap = MobBankParser(self.opener)
		ap.feed(self.opener.open(page))
		ap.close()
	def doJob(self):
		self.profile.loadProfile()
		self.hit()
		self.profile.loadProfile()
		if not self.profile.jailed and self.profile.cash!='0':
			self.deposit()
	def run (self):
		sleepTime=20*60
		while True:
			try:
				self.opener.login()
				self.doJob()
				if not self.opener.logout():
					sleepTime=2
				else:
					sleepTime=20*60
			except:
				print ("Something went wrong:", sys.exc_info()[0], "Trackback:")
				traceback.print_exc(file=sys.stdout)
				sleepTime=2
			print ('Going to sleep. Sleep time:', sleepTime)
			time.sleep(random.randint(sleepTime, sleepTime+60))
if __name__ == "__main__":
	f = Facebook('mobwars.lwp')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		mobwars = MobWars(f)
		mobwars.doJob()
		f.logout()
	else:
		mobbonty = MobWarsBounty(f)
		mobbonty.start()

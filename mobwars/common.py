from FBAuto.common.mightofmany_common import *

appID='mobwars'
config = GameConfig()
config.appID=appID
config.energyPrefix='<[^<>]*cur_energy[^<>]*>'
config.healthPrefix='<[^<>]*cur_health[^<>]*>'
config.staminaPrefix='<[^<>]*cur_recovery[^<>]*>'
config.cashPrefix='<[^<>]*cur_cash[^<>]*>\$'
config.itemPrefix='Owned:[ ]*<[^<>]*cur_item_qty[^<>]*>'
config.bankPrefix='Account Balance: <span>\$'
config.propertyPrefix='Owned: <[^>]*>'
config.energyMidfix='</span>/'
config.healthMidfix='</span>/'
config.staminaMidfix='</span>/'
config.jailstr='Jail'
config.jobDelay=600
config.deposit=False

config.bankURL='bank'
config.propertyURL='city'
config.bountyURL='hitlist'
config.jobURL='jobs'
config.weaponURL='stockpile'
config.jailURL='jail'
config.energyRechargeTime=300

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True

	might = ProfileLoader(f, config)
	might.loadProfile()
	might.displayProfile()

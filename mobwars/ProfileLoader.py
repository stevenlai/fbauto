import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class MobBankParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	cash=''

	def __init__(self, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form' and attrs[0][1]=='do.php':
			self.begin=True
		elif self.begin and tag=='input' and attrs[0][1]=='hidden' and attrs[1][1]!='action':
			self.header.append((attrs[1][1], attrs[2][1]))
		elif self.begin and tag=='input' and attrs[0][1]=='text' and attrs[1][1]=='deposit_amount':
			self.header.append((attrs[1][1], attrs[2][1]))
			self.cash=attrs[2][1]
		elif self.begin and tag=='input' and attrs[0][1]=='hidden' and attrs[1][1]=='action' and attrs[2][1]=='deposit':
			self.header.append((attrs[1][1], attrs[2][1]))
		elif self.begin and tag=='input' and attrs[0][1]=='hidden' and attrs[1][1]=='action' and attrs[2][1]!='deposit':
			self.begin=False
			self.header=[]

	def handle_endtag(self, tag):
		if self.begin and tag=='form' and self.cash!='0':
			print ('Depositing amount:',self.cash)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/mobwars/bank/do.php', input)
			self.begin=False
			self.header=[]

class ProfileLoader:
	energy=0
	health=0
	stamina=0
	cash=''
	opener=None
	jailed=False
	page=''
	def __init__(self, f):
		self.opener=f
		self.energy=0
		self.health=0
		self.stamina=0
		self.cash=''
		self.jailed=False
		self.page=''
	def loadPage(self):
		self.page=self.opener.open(Facebook.appsURL+'/mobwars/')
		while re.compile("Energy:[ ]*[0-9]+/").search(self.page)==None:
			print ('Error loading the profile! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			self.page=self.opener.open(Facebook.appsURL+'/mobwars/')
	def loadProfile (self):
		self.loadPage()
		m=re.compile("Energy:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.energy=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Health:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.health=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Stamina:[ ]*[0-9]+/").search(self.page)
		if m!=None:
			self.stamina=int(re.compile('[0-9]+').search(m.group()).group())

		m=re.compile("Cash:[ ]*\$[0-9,]+").search(self.page)
		if m!=None:
			self.cash=re.compile('[0-9,]+').search(m.group()).group()

		m=re.compile("Jail").search(self.page)
		if m!=None:
			self.jailed=True
		else:
			self.jailed=False

		print ('Profile loaded. Energy:',self.energy,'Health:',self.health,'Stamina:',self.stamina,'Cash:',self.cash,'Jailed:',self.jailed)


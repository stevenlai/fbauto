from FBAuto.common.facebook import Facebook
import FBAuto.common.streetfights_common

appID='gangster-wars'
config = FBAuto.common.streetfights_common.GameConfig()
config.appID=appID
config.energyPrefix='EN:[ ]*'
config.energyMidfix='/'
config.healthPrefix='HP:[ ]*'
config.healthMidfix='/'
config.staminaPrefix='STA:[ ]*'
config.staminaMidfix='/'
config.levelPrefix='Level:[ ]*'
config.cashPrefix='Cash:[ ]*<[^<>]*>\$'
config.bankPrefix='My bank account:[ ]*<[^<>]*>\$'
config.propertyPrefix='You have '
config.minDepositAmount=100
config.energyRechargeTime=300

if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.streetfights_common.ProfileLoader(f, config)
	f.login()
	p.loadProfile()
	p.displayProfile()
	f.logout()

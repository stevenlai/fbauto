from FBAuto.common.facebook import Facebook
import FBAuto.common.zynga_common

appID='spacewarsgame'
config = FBAuto.common.zynga_common.GameConfig()
config.appID=appID
config.energyPrefix='Energy:.*<strong><span id="app[0-9]+_current_energy" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.healthPrefix='Shields:.*<strong><span id="app[0-9]+_current_health" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.staminaPrefix='Munitions:.*<strong><span id="app[0-9]+_current_stamina" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.cashPrefix='Credits:.*<strong class="money">[ ]*\$'
config.bankPrefix='Account Balance:.*<b class="money">\$'
config.propertyPrefix='Owned:[^0-9]+'
config.memberPrefix='my fleet[ ]*\('
config.fightFriend=True
#config.doVendetta=True
config.healThreshold=0.5
#config.fightFromFeed=True
config.useEnergyPack=False
config.bountyLevel=10
config.doFight=True
config.feedPrefix='Heads up! You were challenged by[ ]*'
config.winMsg='You WON the battle'
config.jailstr='Jail'
config.energyRechargeTime=200

if __name__ == "__main__":
	f = Facebook('facebook')
	f.login()
	p=FBAuto.common.zynga_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()
	f.logout()

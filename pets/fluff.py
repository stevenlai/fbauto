import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.treasurehunt.ProfileSearcher import *

appID='fluff'
searchingDepth=0
class PetParser(HTMLParser):
	begin=False
	header=[]
	input=''
	petID=''

	def __init__(self, petID):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.petID=petID
		self.input=''
		self.jobDescription='Fluff friends'
	def handle_starttag(self, tag, attrs):
		if tag=='form' and not self.begin:
			for a in attrs:
				if a[0]=='id' and re.compile('app[0-9]+_pet_friend').search(a[1])!=None:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			self.input = urllib.parse.urlencode(self.header)
			self.begin=False

class JobMain(JobRunner):
	opener=None
	maxDepth=100
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.jobDescription='Pet'
	def foundLink(self, link):
		global searchingDepth
		ownerID=re.compile('[0-9]+').search(link).group()

		print ('Petting: '+ownerID+'s pets. Depth:',searchingDepth)
		html=self.opener.open(Facebook.appsURL+'/'+appID+'/'+link)
		tp=PetParser(ownerID)
		tp.feed(html)
		try:
			tp.close()
		except:
			#Used to log the data here!!!
			pass
		#print (tp.input)
		html=self.opener.open(Facebook.appsURL+'/'+appID+'/'+link, tp.input)
		time.sleep(random.randint(10, 70))

		#Recursive Search
		if searchingDepth<=self.maxDepth:
			links = LinkSearcher(self.opener, 'fluffbook\.php\?id=[0-9]+')
			links.callback=None
			links.blockList=['fluffbook.php?id='+ownerID, 'fluffbook.php?id='+self.opener.thisUserID]
			links.prefix=Facebook.appsURL+'/'+appID+'/'+'fluffbook.php?id='+ownerID
			searchingDepth=searchingDepth+1
			links.load('')
			if len(links.linkList)>0:
				self.foundLink(links.linkList[random.randint(0, len(links.linkList)-1)])
			searchingDepth=searchingDepth-1
	def doJob(self):
		global searchingDepth
		searchingDepth=0
		links = LinkSearcher(self.opener, 'fluffbook\.php\?id=[0-9]+')
		links.callback=None
		links.blockList=['fluffbook.php?id='+self.opener.thisUserID]
		links.prefix=Facebook.appsURL+'/'+appID+'/'+'fluffbook.php?id='+self.opener.thisUserID
		#links.prefix=Facebook.appsURL+'/'+appID+'/'+'fluffbook.php?id='+'745873407'
		links.load('')
		if len(links.linkList)>0:
			self.foundLink(links.linkList[random.randint(0, len(links.linkList)-1)])

		self.sleepTime=[30*60, 60*60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = JobMain(f)
		job.doJob()
		f.logout()
	else:
		#f.rememberPass=True
		job = JobMain(f)
		job.start()



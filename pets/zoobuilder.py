import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
import FBAuto.common.metropolisgame_common

appID='zoobuilder'
config = FBAuto.common.metropolisgame_common.GameConfig()
config.appID=appID
config.appNum='36533109268'
config.entertainStr='Send Happiness'

class JobMain(FBAuto.common.metropolisgame_common.JobMain):
	game_config=config

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = JobMain(f)
		#job.scheduled_url='connections.php?show=members'
		#job.alwaysGiveYoung=True
		job.doJob()
	else:
		#f.rememberPass=True
		job = JobMain(f)
		job.start()

import sys
import getopt
import time
import random
from FBAuto.common.facebook import Facebook
import FBAuto.common.helptheplanet_common

appID='helptheplanet'
config = FBAuto.common.helptheplanet_common.GameConfig()
config.appID=appID
config.cashPrefix='<[^>]*revenuebreakdown\.php[^>]*><b>\$'
config.supportPrefix='<td style="text-align: right;">'
config.supportMidfix=' / '
config.requiredSupport=250
config.buy1=['14', 5]
config.buy2=['12', 10]

class JobMain(FBAuto.common.helptheplanet_common.JobMain):
	game_config=config
class BuildMain(FBAuto.common.helptheplanet_common.BuildMain):
	game_config=config

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		job = BuildMain(f)
		job.doJob()
	else:
		#f.rememberPass=True
		job = JobMain(f)
		job.start()


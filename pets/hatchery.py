import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.treasurehunt.ProfileSearcher import *

appID='hatchery'
searchingDepth=0
class Petter:
	opener=None
	def __init__(self, f):
		self.opener=f
	def foundLink(self, link):
		decoded_link = link.replace('&amp;', '&')
		print ('\tPetting: '+link)
		html=self.opener.open(Facebook.appsURL+'/'+appID+'/'+decoded_link)
class JobMain(JobRunner):
	opener=None
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.jobDescription='Hatchery'
	def foundLink(self, link):
		ownerID=re.compile('[0-9]+').search(link).group()

		print ('Petting: '+ownerID+'s pets.')
		p=Petter(self.opener)
		links = LinkSearcher(self.opener, 'players\.php\?id=[0-9]+&amp;action=[a-zA-Z]+&amp;petid=[0-9]+')
		links.callback=p
		links.prefix=Facebook.appsURL+'/'+appID+'/'+link
		links.load('')
	def doJob(self):
		links = LinkSearcher(self.opener, 'players\.php\?id=[0-9]+')
		links.callback=self
		links.prefix=Facebook.appsURL+'/'+appID+'/'+'requests.php'
		links.load('')

		self.sleepTime=[60*60, 120*60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		job = JobMain(f)
		job.doJob()
		f.logout()
	else:
		#f.rememberPass=True
		job = JobMain(f)
		job.start()



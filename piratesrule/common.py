from FBAuto.common.facebook import Facebook
import FBAuto.common.zynga_common

appID='piratesrule'
config = FBAuto.common.zynga_common.GameConfig()
config.appID=appID
config.energyPrefix='<[^>]*current_energy[^>]*>[ ]*'
config.healthPrefix='<[^>]*current_health[^>]*>[ ]*'
config.staminaPrefix='<[^>]*current_stamina[^>]*>[ ]*'
config.levelPrefix='<[^>]*icon-level[^>]*><[^>]*stats_info_box[^>]*>'
config.cashPrefix='<[^>]*stats_info_box[^>]*><[^>]*icon-blood[^>]*>[ ]*'
config.bankPrefix='Account Balance:.*<b class="money"><img src="[^>]*>[ ]*'
config.memberPrefix='Crew <[^>]*>\('
config.propertyPrefix='Owned:[^0-9]+'
config.feedPrefix='Arrr! Ye were attacked by[ ]*'
config.winMsg='Ye WON the fight'
#config.fightFromFeed=True
config.bountyLevel=50
config.healThreshold=0.4
#config.doFight=True
config.fightFriend=True
config.useEnergyPack=False
config.jailstr='Jail'
config.energyRechargeTime=60
config.staminaRechargeTime=150

if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.zynga_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()

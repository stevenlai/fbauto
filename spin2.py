import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.vampiresgame.common
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite

if __name__ == "__main__":
	accounts=AccountManager()
	acct_idx=0

	current = SessionController(accounts.logins[acct_idx])
	print ('Current account:', accounts.logins[acct_idx].email)
	command = sys.stdin.read(1)
	while command!='q':
		if command=='s':
                        print ('Command received, ready to spin')
			spinner = FBAuto.vampiresgame.earn.TreasureMain(current)
			spinner.repeat=100
			t = spinner.getWaitingTime()
			if t==0 or t==3600*4:
				spinner.spin()
			acct_idx=(acct_idx+1)%len(accounts.logins)
			current = SessionController(accounts.logins[acct_idx])
			print ('Current account:', accounts.logins[acct_idx].email)
		elif command=='n':
			acct_idx=(acct_idx+1)%len(accounts.logins)
			current = SessionController(accounts.logins[acct_idx])
			print ('Current account:', accounts.logins[acct_idx].email)
		elif command=='b':
			if acct_idx>0:
				acct_idx=acct_idx-1
			else:
				acct_idx=len(accounts.logins)-1
			current = SessionController(accounts.logins[acct_idx])
			print ('Current account:', accounts.logins[acct_idx].email)
		elif command=='v':
			print ('Command received, ready to visit links')
			f = open(current.getConfigFile('vw_urls.txt'), 'r')
			url = f.readline().strip('\n')
			while len(url)>0:
				current.refresh(url)
				url=f.readline().strip('\n')
		else:
			pass
		command = sys.stdin.read(1)

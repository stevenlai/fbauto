from FBAuto.common.facebook import Facebook
import FBAuto.common.mafiacities_common

appID='mafiacities'
config = FBAuto.common.mafiacities_common.GameConfig()
config.appID=appID
config.appNum='18844996141'
config.energyPrefix='<span id="app[0-9]+_energy" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.healthPrefix='<span id="app[0-9]+_health" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.staminaPrefix='<span id="app[0-9]+_stamina" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.cashPrefix='<span id="app[0-9]+_cash" fbcontext="[0-9a-zA-Z]+">[ ]*'
config.jailstr='You are in jail'
config.energyRechargeTime=120

if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.mafiacities_common.ProfileLoader(f, config)
	f.login()
	p.loadProfile()
	p.displayProfile()
	p.deposit()
	f.logout()

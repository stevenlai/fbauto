import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *

appID='mafiacities'
class ProfileLoader:
	cash=0
	health=0
	healthMax=0
	energy=0
	energyMax=0
	stamina=0
	staminaMax=0
	jailed=False
	opener=None
	def __init__(self, opener):
		self.cash=0
		self.health=0
		self.healthMax=0
		self.energy=0
		self.energyMax=0
		self.stamina=0
		self.staminaMax=0
		self.jailed=False
		self.opener=opener
	def loadProfile (self):
		page=self.opener.open(Facebook.appsURL+'/'+appID+'/')
		iterator=re.compile("[0-9,]+</span>/[0-9,]+").finditer(page)
		i=0
		for match in iterator:
			m=re.compile("[0-9,]+</span>").search(match.group())
			if m!=None:
				data=int(re.compile("[0-9,]+").search(m.group()).group().replace(',',''))
				if i==0:
					self.health=data
				elif i==1:
					self.energy=data
				elif i==2:
					self.stamina=data
			m=re.compile("/[0-9,]+").search(match.group())
			if m!=None:
				data=int(re.compile("[0-9,]+").search(m.group()).group().replace(',',''))
				if i==0:
					self.healthMax=data
				elif i==1:
					self.energyMax=data
				elif i==2:
					self.staminaMax=data
			i=i+1
		m=re.compile('<td[^>]*>\$[0-9,]+').search(page)
		if m!=None:
			cashstr=re.compile("\$[0-9,]+").search(m.group()).group()
			data=int(re.compile("[0-9,]+").search(cashstr).group().replace(',',''))
			self.cash=data
		if re.compile('You are in jail').search(page)!=None:
			self.jailed=True
		else:
			self.jailed=False
		self.display()
	def display(self):
		print ('Cash:', self.cash)
		print ('Health:', self.health, '/', self.healthMax)
		print ('Energy:', self.energy, '/', self.energyMax)
		print ('Stamina:', self.stamina, '/', self.staminaMax)
		print ('In jail:', self.jailed)

class BankParser(HTMLParser):
	begin=False
	header=[]
	opener=None
	amount=''

	def __init__(self, opener, amount):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
		self.amount=amount
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action='bank.php?action=deposit'
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name=='howmuch':
				value=self.amount
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/'+appID+'/bank.php?action=deposit', input)
			self.begin=False
			self.header=[]

class JobParser(HTMLParser):
	jobID=''
	begin=False
	header=[]
	opener=None
	html=''

	def __init__(self, opener, jobID):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
		self.jobID=jobID
		self.html=''
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action='jobs.php?action=do&id='+self.jobID
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			if name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Ready to do job:', self.jobID)
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.html=self.opener.open(Facebook.appsURL+'/'+appID+'/jobs.php?action=do&id='+self.jobID, input)
			self.begin=False

class MafiaCities(JobRunner):
	opener=None
	profile=None
	counter=0
	breakoutEnergy=5
	job1Failed=False
	job2Failed=False
	#jobList=[(2, 1, 'index.php?action=offer&id=1', None), (2, 1, 'index.php?action=offer&id=2', None), (1, 1, 'index.php?action=collect&id=2', None)]
	jobList=[]
	#No local job
	jobList2=[]
	#Get the paiting
	#jobList2=[(10, 1, '12', None)]
	#Sell the painting
	#jobList2=[(2, 2, '14', 'Old Painting', 1)]
	#General jobs
	jobList3=[(15, 1, '5', None)]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.profile=ProfileLoader(self.opener)
		self.appID=appID
		self.jobDescription='Earn'
	def getRequiredEnergy(self):
		total=0
		if not self.job1Failed:
			for j in self.jobList:
				total=total+j[0]*j[1]
		elif not self.job2Failed and len(self.jobList2)>0:
			for j in self.jobList2:
				total=total+j[0]*j[1]
		else:
			for j in self.jobList3:
				total=total+j[0]*j[1]
		return total
	def canDoJob2 (self):
		for j in self.jobList2:
			if j[3]!=None and len(j)>4:
				page=self.opener.open(Facebook.appsURL+'/'+appID+'/stuff.php')
				m=re.compile(j[3]+'[ ]*x[ ]*[0-9]+').search(page)
				if m==None:
					return False
				else:
					item_no=int(re.compile('[0-9]+').search(m.group()).group())
					if item_no<j[4]:
						return False
		print ('Can do job 2')
		return True
	def __earn (self, job_id):
		
	def earn (self):
		print ('Earning (Mafia Cities) ...')
		self.job1Failed=False
		self.job2Failed=False
		for j in self.jobList:
			for i in range(0, j[1]):
				print ('Earning:',j[2])
				page = Facebook.appsURL+'/'+appID+'/'+j[2]
				html=self.opener.open(page)
				if re.compile('The Law chased you off').search(html)!=None:
					self.job1Failed=True
				if self.job1Failed:
					break
			if self.job1Failed:
				break
		if self.job1Failed and len(self.jobList2)>0 and self.canDoJob2():
			print ('Job1 failed, doing job 2')
			for j in self.jobList2:
				for i in range(0, j[1]):
					print ('Earning:', j[2])
					self.__earn(j[2])
					job=JobParser(self.opener, j[2])
					job.feed(self.opener.open(Facebook.appsURL+'/'+appID+'/jobs.php'))
					job.close()
					if re.compile('You do not meet the requirements of the job').search(job.html)!=None:
						self.job2Failed=True
					if self.job2Failed:
						break
				if self.job2Failed:
					break
		if self.job1Failed and (self.job2Failed or len(self.jobList2)==0):
			print ('Job1 & job2 failed, doing job 3')
			for j in self.jobList3:
				for i in range(0, j[1]):
					print ('Earning:', j[2])
					job=JobParser(self.opener, j[2])
					job.feed(self.opener.open(Facebook.appsURL+'/'+appID+'/jobs.php'))
					job.close()

	def deposit (self):
		if self.profile.cash>0:
			print ('Depositing (Mafia Cities):', self.profile.cash)
			page = Facebook.appsURL+'/'+appID+'/bank.php'
			ap = BankParser(self.opener, str(self.profile.cash))
			ap.feed(self.opener.open(page))
			ap.close()
	def breakout (self):
		while self.profile.energy>=self.breakoutEnergy and self.profile.jailed:
			print ('Trying to break out of jail!')
			self.opener.open(Facebook.appsURL+'/'+appID+'/index.php?action=breakout')
			self.profile.loadProfile()
	def readJobList(self):
		self.jobList = self.loadJob(appID+'1.txt')
		self.jobList2 = self.loadJob(appID+'2.txt')
		self.jobList3 = self.loadJob(appID+'3.txt')
		print ('JobList1:', self.jobList)
		print ('JobList2:', self.jobList2)
		print ('JobList3:', self.jobList3)
	def loadJob(self, filename):
		jList=[]
		d = os.getcwd()
		if re.compile(appID).search(d)==None:
			d=os.path.join(d, 'config', filename)
		else:
			d=os.path.join(d, '..', 'config', filename)
		f = open(d, 'r')
		iterator=re.compile('[a-z0-9A-Z?.=& ]+').findall(f.readline())
		while len(iterator)>0:
			i=0
			for jobInfo in iterator:
				if i==0:
					energy=int(jobInfo)
				elif i==1:
					count=int(jobInfo)
				elif i==2:
					id=jobInfo
				elif i==3:
					if jobInfo=='None':
						prereq=None
					else:
						prereq=jobInfo
				elif i==4:
					prereq_count=int(jobInfo)
				i=i+1
			if prereq==None:
				jList.append((energy, count, id, prereq))
			else:
				jList.append((energy, count, id, prereq, prereq_count))
			iterator=re.compile('[a-z0-9A-Z?.=& ]+').findall(f.readline())
		#print ('Job list:', jList)
		f.close()
		return jList
	def doJob(self):
		self.readJobList()
		self.profile.loadProfile()
		energy=self.profile.energy
		if self.getRequiredEnergy()<=energy and not self.profile.jailed:
			self.earn()
			self.profile.loadProfile()
			if self.profile.energy<energy and not self.profile.jailed:
				self.deposit()
				sleepTime=(self.getRequiredEnergy()-self.profile.energy)*600
			elif self.profile.jailed:
				self.breakout()
				sleepTime=(self.breakoutEnergy-self.profile.energy)*600
			else:
				sleepTime=2
		elif self.profile.jailed:
			self.breakout()
			sleepTime=(self.breakoutEnergy-self.profile.energy)*600
		else:
			#Not enough energy for the job
			self.deposit()
			sleepTime=(self.getRequiredEnergy()-energy)*600

		if sleepTime<0:
			sleepTime=0
		if not self.job1Failed and sleepTime<30*60:
			sleepTime=30*60
		self.sleepTime=[sleepTime, sleepTime+60]

if __name__ == "__main__":
	f = Facebook(appID+'.lwp')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		mafiacities = MafiaCities(f)
		mafiacities.doJob()
		f.logout()
	else:
		mafiacities = MafiaCities(f)
		mafiacities.start()

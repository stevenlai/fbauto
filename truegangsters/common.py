from FBAuto.common.facebook import Facebook
import FBAuto.common.prisonlockdown_common

appID='truegangsters'
config = FBAuto.common.prisonlockdown_common.GameConfig()
config.appID=appID
config.gangURL='viewgang'
config.individualGang='upgrademember&amp;who=[0-9]+'
config.energyPrefix='Energy:<[^<>]*><[^<>]*>[ ]*'
config.memberPrefix='Your Gang \('
config.cashPrefix='Money:<[^<>]*><[^<>]*>\$'
config.ownedSuffix='<[^<>]*><[^<>]*>Owned'
config.pricePrefix='Price: <[^<>]*>\$'
config.buying_action1='upgrademember&amp;who='
config.buying_action2='&amp;buy=1&amp;weapon='
config.amountName='amount'
config.ajax={'type':'2', 'require_login':'true'}
config.inventoryList=["", "Tattoo", "Brass Knuckles", "Knife", "Rags", "Hand Gun", "Motorcycle", "SUV"]
config.targetList=[0, 3000, 3000, 3000, 3000, 3000, 3000, 3000]
#config.targetList=[0, 20, 14, 40, 40, 16, 25, 16]

if __name__ == "__main__":
	f = Facebook('facebook')
	
	p=FBAuto.common.prisonlockdown_common.ProfileLoader(f, config)
	f.login()
	p.loadProfile()
	p.displayProfile()
	f.logout()

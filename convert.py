import re
import sys

if __name__ == "__main__":
	f = open(sys.argv[1], 'r')
	tmpStr = f.read()
	f.close()

	changed=False
	iter = re.compile(' ([^ ]+)\.has_key\(([^\)]+)\)').finditer(tmpStr)
	for m in iter:
		tmpStr=tmpStr.replace(m.group(), ' '+m.group(2)+' in '+m.group(1))
		changed=True

	if changed:
		print ('Converting', sys.argv[1])
		#print (tmpStr)
		f = open(sys.argv[1], 'w')
		f.write(tmpStr)
		f.close()

from FBAuto.common.mightofmany_common import *

appID='mightofmany'
config = GameConfig()
config.appID=appID
config.energyPrefix='Stamina</b>:<[^>]*><[^>]*>[ ]*\n[ ]*<[^>]*>[ ]*'
config.healthPrefix='Life</b>:<[^>]*><[^>]*>[ ]*\n[ ]*<[^>]*>[ ]*'
config.staminaPrefix='Battle Will</b>:<[^>]*><[^>]*>[ ]*\n[ ]*<[^>]*>[ ]*'
config.cashPrefix='Gold</b>:[ ]*'
config.bankPrefix='Gold Balance: <span>'
config.propertyPrefix='Owned: <[^>]*>'
config.jailstr='Dungeon Menu'

config.bankURL='vault'
config.propertyURL='empire'
config.bountyURL='bounty'
config.jobURL='quests'
config.weaponURL='armoury'
config.jailURL='dungeon'
config.energyRechargeTime=300

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True

	might = ProfileLoader(f, config)
	might.loadProfile()
	might.displayProfile()

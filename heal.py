import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.piratesrule.earn
import FBAuto.piratesrule.fight
import FBAuto.piratesrule.property
import FBAuto.dragonwars.earn
import FBAuto.dragonwars.fight
import FBAuto.dragonwars.property
import FBAuto.vampiresgame.earn
import FBAuto.vampiresgame.fight
import FBAuto.vampiresgame.property
import FBAuto.spacewarsgame.earn
import FBAuto.spacewarsgame.fight
import FBAuto.spacewarsgame.property
import FBAuto.heroesvillains.earn
import FBAuto.heroesvillains.fight
import FBAuto.heroesvillains.property
import FBAuto.prisonlockdowngame.earn
import FBAuto.prisonlockdowngame.fight
import FBAuto.prisonlockdowngame.property
import FBAuto.gangwarsgame.earn
import FBAuto.gangwarsgame.fight
import FBAuto.gangwarsgame.property
import FBAuto.fashionwarsgame.earn
import FBAuto.fashionwarsgame.fight
import FBAuto.fashionwarsgame.property
import FBAuto.footballwars.earn
import FBAuto.footballwars.fight
import FBAuto.footballwars.property
import FBAuto.refresher.refresher
import FBAuto.common.autoaccept
import FBAuto.common.autoinvite
import FBAuto.common.autoadd

if __name__ == "__main__":
	controller=SessionController()
	controller.start()
	time.sleep(random.randint(20, 60))
	#Critical tasks
	#vampiresgame_heal = FBAuto.vampiresgame.fight.HealMain(controller)
	#vampiresgame_heal.start()
	#time.sleep(random.randint(1, 3))
	dragonwars_heal = FBAuto.dragonwars.fight.HealMain(controller)
	dragonwars_heal.start()
	time.sleep(random.randint(1, 3))
	heroesvillains_heal = FBAuto.heroesvillains.fight.HealMain(controller)
	heroesvillains_heal.start()
	time.sleep(random.randint(1, 3))
	spacewarsgame_heal = FBAuto.spacewarsgame.fight.HealMain(controller)
	spacewarsgame_heal.start()
	time.sleep(random.randint(1, 3))
	pirate_heal = FBAuto.piratesrule.fight.HealMain(controller)
	pirate_heal.start()
	time.sleep(random.randint(1, 3))


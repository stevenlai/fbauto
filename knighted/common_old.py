import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

appID='knighted'
class BankParser(HTMLParser):
	begin=False
	header=[]
	minAmount=50
	opener=None

	def __init__(self, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action='accounts.php'
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]

			if name=='action' and value!='deposit':
				self.header=[]
				self.begin=False
			elif name!='':
				self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/'+appID+'/accounts.php', input)
			self.begin=False
			self.header=[]


class ProfileLoader:
	stamina=0
	health=0
	energy=0
	jailed=False
	cash=0
	opener=None
	page=''
	energyPrefix='Endurance:[ ]*'
	energySuffix=''
	healthPrefix='Life:[ ]*'
	healthSuffix=''
	staminaPrefix='Strength:[ ]*'
	staminaSuffix=''
	cashPrefix='Gold:[ ]*'
	cashSuffix=''
	jailstr='stretch in the stockade'
	def __init__(self, f):
		self.opener=f
		self.strength=0
		self.life=0
		self.endurance=0
		self.prisoned=False
		self.cash=0
		self.page=''
	def loadPage(self):
		self.page=self.opener.open(Facebook.appsURL+'/'+appID+'/main.php')
		while re.compile(self.energyPrefix+"[0-9]+"+self.energySuffix).search(self.page)==None:
			print ('Error loading the profile! Redo!')
			time.sleep(random.randint(0, 60))
			self.opener.login()
			self.page=self.opener.open(Facebook.appsURL+'/'+appID+'/main.php')
	def loadProfile (self):
		self.loadPage()
		m=re.compile(self.energyPrefix+"[0-9,]+"+self.energySuffix).search(self.page)
		if m!=None:
			energystr=re.compile('[0-9,]+').search(re.compile('[0-9,]+'+self.energySuffix).search(m.group()).group()).group()
			self.energy=int(energystr.replace(',', ''))

		m=re.compile(self.healthPrefix+"[0-9,]+"+self.healthSuffix).search(self.page)
		if m!=None:
			healthstr=re.compile('[0-9,]+').search(re.compile('[0-9,]+'+self.healthSuffix).search(m.group()).group()).group()
			self.health=int(healthstr.replace(',', ''))

		m=re.compile(self.staminaPrefix+"[0-9,]+"+self.staminaSuffix).search(self.page)
		if m!=None:
			staminastr=re.compile('[0-9,]+').search(re.compile('[0-9,]+'+self.staminaSuffix).search(m.group()).group()).group()
			self.stamina=int(staminastr.replace(',', ''))

		m=re.compile(self.cashPrefix+"[0-9,]+"+self.cashSuffix).search(self.page)
		if m!=None:
			cashstr=re.compile('[0-9,]+').search(re.compile('[0-9,]+'+self.cashSuffix).search(m.group()).group()).group()
			self.cash=int(cashstr.replace(',', ''))

		if len(re.compile(self.jailstr).findall(self.page))>1:
			self.jailed=True
		else:
			self.jailed=False

		print ('Profile loaded. '+self.energyPrefix+':',self.energy,self.healthPrefix+':',self.health,self.staminaPrefix+':',self.stamina,self.cashPrefix+':',self.cash,self.jailstr+':',self.jailed)

if __name__ == "__main__":
	f = Facebook(appID+'.lwp')
	f.login()
	p=ProfileLoader(f)
	p.loadProfile()
	f.logout()

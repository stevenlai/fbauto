from FBAuto.common.facebook import Facebook
import FBAuto.common.gangsterbattle_common

appID='knighted'
config = FBAuto.common.gangsterbattle_common.GameConfig()
config.appID=appID
config.energyPrefix='Endurance:[ ]*'
config.energyMidfix='/'
config.healthPrefix='Life:[ ]*'
config.healthMidfix='/'
config.staminaPrefix='Strength:[ ]*'
config.staminaMidfix='/'
config.cashPrefix='<span>(Gold:[ ]*)*'
config.bankPrefix='<h1>[ ]*'
config.propertyPrefix='Owned: <b>'
config.jailstr='stretch in the stockade'
config.itemURL=Facebook.appsURL+'/'+appID+'/arsenal.php?page=arms'
config.stayInHospital=False
config.heal=True
config.minDepositAmount=50
config.energyRechargeTime=289

if __name__ == "__main__":
	f = Facebook('facebook')
	f.login()
	p=FBAuto.common.gangsterbattle_common.ProfileLoader(f, config)
	p.loadProfile()
	p.displayProfile()
	f.logout()

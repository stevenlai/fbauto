import re
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class ProfileParser(HTMLParser):
	#HP/EP related
	hpFull=0
	hpReal=0
	epFull=0
	epReal=0
	hpdetector=0
	detectingHP=False

	#Status related
	positiveStatus=[]
	negativeStatus=[]
	statusDetector=0
	detectingPositiveStatus=False
	lastStatus=None

	#Calculation related constants (might need adjustment when used in FFA!)
	ep_recover_bound=0.99
	#For NPC
	ep_lower_bound=0.2
	#For FFA
	#ep_lower_bound=0.4
	hp_threshold=0.95
	def __init__(self):
		HTMLParser.__init__(self)
		#HP/EP related
		self.hpFull=0
		self.hpReal=0
		self.epFull=0
		self.epReal=0
		self.hpdetector=0
		self.detectingHP=False

		#Status related
		self.positiveStatus=[]
		self.negativeStatus=[]
		self.statusDetector=0
		self.detectingPositiveStatus=False
		self.lastStatus=None
	def handle_starttag(self, tag, attrs):
		if tag=='div' and len(attrs)>0 and attrs[0]==('class', 'hpColor'):
			#Ready for real hp
			self.hpdetector=1
			self.detectingHP=True
		if tag=='div' and len(attrs)>0 and attrs[0]==('class', 'epColor'):
			#Ready for real ep
			self.hpdetector=1
			self.detectingHP=False

		if tag=='span' and len(attrs)>0 and attrs[0]==('class', 'desirableStatus'):
			#Ready for status name
			self.statusDetector=1
			self.detectingPositiveStatus=True
		if tag=='span' and len(attrs)>0 and attrs[0]==('class', 'undesirableStatus'):
			#Ready for status name
			self.statusDetector=1
			self.detectingPositiveStatus=False
	def handle_data (self, data):
		if self.hpdetector==1:
			if self.detectingHP:
				self.hpReal=int(data)
			else:
				self.epReal=int(data)
			self.hpdetector=2
		elif self.hpdetector==2 and data==' / ':
			#Ready for full hp/ep
			self.hpdetector=3
		elif self.hpdetector==3:
			if self.detectingHP:
				self.hpFull=int(data)
			else:
				self.epFull=int(data)
			self.hpdetector=0

		if self.statusDetector==1:
			self.lastStatus=(data, 0)
			#Ready for status duration
			self.statusDetector=2
		elif self.statusDetector==2:
			m=re.compile('[0-9]+m').search(data)
			if m!=None:
				no_minutes=int(re.compile('[0-9]+').search(m.group()).group())
				self.lastStatus=(self.lastStatus[0], no_minutes)

			if self.detectingPositiveStatus:
				self.positiveStatus.append(self.lastStatus)
			else:
				self.negativeStatus.append(self.lastStatus)
			self.lastStatus=None
			#Ready for a new round
			self.statusDetector=0
	def handle_endtag(self, tag):
		pass

	def hasPositiveStatus (self, status):
		buff_expire=2
		for s in self.positiveStatus:
			if s[0]==status and s[1]>=buff_expire:
				return True
		return False
	def hasNegativeStatus (self, status):
		for s in self.negativeStatus:
			if s[0]==status:
				return True
		return False
	def getBuffList(self, buffList):
		newBuff=[]
		for buff in buffList:
			if buff=='PAINT_F' and not self.hasPositiveStatus('informed'):
				newBuff.append(buff)
			elif buff=='PREDICTG' and not self.hasPositiveStatus('lucky'):
				newBuff.append(buff)
			elif buff=='MSHIELD' and not self.hasPositiveStatus('guarded'):
				newBuff.append(buff)
			elif buff=='CLONE' and not self.hasPositiveStatus('cloned'):
				newBuff.append(buff)
		return newBuff
	def severelyInjured(self):
		if self.hasNegativeStatus('blind') or self.hasNegativeStatus('immobile') or self.hasNegativeStatus('paralyzed') or self.hasNegativeStatus('stunned') or self.hasNegativeStatus('schizophrenic'):
			return True
		#These 3 together will have 50% reduction in accuracy
		if self.hasNegativeStatus('confused') and self.hasNegativeStatus('forgetful') and self.hasNegativeStatus('disoriented'):
			return True
		return False
	def enoughEP(self):
		if self.epReal-5*self.getHealCount()>=self.epFull*self.ep_lower_bound:
			return True
		else:
			return False
	def getHealCount(self):
		hp_healed=44
		if float(self.hpReal)/float(self.hpFull)<self.hp_threshold:
			healcount=(int(self.hpFull*self.hp_threshold)-self.hpReal)/hp_healed+1
			return healcount
		return 0
	#Return: time in seconds
	def getEnergyRechargeTime(self):
		weakDuration=0
		energizedDuration=0
		#1 EP recharge for 8s, 16s when weak
		baseRate=float(100)*60/960
		basicTime=int((self.epFull*self.ep_recover_bound-self.epReal)*baseRate*2)
		if basicTime<=0:
			return 0
		for s in self.negativeStatus:
			if s[0]=='weak':
				weakDuration=s[1]*60
		for s in self.positiveStatus:
			if s[0]=='energized':
				energizedDuration=s[1]*60
		#print ('Recover amount:',int(self.epFull*self.ep_recover_bound-self.epReal))
		#print ('Weak duration:', weakDuration)
		#print ('Energized duration:', energizedDuration)
		if basicTime>weakDuration:
			basicTime=basicTime-(basicTime-weakDuration)/2

		t1=weakDuration
		if t1>basicTime:
			t1=basicTime
		#None weak duration
		t2=basicTime-t1

		#Some formulae
		if energizedDuration<=t1:
			basicTime=basicTime-energizedDuration/2
		elif energizedDuration>t1 and energizedDuration<=basicTime/2:
			basicTime=basicTime+t1/2-energizedDuration
		else:
			basicTime=(basicTime+t1/2)/2

		return basicTime
	#Return: time in seconds
	def getInjuryRecoveryTime(self):
		t1=0
		t2=0
		for s in self.negativeStatus:
			if s[0]=='blind' and s[1]>t1:
				t1=s[1]
			elif s[0]=='immobile' and s[1]>t1:
				t1=s[1]
			elif s[0]=='paralyzed' and s[1]>t1:
				t1=s[1]
			elif s[0]=='stunned' and s[1]>t1:
				t1=s[1]
			elif s[0]=='schizophrenic' and s[1]>t1:
				t1=s[1]
		#These 3 together will have 50% reduction in accuracy
		if self.hasNegativeStatus('confused') and self.hasNegativeStatus('forgetful') and self.hasNegativeStatus('disoriented'):
			for s in self.negativeStatus:
				if (s[0]=='confused' and s[1]<t2) or t2==0:
					t2=s[1]
				elif (s[0]=='forgetful' and s[1]<t2) or t2==0:
					t2=s[1]
				elif (s[0]=='disoriented' and s[1]<t2) or t2==0:
					t2=s[1]
		if t1>t2:
			return (t1)*60
		else:
			return (t2)*60
	def getSleepTime(self):
		injuryRecover=self.getInjuryRecoveryTime()
		energyTime=self.getEnergyRechargeTime()
		print ('Energy recharge time:', energyTime)
		print ('Injury recovery time:', injuryRecover)
		if injuryRecover>energyTime:
			return injuryRecover
		else:
			return energyTime
if __name__ == "__main__":
	f = Facebook('facebook')
	pf=ProfileParser()

	f.login()
	page=f.open(Facebook.appsURL+'/ability/profile/2233834')
	#print (page)
	pf.feed(page)
	#pf.feed(f.open(Facebook.appsURL+'/ability/profile/512586'))
	pf.close()
	print ('HP:', pf.hpReal, '/', pf.hpFull)
	print ('EP:', pf.epReal, '/', pf.epFull)
	print ('Positive statuses:', pf.positiveStatus)
	print ('Negative statuses:', pf.negativeStatus)
	buffs=['PAINT_F', 'PREDICTG', 'CLONE']
	cal_buffs=pf.getBuffList(buffs)
	print ('Calculated buff:', cal_buffs)
	print ('Calculated heal count:', pf.getHealCount())
	print ('Severely injured:', pf.severelyInjured())
	print ('Enough EP:', pf.enoughEP())
	t=pf.getEnergyRechargeTime()
	print ('Energy recharge time:', t)
	t=pf.getInjuryRecoveryTime()
	print ('Injury recovery time:', t)
	f.logout()

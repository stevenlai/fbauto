import os.path
import sys
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from .ProfileParser import ProfileParser

attackThread_finished=False
class AttackThread(threading.Thread):
	opener=None
	runner=None
	action=''
	target=[]
	header=[]
	profile=None
	def __init__(self, opener, runner, target, action):
		threading.Thread.__init__(self)
		self.target=target
		self.runner=runner
		self.opener=opener
		self.action=action

	def run (self):
		#for k, v in enumerate(header):
			#print (k, v)
		#for k, v in enumerate(self.target):
			#print (k, v)
		global attackThread_finished
		if attackThread_finished:
			self.runner.runAway()
		elif len(self.target)<1:
			attackThread_finished=True
			print ('No Job, run!')
			self.runner.runAway()
		else:
			try:
				html=self.opener.open('http://apps.facebook.com/ability/use?selectedAction='+self.action+'&targetFilter=LOCAL')
				ab=BasicParser()
				ab.opener=self.opener
				ab.feed(html)
				ab.close()
				self.header=ab.header
				input=self.header+self.target
				input.append(('selectedAction', self.action))
				#print ('generating actions:')
				#for k, v in enumerate(input):
					#print (k, v)
				if len(input)<4:
					print ('Oops, header missing?')
					attackThread_finished=True
					self.runner.runAway()
					return
				input = urllib.parse.urlencode(input)
				html = self.opener.open('http://apps.facebook.com/ability/use', input)
				if re.compile("out of range").search(html)!=None:
					attackThread_finished=True
					print ('Ability: '+self.action+' used (out of range)!')
					self.runner.runAway()
				elif len(re.compile("knock").findall(html))>1:
					attackThread_finished=True
					print ('Ability: '+self.action+' used (KO)!')
					self.runner.runAway()
				elif len(re.compile("enough energy").findall(html))>1:
					attackThread_finished=True
					print ('Ability: '+self.action+' used (No EP)!')
					self.runner.runAway()
				elif re.compile("body must rest").search(html)!=None:
					print ('Ability: '+self.action+' used too soon!')
				elif re.compile("fail").search(html)!=None:
					print ('Ability: '+self.action+' used (failed)!')
				elif re.compile("success").search(html)!=None:
					self.runner.actionSuccessful(self.action)
					print ('Ability: '+self.action+' used successfully!')
				else:
					print ('Ability: '+self.action+' used with unknown results!')
			except:
				print ("Attack thread failed: ", sys.exc_info()[0], " Run!")
				self.runner.runAway()
class BasicParser(HTMLParser):
	begin=False
	searchHP=0
	lvlRange=None
	opener=None
	header=[]
	target=[]
	realFighter=None
	candidateFighter=None
	#friends up to 2908. Better not touch 2015701. It'd crach the profile parser if killed
	friends=['2015701', '170272', '889325', '101318', '2908', '15146', '39464', '115388', '340393', '347727', '364131', '404793', '450272', '465331', '490936', '512586', '544796', '550424', '575365', '703505', '791200', '970911', '1024689', '27363', '47479', '74702', '142522', '242776', '249392', '276621', '357422', '395028', '396832', '417841', '423406', '691656', '697988', '843187', '1115781', '1256921', '1310165', '1335201', '138863', '399924', '1076226', '1088181', '1440671', '1663644', '1728999', '1798376', '1849569', '1926579', '2389526']
	enemies=['57824', '1217115']

	def __init__(self):
		HTMLParser.__init__(self)
		self.header=[]
		self.target=[]
		self.begin=False
		self.searchHP=0
		self.realFighter=None
		self.candidateFighter=None
		self.lvlRange=(50, 80)
	def handle_starttag(self, tag, attrs):
		if tag=='form' and attrs.count(('action','http://apps.facebook.com/ability/use'))>0:
			self.begin=True
		if tag=='input' and self.begin:
			if attrs[0]==('type', 'hidden') and attrs[1][1]!='selectedAction':
				self.header.append((attrs[1][1], attrs[2][1]))
			elif attrs[0]==('type', 'checkbox'):
				self.searchHP=1
				self.candidateFighter=(attrs[2][1], attrs[3][1],)
		elif self.begin and tag=='div' and len(attrs)>0 and attrs[0]==('class','level') and self.searchHP==1:
			self.searchHP=2

	def handle_data (self, data):
		if self.searchHP==2 and re.compile("Level").search(data)!=None:
			m=re.compile('[0-9]+').search(data)
			self.candidateFighter=(self.candidateFighter[0], self.candidateFighter[1],int(m.group()))
			#self.addFighterStalk()
			self.addFighterRandom()
			self.searchHP=0

	def handle_endtag(self, tag):
		if tag=='form' and self.begin:
			if self.realFighter!=None:
				print ('Picked up a target, id: '+self.realFighter[1]+', level: ',)
				print (self.realFighter[2])
				self.target.append((self.realFighter[0], self.realFighter[1]))

	#Attack policies goes here
	def addFighterStalk(self):
		if self.candidateFighter[2]>=self.lvlRange[0] and self.candidateFighter[2]<=self.lvlRange[1] and self.friends.count(self.candidateFighter[1])<1:
			if self.realFighter==None:
				self.realFighter=self.candidateFighter
			elif self.enemies.count(self.candidateFighter[1])>0 and self.enemies.count(self.realFighter[1])<1:
				print ('Enemy found, time to revenge!')
				self.realFighter=self.candidateFighter
			elif self.enemies.count(self.realFighter[1])<1 and (self.realFighter[2]<self.candidateFighter[2] or (self.realFighter[2]==self.candidateFighter[2] and random.randint(1, 2)==1)):
				self.realFighter=self.candidateFighter

	def addFighterRandom(self):
		if self.candidateFighter[2]>=self.lvlRange[0] and self.candidateFighter[2]<=self.lvlRange[1] and self.friends.count(self.candidateFighter[1])<1:
			if self.realFighter==None:
				self.realFighter=self.candidateFighter
			elif self.enemies.count(self.candidateFighter[1])>0 and self.enemies.count(self.realFighter[1])<1:
				print ('Enemy found, time to revenge!')
				self.realFighter=self.candidateFighter
			elif self.enemies.count(self.realFighter[1])<1 and random.randint(1, 2)==1:
				self.realFighter=self.candidateFighter
class Ability(threading.Thread):
	opener=None
	hasRun=False
	maxRetry=0
	disguise=True
	dirtyFight=True
	sleepTime=2
	restFirst=False
	# Need to be changed based on the location
	#safetyPlace=('20','<b>World Financial')
	#safetyPlace=('30','<b>Outside Excalibur')
	safetyPlace=('46','<b>Moscow State University')
	dangerPlace=None
	#Action, start, repetition, check results, finished
	#for FFA
	#debufs=[['PREDICTD', 0, 1, False, False], ['DISGUISE', 2, 6, True, False], ['SPY', 0, 5, True, False], ['MUDDLE_T', 0, 5, True, False], ['HEARVOIC', 0, 5, True, False], ['FORGET', 1, 5, True, False], ['PLANT_FK', 1, 5, True, False]]
	#for NPC
	debufs=[['PREDICTD', 0, 1, False, False], ['SPY', 0, 5, False, False], ['MUDDLE_T', 0, 5, False, False], ['FORGET', 1, 5, False, False], ['PLANT_FK', 1, 5, False, False], ['BRI_LGHT', 0, 1, False, False]]
	def __init__(self, f):
		threading.Thread.__init__(self)
		self.opener=f
		self.hasRun=False
		self.sleepTime=2
	def __goBattle (self):
		#NPC (Las Vegas)
		#battleFields=[('24', '<b>McCarran International'), ('29', '<b>Outside the Bellagio'), ('26', '<br>The Strip')]
		#NPC (Russia)
		battleFields=[('45', '<b>Red Square')]
		#FFA
		#battleFields=[('6', '<b>Times Square'), ('12', '<b>Central Park'), ('13', '<br>Battery Park')]
		page = 'http://apps.facebook.com/ability/jGo'
		#NY (Random FFA)
		selection=random.randint(0, len(battleFields)-1)
		self.dangerPlace=battleFields[selection]
		print ('going to battle field: ', self.dangerPlace[1])
		try:
			info=urllib.parse.urlencode({"destLocationId" : self.dangerPlace[0], "walk" : "true"})
			self.opener.open(page, info)
			page = 'http://apps.facebook.com/ability/use?selectedAction=PUNCH&targetFilter=LOCAL'
			return self.opener.open(page)
		except:
			print ("Going to battle failed: ", sys.exc_info()[0])
			self.runAway()
	def __useBuff (self):
		#FFA
		#buffs=['PAINT_F', 'PREDICTG', 'MSHIELD', 'CLONE']
		#NPC
		buffs=['PAINT_F', 'PREDICTG', 'CLONE']
		newBuffs=self.profile.getBuffList(buffs)
		print ('Buffs going to give:',newBuffs)
		if len(newBuffs)>0:
			ab = BasicParser()
			ab.opener=self.opener
			page = 'http://apps.facebook.com/ability/use?selectedAction=PAINT_F&targetFilter=SELF'
			ab.feed(self.opener.open(page))
			ab.close()
			target = ab.target
			header = ab.header
			self.__useAction(header, target, newBuffs, 1)
	def initDebufs (self):
		for a in self.debufs:
			a[4] = False
	def actionSuccessful (self, action):
		for  a in self.debufs:
			if action==a[0]:
				a[4] = True
	def __useAction_thread (self, header, target, action, repeat):
		global attackThread_finished
		attackThread_finished=False
		thread_interval=0.3
		for i in range(0, repeat):
			if self.dirtyFight:
				for a in self.debufs:
					if not a[4] and (i-a[1])>=0 and (i-a[1])%a[2]==0:
						thrd = AttackThread(self.opener, self, target, a[0])
						thrd.start()
						if not a[3]:
							a[4] = True

			for a in action:
				thrd = AttackThread(self.opener, self, target, a)
				thrd.start()
				time.sleep(thread_interval)

			#Last thread just to join
			if i==repeat-1:
				thrd = AttackThread(self.opener, self, target, 'PUNCH')
				thrd.start()
				thrd.join()

		#if 30>repeat*2*thread_interval:
			#time.sleep(30-repeat*2*thread_interval)
		print ('Last running away!')
		self.runAway()
	def __useAction (self, header, target, action, repeat):
		header_real=header
		target_real=target
		finished=False
		for i in range(0, repeat):
			if finished:
				break
			j=0
			while j<len(action):
				if finished:
					break
				input=header_real+target_real
				input.append(('selectedAction', action[j]))
				input = urllib.parse.urlencode(input)
				html = self.opener.open('http://apps.facebook.com/ability/use', input)
				if re.compile("out of range").search(html)!=None or len(re.compile("knock").findall(html))>1 or len(re.compile("enough energy").findall(html))>1:
					finished=True
					print ('Ability: '+action[j]+' used (finished)!')
				elif re.compile("fail").search(html)!=None:
					print ('Ability: '+action[j]+' used (failed)!')
				else:
					print ('Ability: '+action[j]+' used!')
					j=j+1
				ab=BasicParser()
				ab.opener=self.opener
				ab.feed(html)
				ab.close()
				header_real=ab.header
	def __attack (self):
		self.initDebufs()
		ab=BasicParser()
		ab.opener=self.opener
		html = self.__goBattle()
		ab.feed(html)
		ab.close()

		target = ab.target
		header = ab.header
		self.__useAction_thread(header, target, ['MINDSHOU', 'PUNCH', 'EXPLO'], 30)
	def travelBack (self):
		info = urllib.parse.urlencode({"destLocationId" : self.safetyPlace[0], "useFlyingAbility" : "true"})
		self.opener.open('http://apps.facebook.com/ability/jGo', info)
		time.sleep(random.randint(6,10))
	def runAway (self):
		if self.hasRun==False:
			#http://apps.facebook.com/ability/go?destLocationId=36&btnGo=true
			page = 'http://apps.facebook.com/ability/jGo'
			print ('Running away!')
			try:
				self.hasRun=True
				info = urllib.parse.urlencode({"destLocationId" : self.safetyPlace[0], "walk" : "true"})
				self.opener.open(page, info)
				html=f.open('http://apps.facebook.com/ability/')

				if re.compile(self.safetyPlace[1]).search(html)!=None:
					print ('Succeeded!')
				elif re.compile(self.dangerPlace[1]).search(html)!=None:
					print ('Failed! Stuck in the dangerous place!')
					self.maxRetry=self.maxRetry-1
					if self.maxRetry<=0:
						print ("Max retrial reached, God help us!")
						self.hasRun=True
					self.hasRun=False
					self.runAway()
				else:
					print ('We have been teleported!')
					self.opener.login()
					self.travelBack()
			except:
				print ("Failed: ", sys.exc_info()[0])
				self.maxRetry=self.maxRetry-1
				if self.maxRetry<=0:
					print ("Max retrial reached, God help us!")
					self.hasRun=True
				self.hasRun=False
				self.opener.login()
				self.runAway()
		else:
			pass
	def heal(self):
		if self.profile.hasNegativeStatus('weak'):
			print ('Ready to restore strength')
			ab = BasicParser()
			ab.opener=self.opener
			page = 'http://apps.facebook.com/ability/use?selectedAction=RESTOSTR&targetFilter=SELF'
			ab.feed(self.opener.open(page))
			ab.close()
			target = ab.target
			header = ab.header
			self.__useAction(header, target, ['RESTOSTR'], 1)

		if self.profile.hasNegativeStatus('scared') or self.profile.hasNegativeStatus('fearful'):
			print ('Ready to calm down')
			ab = BasicParser()
			ab.opener=self.opener
			page = 'http://apps.facebook.com/ability/use?selectedAction=CALM&targetFilter=SELF'
			ab.feed(self.opener.open(page))
			ab.close()
			target = ab.target
			header = ab.header
			self.__useAction(header, target, ['CALM'], 1)

		healcount=self.profile.getHealCount()
		if healcount>0:
			print ('Ready to heal for', healcount, 'times')
			ab = BasicParser()
			ab.opener=self.opener
			page = 'http://apps.facebook.com/ability/use?selectedAction=HEALMB&targetFilter=SELF'
			ab.feed(self.opener.open(page))
			ab.close()
			target = ab.target
			header = ab.header
			self.__useAction(header, target, ['HEALMB'], healcount)

	def loadProfile(self):
		self.profile=ProfileParser()
		self.profile.feed(f.open('http://apps.facebook.com/ability/profile/2233834'))
		if self.profile.hpFull==0 or self.profile.epFull==0:
			print ('Error loading profile! Retrying!')
			self.opener.login()
			self.loadProfile()
		else:
			print ('Profile loaded successfully')
			print ('HP:', self.profile.hpReal, '/', self.profile.hpFull)
			print ('EP:', self.profile.epReal, '/', self.profile.epFull)
			print ('Positive statuses:', self.profile.positiveStatus)
			print ('Negative statuses:', self.profile.negativeStatus)
	def visitPages(self):
		pages=['http://apps.facebook.com/ability/friends', 'http://apps.facebook.com/ability/group/31167', 'http://apps.facebook.com/ability/groupMembers']
		for i in range(0, random.randint(0, len(pages))):
			self.opener.open(pages[random.randint(0, len(pages)-1)])
			time.sleep(random.randint(10,30))
	def doJob(self):
		t1=time.time()
		self.loadProfile()
		while not self.profile.severelyInjured() and not self.profile.hasNegativeStatus('weak') and self.profile.enoughEP():
			self.heal()
			self.__useBuff()
			try:
				self.hasRun=False
				self.maxRetry=random.randint(100, 200)
				self.__attack()
			except:
				print ("Attack failed: ", sys.exc_info()[0], " run away!")
				self.hasRun=False
				self.runAway()
			time.sleep(random.randint(5, 20))
			self.loadProfile()
		if not self.profile.severelyInjured():
			self.heal()
		t2=time.time()-t1
		print ('Finished attack, used time:',t2)
		self.visitPages()
		self.loadProfile()
		if self.profile.hasNegativeStatus('weak'):
			self.sleepTime=self.profile.getInjuryRecoveryTime()
		else:
			self.sleepTime=self.profile.getSleepTime()


	def run (self):
		if self.restFirst:
			print ('Wait for energy recharge first!')
			self.opener.login()
			self.loadProfile()
			self.sleepTime=self.profile.getSleepTime()
			self.opener.logout()
			print ('Going to sleep. Sleep time:',self.sleepTime)
			time.sleep(random.randint(self.sleepTime, self.sleepTime+60))
		while True:
			try:
				self.opener.login()
				self.doJob()
				if self.opener.logout():
					self.sleepTime=2
			except:
				print ("Something went wrong: ", sys.exc_info()[0])
				self.sleepTime=2
			print ('Going to sleep and wait for the next attack. Sleep time:',self.sleepTime)
			time.sleep(random.randint(self.sleepTime, self.sleepTime+60))

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "lr")
	runOnce=False
	restFirst=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
		elif o == "-r":
			restFirst=True
	#Temp
	#time.sleep(45*60)
	ability = Ability(f)
	ability.restFirst=restFirst
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		ability.doJob()
		f.logout()
	else:
		f.rememberPass=True
		ability.start()

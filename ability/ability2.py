import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook
from FBAuto.common.JobRunner import *
from FBAuto.common.FormParser import *
from .ProfileParser import ProfileParser
from .FightParser import *

appID='ability'
thisProfile='2233834'
attackThread_finished=False
class CodeLoader(threading.Thread):
	opener=None
	runner=None
	notifyThreads=[]
	def __init__(self, opener, targetID, runner):
		threading.Thread.__init__(self)
		self.opener=opener
		self.targetID=targetID
		self.runner=runner
		self.notifyThreads=[]
	def run (self):
		global attackThread_finished
		if not attackThread_finished:
			html=self.opener.open(Facebook.appsURL+'/'+appID+'/target?targetUserId='+self.targetID)
			cf=FightConfigure(html)
			if cf.code!='':
				print ('Code loaded:', cf.code)
				for i in range(0, len(self.notifyThreads)):
					self.notifyThreads[i].header['useAbilityCode']=cf.code
					self.notifyThreads[i].header['fb_sig_friends']=cf.friendList
					self.notifyThreads[i].start()
					time.sleep(0.3)
					if i==len(self.notifyThreads)-1:
						self.notifyThreads[i].join()
			else:
				print ('Cannot load the code, run!')
				attackThread_finished=True
				self.runner.runAway()

class AttackThread(threading.Thread):
	opener=None
	runner=None
	header=[]
	def __init__(self, opener, runner, header):
		threading.Thread.__init__(self)
		self.runner=runner
		self.opener=opener
		self.header=header

	def run (self):
		#for k, v in enumerate(header):
			#print (k, v)
		#for k, v in enumerate(self.target):
			#print (k, v)
		global attackThread_finished
		if attackThread_finished:
			self.runner.runAway()
		else:
			try:
				print ('Using action ('+self.header['useAbilityCode']+'): '+self.header['selectedAction'])
				#print (self.header)
				html=self.opener.open(Facebook.appsURL+'/'+appID+'/jUse', urllib.parse.urlencode(self.header))
				if re.compile("out of range").search(html)!=None:
					attackThread_finished=True
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used (out of range)!')
					self.runner.runAway()
				elif len(re.compile("knock").findall(html))>1:
					attackThread_finished=True
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used (KO)!')
					self.runner.runAway()
				elif len(re.compile("enough energy").findall(html))>1:
					attackThread_finished=True
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used (No EP)!')
					self.runner.runAway()
				elif re.compile("body must rest").search(html)!=None:
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used too soon!')
				elif re.compile("fail").search(html)!=None:
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used (failed)!')
				elif re.compile("success").search(html)!=None:
					self.runner.actionSuccessful(self.header['selectedAction'])
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used successfully!')
				else:
					print ('Ability ('+self.header['useAbilityCode']+'): '+self.header['selectedAction']+' used with unknown results!')
			except:
				print ("Attack thread failed: ", sys.exc_info()[0], " Run!")
				traceback.print_exc(file=sys.stdout)
				self.runner.runAway()
class Ability(JobRunner):
	opener=None
	hasRun=False
	maxRetry=0
	disguise=True
	dirtyFight=True
	restFirst=False
	# Need to be changed based on the location
	#safetyPlace=('20','<b>World Financial')
	#safetyPlace=('30','<b>Outside Excalibur')
	safetyPlace=('46','<b>Moscow State University')
	dangerPlace=None
	#Action, start, repetition, check results, finished
	#for FFA
	#debufs=[['PREDICTD', 0, 1, False, False], ['DISGUISE', 2, 6, True, False], ['SPY', 0, 5, True, False], ['MUDDLE_T', 0, 5, True, False], ['HEARVOIC', 0, 5, True, False], ['FORGET', 1, 5, True, False], ['PLANT_FK', 1, 5, True, False]]
	#for NPC
	debufs=[['PREDICTD', 0, 1, False, False], ['SPY', 0, 5, False, False], ['BRI_LGHT', 0, 1, False, False]]
	def __init__(self, f):
		JobRunner.__init__(self, f)
		self.opener=f
		self.appID=appID
		self.jobDescription='General'
		self.hasRun=False
	def goBattle (self):
		#NPC (Las Vegas)
		#battleFields=[('24', '<b>McCarran International'), ('29', '<b>Outside the Bellagio'), ('26', '<br>The Strip')]
		#NPC (Russia)
		battleFields=[('45', '<b>Red Square')]
		#battleFields=[('2', '<b>Grand Central')]
		#FFA
		#battleFields=[('6', '<b>Times Square'), ('12', '<b>Central Park'), ('13', '<br>Battery Park')]
		page = Facebook.appsURL+'/'+appID+'/jGo'
		#NY (Random FFA)
		selection=random.randint(0, len(battleFields)-1)
		self.dangerPlace=battleFields[selection]
		print ('going to battle field: ', self.dangerPlace[1])
		try:
			info=urllib.parse.urlencode({"destLocationId" : self.dangerPlace[0], "walk" : "true"})
			self.opener.open(page, info)
		except:
			print ("Going to battle failed: ", sys.exc_info()[0])
			traceback.print_exc(file=sys.stdout)
			self.runAway()
	def useBuff (self):
		#FFA
		#buffs=['PAINT_F', 'PREDICTG', 'MSHIELD', 'CLONE']
		#NPC
		buffs=['PAINT_F', 'PREDICTG', 'CLONE']
		newBuffs=self.profile.getBuffList(buffs)
		print ('Buffs going to give:',newBuffs)
		if len(newBuffs)>0:
			self.__useAction(newBuffs, 1)
	def initDebufs (self):
		for a in self.debufs:
			a[4] = False
	def actionSuccessful (self, action):
		for  a in self.debufs:
			if action==a[0]:
				a[4] = True
	def useAction_thread (self, config, target, action, repeat):
		global attackThread_finished
		attackThread_finished=False
		thread_interval=0.3
		for i in range(0, repeat):
			cl=None
			#Need to load the code
			cl=CodeLoader(self.opener, target, self)
			if self.dirtyFight:
				for a in self.debufs:
					if not a[4] and (i-a[1])>=0 and (i-a[1])%a[2]==0:
						header={"targetUserId" : target, "selectedAction" : a[0], "notificationType" : "EMAIL", "useAbilityCode" : config.code,  "fb_sig_friends": config.friendList}
						thrd = AttackThread(self.opener, self, header)
						cl.notifyThreads.append(thrd)
						if not a[3]:
							a[4] = True

			for a in action:
				header={"targetUserId" : target, "selectedAction" : a, "notificationType" : "EMAIL", "useAbilityCode" : config.code,  "fb_sig_friends": config.friendList}
				thrd = AttackThread(self.opener, self, header)
				cl.notifyThreads.append(thrd)

			#Last thread just to join
			cl.start()
			time.sleep(0.6)
			if i==repeat-1:
				cl.join()

		#if 30>repeat*2*thread_interval:
			#time.sleep(30-repeat*2*thread_interval)
		time.sleep(20)
		print ('Last running away!')
		self.runAway()
	def __useAction (self, action, repeat):
		html=None
		finished=False
		for i in range(0, repeat):
			if finished:
				break
			j=0
			while j<len(action):
				if finished:
					break
				html=self.opener.open(Facebook.appsURL+'/'+appID+'/use?selectedAction='+action[j]+'&targetFilter=SELF')

				fp = SimpleFormParser(appID+'/use', {}, {})
				fp.feed(html)
				fp.close()
				if len(fp.forms)>0:
					html = fp.forms[0].submit(self.opener, Facebook.appsURL+'/'+appID+'/use')
				else:
					finished=True

				if re.compile("out of range").search(html)!=None or len(re.compile("knock").findall(html))>1 or len(re.compile("enough energy").findall(html))>1:
					finished=True
					print ('Ability: '+action[j]+' used (finished)!')
				elif re.compile("fail").search(html)!=None:
					print ('Ability: '+action[j]+' used (failed)!')
				else:
					print ('Ability: '+action[j]+' used!')
					j=j+1
	def attack (self):
		self.initDebufs()
		self.goBattle()
		fp=FightParser()
		fp.lvlRange==(50, 80)
		fp.friendList=[]
		fp.enemyList=[]
		page=self.opener.open(Facebook.appsURL+'/'+appID+'/local?fight=true')
		#print (page)
		fp.feed(page)
		fp.close()
		c=FightConfigure(page)
		if fp.target==None:
			print ('No target, run!')
			self.runAway()
		else:
			print ('Pick up a target:')
			fp.target.display()
			print ('Code:', c.code)
			self.useAction_thread(c, fp.target.id, ['MINDSHOU', 'EXPLO'], 40)
	def travelBack (self):
		info = urllib.parse.urlencode({"destLocationId" : self.safetyPlace[0], "useFlyingAbility" : "true"})
		self.opener.open(Facebook.appsURL+'/'+appID+'/jGo', info)
		time.sleep(random.randint(6,10))
	def runAway (self):
		if self.hasRun==False:
			#http://apps.facebook.com/'+appID+'/go?destLocationId=36&btnGo=true
			page = Facebook.appsURL+'/'+appID+'/jGo'
			print ('Running away!')
			try:
				self.hasRun=True
				info = urllib.parse.urlencode({"destLocationId" : self.safetyPlace[0], "walk" : "true"})
				self.opener.open(page, info)
				html=self.opener.open(Facebook.appsURL+'/'+appID+'/')

				if re.compile(self.safetyPlace[1]).search(html)!=None:
					print ('Succeeded!')
				elif re.compile(self.dangerPlace[1]).search(html)!=None:
					print ('Failed! Stuck in the dangerous place!')
					self.maxRetry=self.maxRetry-1
					if self.maxRetry<=0:
						print ("Max retrial reached, God help us!")
						self.hasRun=True
					self.hasRun=False
					self.runAway()
				else:
					print ('We have been teleported!')
					self.opener.login()
					self.travelBack()
			except:
				print ("Failed: ", sys.exc_info()[0])
				traceback.print_exc(file=sys.stdout)
				self.maxRetry=self.maxRetry-1
				if self.maxRetry<=0:
					print ("Max retrial reached, God help us!")
					self.hasRun=True
				self.hasRun=False
				self.opener.login()
				self.runAway()
		else:
			pass
	def heal(self):
		if self.profile.hasNegativeStatus('weak'):
			print ('Ready to restore strength')
			self.__useAction(['RESTOSTR'], 1)

		if self.profile.hasNegativeStatus('scared') or self.profile.hasNegativeStatus('fearful'):
			print ('Ready to calm down')
			self.__useAction(['CALM'], 1)

		healcount=self.profile.getHealCount()
		if healcount>0:
			print ('Ready to heal for', healcount, 'times')
			self.__useAction(['HEALMB'], healcount)

	def loadProfile(self):
		self.profile=ProfileParser()
		self.profile.feed(self.opener.open(Facebook.appsURL+'/'+appID+'/profile/'+thisProfile))
		while self.profile.hpFull==0 or self.profile.epFull==0:
			print ('Error loading profile! Retrying!')
			self.opener.login()
			self.profile.feed(self.opener.open(Facebook.appsURL+'/'+appID+'/profile/'+thisProfile))

		print ('Profile loaded successfully')
		print ('HP:', self.profile.hpReal, '/', self.profile.hpFull)
		print ('EP:', self.profile.epReal, '/', self.profile.epFull)
		print ('Positive statuses:', self.profile.positiveStatus)
		print ('Negative statuses:', self.profile.negativeStatus)
	def visitPages(self):
		pages=[Facebook.appsURL+'/'+appID+'/friends', Facebook.appsURL+'/'+appID+'/group', Facebook.appsURL+'/'+appID+'/groupMembers']
		for i in range(0, random.randint(0, len(pages))):
			self.opener.open(pages[random.randint(0, len(pages)-1)])
			time.sleep(random.randint(10,30))
	def initJob(self):
		if self.restFirst:
			print ('Wait for energy recharge first!')
			self.opener.login()
			self.loadProfile()
			sleepTime=self.profile.getSleepTime()
			self.opener.logout()
			print ('Going to sleep. Sleep time:',sleepTime)
			time.sleep(random.randint(sleepTime, sleepTime+60))
	def doJob(self):
		t1=time.time()
		self.loadProfile()
		while not self.profile.severelyInjured() and not self.profile.hasNegativeStatus('weak') and self.profile.enoughEP():
			self.heal()
			self.useBuff()
			try:
				self.hasRun=False
				self.maxRetry=random.randint(100, 200)
				self.attack()
			except:
				print ("Attack failed: ", sys.exc_info()[0], " run away!")
				traceback.print_exc(file=sys.stdout)
				self.hasRun=False
				self.runAway()
			time.sleep(random.randint(5, 20))
			self.loadProfile()
		if not self.profile.severelyInjured():
			self.heal()
		t2=time.time()-t1
		print ('Finished attack, used time:',t2)
		self.visitPages()
		self.loadProfile()
		if self.profile.hasNegativeStatus('weak'):
			sleepTime=self.profile.getInjuryRecoveryTime()
		else:
			sleepTime=self.profile.getSleepTime()
		self.sleepTime=[sleepTime, sleepTime+60]

if __name__ == "__main__":
	f = Facebook('facebook')
	opts, args = getopt.getopt(sys.argv[1:], "lr")
	runOnce=False
	restFirst=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
		elif o == "-r":
			restFirst=True
	#Temp
	#time.sleep(45*60)
	ability = Ability(f)
	ability.restFirst=restFirst
	#f.rememberPass=True
	if runOnce:
		print ('Executing doJob for once!')
		f.login()
		ability.doJob()
		f.logout()
	else:
		ability_timer=JobTimer([18*3600, 20*3600], [4*3600, 6*3600])
		ability.jobTimer=ability_timer
		ability.start()


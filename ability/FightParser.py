import re
import random
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

class Hero:
	id=''
	name=''
	group=''
	level=0
	hpFull=0
	hpReal=0
	abilities=[]
	def __init__(self):
		self.id=''
		self.name=''
		self.group=''
		self.level=0
		self.hpFull=0
		self.hpReal=0
		self.abilities=[]
	def display(self):
		print ('Name:', self.name)
		print ('ID:', self.id)
		print ('Group:', self.group)
		print ('Level:', self.level)
		print ('HP:', self.hpReal, '/', self.hpFull)
		print ('Abilities:', self.abilities)
		print ('')

class FightConfigure:
	code=''
	friendList=''
	page=''
	def __init__(self, page):
		self.code=''
		self.friendList=''
		self.page=page
		self.loadInfo()
	def loadInfo(self):
		m=re.compile('var [a-zA-Z0-9]+_useAbilityCode = "[a-zA-Z0-9]+";').search(self.page)
		if m!=None:
			self.code=re.compile('[a-zA-Z0-9]+').search(re.compile('"[a-zA-Z0-9]+"').search(m.group()).group()).group()
		m=re.compile('"fb_sig_friends" : "[0-9,]+"').search(self.page)
		if m!=None:
			self.friendList=re.compile('[0-9,]+').search(m.group()).group()
	def display(self):
		print ('Code:', self.code)
		print ('Friend list:', self.friendList)

#May need to change if there are 'invisible' on the list
class FightParser(HTMLParser):
	#HP/EP related
	detectProgress=0
	currentHero=None
	target=None
	lvlRange=(1, 80)
	friendList=[]
	enemyList=[]

	def __init__(self):
		HTMLParser.__init__(self)
		#HP/EP related
		self.detectProgress=0
		self.currentHero=None

	def addCurrentHero(self):
		if self.target==None:
			self.target=self.currentHero
		if self.currentHero.level>=self.lvlRange[0] and self.currentHero.level<=self.lvlRange[1] and self.friendList.count(self.currentHero.id)<1:
			if self.target==None:
				self.target=self.currentHero
			elif self.enemyList.count(self.currentHero.id)>0 and self.enemyList.count(self.currentHero.id)<1:
				print ('Enemy found, time to revenge!')
				self.target=self.currentHero
			elif self.enemyList.count(self.target.id)<1 and random.randint(1, 2)==1:
				self.target=self.currentHero
	def handle_starttag(self, tag, attrs):
		if tag=='table':
			for a in attrs:
				if a[0]=='class' and a[1]=='personTable':
					# Find a new hero, ready for id
					self.detectProgress=1
					self.currentHero=Hero()
		elif tag=='a' and self.detectProgress==1 and self.currentHero!=None and self.currentHero.id=='':
			for a in attrs:
				if a[0]=='href':
					m=re.compile('[0-9]+').search(a[1])
					if m!=None:
						self.currentHero.id=m.group()
						#Ready for name (Really necessary?)
						self.detectProgress=2
		elif tag=='img' and self.detectProgress==2 and self.currentHero!=None and self.currentHero.name=='':
			for a in attrs:
				if a[0]=='alt':
					self.currentHero.name=a[1]
					#Ready for group tag (Necessary?)
					self.detectProgress=3
		elif tag=='div' and self.detectProgress==3 and self.currentHero!=None and self.currentHero.group=='':
			for a in attrs:
				if a[0]=='class' and a[1]=='group':
					#Ready for group info/string
					self.detectProgress=4
		elif tag=='a' and self.detectProgress==4 and self.currentHero!=None and self.currentHero.group=='':
			for a in attrs:
				if a[0]=='href' and re.compile('group').search(a[1])!=None:
					self.currentHero.group=re.compile('[0-9]+').search(a[1]).group()
					#Ready for level tag
					self.detectProgress=5
		elif tag=='span' and self.detectProgress==5 and self.currentHero!=None and self.currentHero.level==0:
			for a in attrs:
				if a[0]=='id' and re.compile('_level').search(a[1])!=None:
					#Ready for level string
					self.detectProgress=6
		elif tag=='span' and self.detectProgress==7 and self.currentHero!=None and self.currentHero.hpReal==0:
			for a in attrs:
				if a[0]=='id' and re.compile('_hp').search(a[1])!=None:
					#Ready for real hp string
					self.detectProgress=8
		elif tag=='span' and self.detectProgress==9 and self.currentHero!=None and self.currentHero.hpFull==0:
			for a in attrs:
				if a[0]=='id' and re.compile('_maxHP').search(a[1])!=None:
					#Ready for full hp string
					self.detectProgress=10
		elif tag=='a' and self.detectProgress==11 and self.currentHero!=None:
			for a in attrs:
				if a[0]=='href' and re.compile('abiliwiki').search(a[1])!=None:
					#Ready for full ability string
					self.detectProgress=12
		elif tag=='span' and self.detectProgress==11:
			for a in attrs:
				if a[0]=='id' and re.compile('_displayLoc').search(a[1])!=None:
					#self.currentHero.display()
					self.addCurrentHero()
					#Start over again
					self.detectProgress=0
					self.currentHero=None
	def handle_data (self, data):
		if self.detectProgress==4 and self.currentHero!=None and self.currentHero.group=='':
			if data.strip()!='':
				self.currentHero.group=data.strip()
				#Ready for level tag
				self.detectProgress=5
		elif self.detectProgress==6 and self.currentHero!=None and self.currentHero.level==0:
			self.currentHero.level=int(data)
			#Ready for real hp tag
			self.detectProgress=7
		elif self.detectProgress==8 and self.currentHero!=None and self.currentHero.hpReal==0:
			self.currentHero.hpReal=int(data)
			#Ready for full hp tag
			self.detectProgress=9
		elif self.detectProgress==10 and self.currentHero!=None and self.currentHero.hpFull==0:
			self.currentHero.hpFull=int(data)
			#Ready for ability List
			self.detectProgress=11
		elif self.detectProgress==12 and self.currentHero!=None:
			self.currentHero.abilities.append(data)
			#Ready for ability List
			self.detectProgress=11
	def handle_endtag(self, tag):
		pass

if __name__ == "__main__":
	f = Facebook('facebook')
	fp=FightParser()

	f.login()
	page=f.open(Facebook.appsURL+'/ability/local?fight=true')
	#print (page)
	fp.feed(page)
	fp.close()
	c=FightConfigure(page)
	c.display()
	f.logout()

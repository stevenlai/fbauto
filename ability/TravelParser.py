import re
import urllib
from html.parser import HTMLParser
from FBAuto.common.facebook import Facebook

#The parser class is NOT used any more. The program is only used to run once in order to travel
class TravelParser(HTMLParser):
	begin=False
	header=[]
	opener=None

	def __init__(self, opener):
		HTMLParser.__init__(self)
		self.header=[]
		self.begin=False
		self.opener=opener
	def handle_starttag(self, tag, attrs):
		if tag=='form':
			action=Facebook.appsURL+'/ability/go'
			for a in attrs:
				if a[0]=='action' and a[1]==action:
					self.begin=True
		elif self.begin and tag=='input':
			name=''
			value=''
			for a in attrs:
				if a[0]=='name':
					name=a[1]
				elif a[0]=='value':
					value=a[1]
			self.header.append((name, value))

	def handle_endtag(self, tag):
		if self.begin and tag=='form':
			print ('Ready to travel:')
			for i in self.header:
				print (i)
			input = urllib.parse.urlencode(self.header)
			self.opener.open(Facebook.appsURL+'/ability/go', input)
			self.begin=False

if __name__ == "__main__":
	f = Facebook('facebook')
	#f.rememberPass=True
	f.login()
	info = urllib.parse.urlencode({"destLocationId" : "2", "useFlyingAbility" : "true"})
	html=f.open(Facebook.appsURL+'/ability/jGo', info)
	#print (html)
	html=f.open(Facebook.appsURL+'/ability/')
	#print (html)
	f.logout()

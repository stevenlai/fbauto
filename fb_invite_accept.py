import os.path
import sys
import traceback
import urllib
import threading
import time
import re
import random
import getopt
from html.parser import HTMLParser
from FBAuto.common.facebook import *
from FBAuto.common.JobRunner import *
from FBAuto.common.SessionController import SessionController
#Apps
import FBAuto.common.autoaccept

if __name__ == "__main__":
	opts, args = getopt.getopt(sys.argv[1:], "l")
	runOnce=False
	for o, a in opts:
		if o == "-l":
			runOnce=True
	accounts=AccountManager('fb_invite_accept.txt')
	senders=[]
	for f in accounts.logins:
		controller=SessionController(f)
		controller.setMaxConnection(100)
		senders.append(controller)

		gameJob = FBAuto.common.autoaccept.JobMain(controller)
		gameJob.jobURL='/?sk=games&ap=1&_fb_noscript=1'
		gameJob.email=f.email

		appsJob = FBAuto.common.autoaccept.JobMain(controller)
		appsJob.jobURL='/?sk=apps&ap=1&_fb_noscript=1'
		appsJob.email=f.email

		if runOnce:
			gameJob.doJob()
			appsJob.doJob()
		else:
			gameJob.start()
			appsJob.start()
		#time.sleep(random.randint(1, 2))

